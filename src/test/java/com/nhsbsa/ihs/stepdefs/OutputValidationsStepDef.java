package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;

public class OutputValidationsStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private IHSNumberPage ihsNumberPage;
    private VisaShareCodePage visaShareCodePage;
    private EnterAddressPage enterAddressPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;
    private UploadEHICFilePage uploadEHICFilePage;
    private UploadCASFilePage uploadCASFilePage;
    private AddDependantPage addDependantPage;
    private DependantPaidWorkPage dependantPaidWorkPage;
    private DependantLivingInUKPage dependantLivingInUKPage;
    private DependantNotEligiblePage dependantNotEligiblePage;
    private DependantNamePage dependantNamePage;
    private DependantDateOfBirthPage dependantDateOfBirthPage;
    private DependantIHSNumberPage dependantIHSNumberPage;
    private DependantVisaShareCodePage dependantVisaShareCodePage;
    private DependantSameAddressPage dependantSameAddressPage;
    private DependantEnterAddressPage dependantEnterAddressPage;
    private CheckDependantsPage checkDependantsPage;

    public OutputValidationsStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        visaShareCodePage = new VisaShareCodePage(driver);
        enterAddressPage = new EnterAddressPage(driver);
        emailPage = new EmailPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        uploadEHICFilePage = new UploadEHICFilePage(driver);
        uploadCASFilePage = new UploadCASFilePage(driver);
        addDependantPage = new AddDependantPage(driver);
        dependantPaidWorkPage = new DependantPaidWorkPage(driver);
        dependantLivingInUKPage = new DependantLivingInUKPage(driver);
        dependantNotEligiblePage = new DependantNotEligiblePage(driver);
        dependantNamePage = new DependantNamePage(driver);
        dependantDateOfBirthPage = new DependantDateOfBirthPage(driver);
        dependantIHSNumberPage = new DependantIHSNumberPage(driver);
        dependantVisaShareCodePage = new DependantVisaShareCodePage(driver);
        dependantSameAddressPage = new DependantSameAddressPage(driver);
        dependantEnterAddressPage = new DependantEnterAddressPage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
    }

    @Then("^I will see the (.*)$")
    public void iWillSeeTheOutput(String output) {
        switch (output) {
            case "Start screen":
                Assert.assertEquals(START_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains( START_PAGE_URL));
                break;
            case "IHS Paid screen":
                Assert.assertEquals(IHS_PAID_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(IHS_PAID_PAGE_URL));
                break;
            case "Name screen":
                Assert.assertEquals(NAME_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(NAME_PAGE_URL));
                break;
            case "Date of birth screen":
                Assert.assertEquals(DOB_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DOB_PAGE_URL));
                break;
            case "IHS Number screen":
                Assert.assertEquals(IHS_NUMBER_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(IHS_NUMBER_PAGE_URL));
                break;
            case "Visa Share Code screen":
                Assert.assertEquals(VISA_SHARE_CODE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(VISA_SHARE_CODE_PAGE_URL));
                break;
            case "Enter Address screen":
                Assert.assertEquals(ENTER_ADDRESS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(ENTER_ADDRESS_PAGE_URL));
                break;
            case "Email Address screen":
                Assert.assertEquals(EMAIL_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(EMAIL_PAGE_URL));
                break;
            case "Phone number screen":
                Assert.assertEquals(PHONE_NUMBER_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(PHONE_NUMBER_PAGE_URL));
                break;
            case "Upload EHIC Card screen":
                Assert.assertEquals(UPLOAD_EHIC_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(UPLOAD_EHIC_PAGE_URL));
                break;
            case "View Uploaded EHIC screen":
                Assert.assertEquals(VIEW_EHIC_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(VIEW_EHIC_PAGE_URL));
                break;
            case "Upload CAS Letter screen":
                Assert.assertEquals(UPLOAD_CAS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(UPLOAD_CAS_PAGE_URL));
                break;
            case "View Uploaded Evidence screen":
                Assert.assertEquals(VIEW_EVIDENCE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(VIEW_EVIDENCE_PAGE_URL));
                break;
            case "Add Dependant screen":
                Assert.assertEquals(ADD_DEPENDANT_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(ADD_DEPENDANT_PAGE_URL));
                break;
            case "Dependant Not Eligible screen":
                Assert.assertEquals(DEPENDANT_NOT_ELIGIBLE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_NOT_ELIGIBLE_PAGE_URL));
                break;
            case "Dependant Paid Work screen":
                Assert.assertEquals(DEPENDANT_PAID_WORK_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_PAID_WORK_PAGE_URL));
                break;
            case "Dependant Living in UK screen":
                Assert.assertEquals(DEPENDANT_LIVING_UK_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_LIVING_UK_PAGE_URL));
                break;
            case "Dependant Name screen":
                Assert.assertEquals(DEPENDANT_NAME_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_NAME_PAGE_URL));
                break;
            case "Dependant Date of birth screen":
                Assert.assertEquals(DEPENDANT_DOB_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_DOB_PAGE_URL));
                break;
            case "Dependant IHS Number screen":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_IHS_NUMBER_PAGE_URL));
                break;
            case "Dependant Visa Share Code screen":
                Assert.assertEquals(DEPENDANT_VISA_SHARE_CODE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_VISA_SHARE_CODE_PAGE_URL));
                break;
            case "Same Address as Applicant screen":
                Assert.assertEquals(SAME_ADDRESS_AS_APPLICANT_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(SAME_ADDRESS_AS_APPLICANT_PAGE_URL));
                break;
            case "Enter Dependant Address screen":
                Assert.assertEquals(ENTER_DEPENDANT_ADDRESS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(ENTER_DEPENDANT_ADDRESS_PAGE_URL));
                break;
            case "Upload Dependant EHIC Card screen":
                Assert.assertEquals(UPLOAD_DEPENDANT_EHIC_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(UPLOAD_DEPENDANT_EHIC_PAGE_URL));
                break;
            case "View Uploaded Dependant EHIC screen":
                Assert.assertEquals(VIEW_DEPENDANT_EHIC_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(VIEW_DEPENDANT_EHIC_PAGE_URL));
                break;
            case "Check Dependant Details screen":
                Assert.assertEquals(CHECK_DEPENDANTS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_DEPENDANTS_PAGE_URL));
                break;
            case "Check Your Answers screen":
                Assert.assertEquals(CHECK_ANSWERS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_ANSWERS_PAGE_URL));
                break;
            case "Declaration screen":
                Assert.assertEquals(DECLARATION_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DECLARATION_PAGE_URL));
                break;
            case "Confirmation screen":
                Assert.assertEquals(CONFIRMATION_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CONFIRMATION_PAGE_URL));
                break;
            case "GOV UK screen":
                Assert.assertEquals(GOV_UK_PAGE, commonPage.getCurrentPageTitle());
                break;
            case "Service Start screen":
                Assert.assertEquals(START_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                dependantNotEligiblePage.navigateToMoreInfo();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains( START_PAGE_URL));
                break;
            case "GOV UK View and Prove screen":
                Assert.assertEquals(REQUEST_SHARE_CODE_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                visaShareCodePage.navigateToShareCode();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(REQUEST_SHARE_CODE_PAGE_URL));
                break;
            case "Gov UK Student Feedback screen":
                Assert.assertEquals(GOV_UK_STUDENT_FEEDBACK_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Help screen":
                Assert.assertEquals(HELP_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Snap survey preview":
                Assert.assertEquals(SNAP_SURVEY_PREVIEW_PAGE, commonPage.getSurveyPreviewPageTitle());
                break;
            case "IHS Students Service Survey screen":
                Assert.assertEquals(STUDENT_SNAP_SURVEY_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Contact Us screen":
                Assert.assertEquals(CONTACT_US_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToContactUs();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(CONTACT_US_PAGE_URL));
                break;
            case "Cookies Policy screen":
                Assert.assertEquals(COOKIES_POLICY_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToCookies();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(COOKIES_POLICY_PAGE_URL));
                break;
            case "Accessibility Statement screen":
                Assert.assertEquals(ACCESSIBILITY_STATEMENT_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToAccessibilityStatement();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(ACCESSIBILITY_STATEMENT_PAGE_URL));
                break;
            case "Terms and Conditions screen":
                Assert.assertEquals(TERMS_CONDITIONS_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToTermsConditions();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(TERMS_CONDITIONS_PAGE_URL));
                break;
            case "Privacy Notice screen":
                Assert.assertEquals(PRIVACY_NOTICE_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToPrivacyNotice();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(PRIVACY_NOTICE_PAGE_URL));
                break;
            case "Contact UK Visas and Immigration screen":
                Assert.assertEquals(CONTACT_UK_VISAS_AND_IMMIGRATION_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToVisasAndImmigrationLink();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(CONTACT_UK_VISAS_AND_IMMIGRATION_PAGE_URL));
                break;
            case "Cookies Policy page":
                Assert.assertEquals(COOKIES_POLICY_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "Blank given name error":
                Assert.assertEquals("Enter the applicant's given name", namePage.getGivenNameErrorMessage());
                break;
            case "Blank family name error":
                Assert.assertEquals("Enter the applicant's family name", namePage.getFamilyNameErrorMessage());
                break;
            case "Invalid given name error":
                Assert.assertEquals("Enter the applicant's given name using only letters, hyphens and apostrophes using between 2 and 50 characters", namePage.getGivenNameErrorMessage());
                break;
            case "Invalid family name error":
                Assert.assertEquals("Enter the applicant's family name using only letters, hyphens and apostrophes using between 2 and 50 characters", namePage.getFamilyNameErrorMessage());
                break;
            case "Invalid range given name error":
                Assert.assertEquals("Enter the applicant's given name using between 2 and 50 characters", namePage.getGivenNameErrorMessage());
                break;
            case "Invalid range family name error":
                Assert.assertEquals("Enter the applicant's family name using between 2 and 50 characters", namePage.getFamilyNameErrorMessage());
                break;
            case "Blank date of birth error":
                Assert.assertEquals("Enter the applicant's date of birth", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid format date of birth error":
                Assert.assertEquals("Enter the applicant's date of birth in the correct format, for example, 31 3 1980", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Minimum 16 years error":
                Assert.assertEquals("Enter a date of birth that is a minimum of 16 years ago", dateOfBirthPage.getMinimumYearsErrorMessage());
                break;
            case "Blank day error":
                Assert.assertEquals("Enter a day", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Blank month error":
                Assert.assertEquals("Enter a month", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Blank year error":
                Assert.assertEquals("Enter a year", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Incomplete date of birth error":
                Assert.assertEquals("Enter a complete date of birth", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid date of birth error":
                Assert.assertEquals("Enter a valid date of birth", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid character date of birth error":
                Assert.assertEquals("Enter a date of birth using numbers only", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid character day error":
                Assert.assertEquals("Enter a day using numbers only", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid character month error":
                Assert.assertEquals("Enter a month using numbers only", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid character year error":
                Assert.assertEquals("Enter a year using numbers only", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Blank ihs number error":
                Assert.assertEquals("Enter the applicant’s IHS number", ihsNumberPage.getIHSNumberErrorMessage());
                break;
            case "Invalid ihs number error":
                Assert.assertEquals("Enter an IHS number that starts with IHS or IHSC and has 9 numbers", ihsNumberPage.getIHSNumberErrorMessage());
                break;
            case "Invalid range ihs number error":
                Assert.assertEquals("Enter an IHS number that is 12 or 13 characters", ihsNumberPage.getIHSNumberErrorMessage());
                break;
            case "Blank share code error":
                Assert.assertEquals("Enter the applicant's visa share code", visaShareCodePage.getEmptyVisaShareCodeErrorMessage());
                break;
            case "Invalid share code error":
                Assert.assertEquals("Enter a correct visa share code", visaShareCodePage.getVisaShareCodeErrorMessage());
                break;
            case "Blank address line 1 error":
                Assert.assertEquals("Enter the applicant's address line 1", enterAddressPage.getAddressLineErrorMessage());
                break;
            case "Invalid address line 1 error":
                Assert.assertEquals("Enter the applicant's address line 1 using only letters, numbers, hyphens and apostrophes using between 2 and 35 characters", enterAddressPage.getAddressLineErrorMessage());
                break;
            case "Invalid range address line 1 error":
                Assert.assertEquals("Enter the applicant's address line 1 using between 2 and 35 characters", enterAddressPage.getAddressLineErrorMessage());
                break;
            case "Invalid address line 2 error":
                Assert.assertEquals("Enter the applicant's address line 2 using only letters, numbers, hyphens and apostrophes using between 2 and 35 characters", enterAddressPage.getAddressLineErrorMessage());
                break;
            case "Invalid range address line 2 error":
                Assert.assertEquals("Enter the applicant's address line 2 using between 2 and 35 characters", enterAddressPage.getAddressLineErrorMessage());
                break;
            case "Blank town error":
                Assert.assertEquals("Enter the applicant's town or city", enterAddressPage.getTownErrorMessage());
                break;
            case "Invalid town error":
                Assert.assertEquals("Enter the applicant's town or city using only letters, hyphens and apostrophes using between 2 and 35 characters", enterAddressPage.getTownErrorMessage());
                break;
            case "Invalid range town error":
                Assert.assertEquals("Enter the applicant's town or city using between 2 and 35 characters", enterAddressPage.getTownErrorMessage());
                break;
            case "Invalid county error":
                Assert.assertEquals("Enter the applicant's county using only letters, hyphens and apostrophes using between 2 and 35 characters", enterAddressPage.getCountyErrorMessage());
                break;
            case "Invalid range county error":
                Assert.assertEquals("Enter the applicant's county using between 2 and 35 characters", enterAddressPage.getCountyErrorMessage());
                break;
            case "Blank post code error":
                Assert.assertEquals("Enter the applicant's postcode", enterAddressPage.getPostCodeErrorMessage());
                break;
            case "Wrong post code error":
                Assert.assertEquals("Enter a valid postcode", enterAddressPage.getWrongPostCodeErrorMessage());
                break;
            case "Blank email address error":
                Assert.assertEquals("Enter an email address that we can use to contact the applicant about the reimbursement", emailPage.getEmailErrorMessage());
                break;
            case "Invalid email address error":
                Assert.assertEquals("Enter an email address in the correct format, like name@example.com", emailPage.getEmailErrorMessage());
                break;
            case "Invalid range email address error":
                Assert.assertEquals("Enter an email address that is no more than 50 characters", emailPage.getEmailErrorMessage());
                break;
            case "Invalid phone number error":
                Assert.assertEquals("Enter the applicant's telephone number using numbers 0 to 9", mobileNumberPage.getMobileNumberErrorMessage());
                break;
            case "Invalid range phone number error":
                Assert.assertEquals("Enter a telephone number using between 11 and 15 characters", mobileNumberPage.getMobileNumberErrorMessage());
                break;
            case "No file selected error":
                Assert.assertEquals("Select a file to upload as evidence", uploadEHICFilePage.getUploadErrorMessage());
                break;
            case "Dependant question error":
                Assert.assertEquals("Select 'Yes' if the applicant wants to add any dependants", addDependantPage.getErrorMessageSelectNoneOption());
                break;
            case "Dependant Paid Work Error Message":
                Assert.assertEquals("Select 'Yes' if the dependant is carrying out any paid work in the UK", dependantPaidWorkPage.getErrorMessage());
                break;
            case "Dependant Living in UK Error Message":
                Assert.assertEquals("Select 'Yes' if the dependant is currently living in the UK", dependantLivingInUKPage.getErrorMessage());
                break;
            case "Blank dependant given name error":
                Assert.assertEquals("Enter the dependant's given name", dependantNamePage.getGivenNameErrorMessage());
                break;
            case "Blank dependant family name error":
                Assert.assertEquals("Enter the dependant's family name", dependantNamePage.getFamilyNameErrorMessage());
                break;
            case "Invalid dependant given name error":
                Assert.assertEquals("Enter the dependant's given name using only letters, hyphens and apostrophes using between 2 and 50 characters", dependantNamePage.getGivenNameErrorMessage());
                break;
            case "Invalid dependant family name error":
                Assert.assertEquals("Enter the dependant's family name using only letters, hyphens and apostrophes using between 2 and 50 characters", dependantNamePage.getFamilyNameErrorMessage());
                break;
            case "Invalid range dependant given name error":
                Assert.assertEquals("Enter the dependant's given name using between 2 and 50 characters", dependantNamePage.getGivenNameErrorMessage());
                break;
            case "Invalid range dependant family name error":
                Assert.assertEquals("Enter the dependant's family name using between 2 and 50 characters", dependantNamePage.getFamilyNameErrorMessage());
                break;
            case "Blank dependant date of birth error":
                Assert.assertEquals("Enter the dependant's date of birth", dependantDateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid format dependant date of birth error":
                Assert.assertEquals("Enter the dependant's date of birth in the correct format, for example, 31 3 1980", dependantDateOfBirthPage.getDOBErrorMessage());
                break;
            case "Future date dependant error":
                Assert.assertEquals("Enter a date of birth that is either today's date or before", dependantDateOfBirthPage.getMinimumYearsErrorMessage());
                break;
            case "Blank dependant ihs number error":
                Assert.assertEquals("Enter the dependant’s IHS number", dependantIHSNumberPage.getIHSNumberErrorMessage());
                break;
            case "Invalid dependant ihs number error":
                Assert.assertEquals("Enter an IHS number that starts with IHS or IHSC and has 9 numbers", dependantIHSNumberPage.getIHSNumberErrorMessage());
                break;
            case "Invalid range dependant ihs number error":
                Assert.assertEquals("Enter an IHS number that is 12 or 13 characters", dependantIHSNumberPage.getIHSNumberErrorMessage());
                break;
            case "Blank dependant share code error":
                Assert.assertEquals("Enter the dependant's visa share code", dependantVisaShareCodePage.getEmptyVisaShareCodeErrorMessage());
                break;
            case "Invalid dependant share code error":
                Assert.assertEquals("Enter a correct visa share code", dependantVisaShareCodePage.getVisaShareCodeErrorMessage());
                break;
            case "Address question error":
                Assert.assertEquals("Select 'Yes' if the dependant lives at the same address as the applicant", dependantSameAddressPage.getErrorMessage());
                break;
            case "Blank dependant address line 1 error":
                Assert.assertEquals("Enter the dependant's address line 1", dependantEnterAddressPage.getAddressLineErrorMessage());
                break;
            case "Invalid dependant address line 1 error":
                Assert.assertEquals("Enter the dependant's address line 1 using only letters, numbers, hyphens and apostrophes using between 2 and 35 characters", dependantEnterAddressPage.getAddressLineErrorMessage());
                break;
            case "Invalid range dependant address line 1 error":
                Assert.assertEquals("Enter the dependant's address line 1 using between 2 and 35 characters", dependantEnterAddressPage.getAddressLineErrorMessage());
                break;
            case "Invalid dependant address line 2 error":
                Assert.assertEquals("Enter the dependant's address line 2 using only letters, numbers, hyphens and apostrophes using between 2 and 35 characters", dependantEnterAddressPage.getAddressLineErrorMessage());
                break;
            case "Invalid range dependant address line 2 error":
                Assert.assertEquals("Enter the dependant's address line 2 using between 2 and 35 characters", dependantEnterAddressPage.getAddressLineErrorMessage());
                break;
            case "Blank dependant town error":
                Assert.assertEquals("Enter the dependant's town or city", dependantEnterAddressPage.getTownErrorMessage());
                break;
            case "Invalid dependant town error":
                Assert.assertEquals("Enter the dependant's town or city using only letters, hyphens and apostrophes using between 2 and 35 characters", dependantEnterAddressPage.getTownErrorMessage());
                break;
            case "Invalid range dependant town error":
                Assert.assertEquals("Enter the dependant's town or city using between 2 and 35 characters", dependantEnterAddressPage.getTownErrorMessage());
                break;
            case "Invalid dependant county error":
                Assert.assertEquals("Enter the dependant's county using only letters, hyphens and apostrophes using between 2 and 35 characters", dependantEnterAddressPage.getCountyErrorMessage());
                break;
            case "Invalid range dependant county error":
                Assert.assertEquals("Enter the dependant's county using between 2 and 35 characters", dependantEnterAddressPage.getCountyErrorMessage());
                break;
            case "Blank dependant post code error":
                Assert.assertEquals("Enter the dependant's postcode", dependantEnterAddressPage.getPostCodeErrorMessage());
                break;
            case "Wrong dependant post code error":
                Assert.assertEquals("Enter a valid postcode", dependantEnterAddressPage.getWrongPostCodeErrorMessage());
                break;
            case "More Dependants question error":
                Assert.assertEquals("Select 'Yes' if the applicant wants to add any dependants", checkDependantsPage.getErrorMessageSelectNoneOption());
                break;
            case "Dependant deleted success message for single dependant delete":
                Assert.assertEquals(ADD_DEPENDANT_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(checkDependantsPage.getDeleteSuccessMessage().contains("Dependant 1 Given Name Family Name has been deleted"));
                break;
            case "Dependant deleted success message on Check dependant details screen":
                Assert.assertEquals(CHECK_DEPENDANTS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(checkDependantsPage.getDeleteSuccessMessage().contains("Dependant 1 Given Name Family Name has been deleted"));
                break;
        }
    }

    @And("My Page Title is (.*) title$")
    public void myPageTitleIsScreenTitle(String screen) {
        switch (screen) {
            case "Name screen":
                Assert.assertEquals(NAME_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Date of birth screen":
                Assert.assertEquals(DOB_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "IHS Number screen":
                Assert.assertEquals(IHS_NUMBER_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Visa Share Code screen":
                Assert.assertEquals(VISA_SHARE_CODE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Enter Address screen":
                Assert.assertEquals(ENTER_ADDRESS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Email Address screen":
                Assert.assertEquals(EMAIL_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Phone number screen":
                Assert.assertEquals(PHONE_NUMBER_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Upload EHIC Card screen":
                Assert.assertEquals(UPLOAD_EHIC_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "View Uploaded EHIC screen":
                Assert.assertEquals(VIEW_EHIC_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Upload CAS Letter screen":
                Assert.assertEquals(UPLOAD_CAS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "View Uploaded Evidence screen":
                Assert.assertEquals(VIEW_EVIDENCE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Add Dependant screen":
                Assert.assertEquals(ADD_DEPENDANT_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Dependant Not Eligible screen":
                Assert.assertEquals(DEPENDANT_NOT_ELIGIBLE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Dependant Paid Work screen":
                Assert.assertEquals(DEPENDANT_PAID_WORK_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Dependant Living in UK screen":
                Assert.assertEquals(DEPENDANT_LIVING_UK_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Dependant Name screen":
                Assert.assertEquals(DEPENDANT_NAME_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Dependant Date of birth screen":
                Assert.assertEquals(DEPENDANT_DOB_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Dependant IHS Number screen":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Dependant Visa Share Code screen":
                Assert.assertEquals(DEPENDANT_VISA_SHARE_CODE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Same Address as Applicant screen":
                Assert.assertEquals(SAME_ADDRESS_AS_APPLICANT_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Enter Dependant Address screen":
                Assert.assertEquals(ENTER_DEPENDANT_ADDRESS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Upload Dependant EHIC Card screen":
                Assert.assertEquals(UPLOAD_DEPENDANT_EHIC_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "View Uploaded Dependant EHIC screen":
                Assert.assertEquals(VIEW_DEPENDANT_EHIC_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Check Dependant Details screen":
                Assert.assertEquals(CHECK_DEPENDANTS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Check Your Answers screen":
                Assert.assertEquals(CHECK_ANSWERS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Declaration screen":
                Assert.assertEquals(DECLARATION_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Confirmation screen":
                Assert.assertEquals(CONFIRMATION_PAGE_TITLE, commonPage.getCurrentPageTitle());
                break;
            case "Contact Us screen":
                Assert.assertEquals(CONTACT_US_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "Cookies Policy screen":
                Assert.assertEquals(COOKIES_POLICY_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "Accessibility Statement screen":
                Assert.assertEquals(ACCESSIBILITY_STATEMENT_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "Terms and Conditions screen":
                Assert.assertEquals(TERMS_CONDITIONS_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "Privacy Notice screen":
                Assert.assertEquals(PRIVACY_NOTICE_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
        }
    }

    @Then("^I will be displayed (.*) for (.*)(.*) file$")
    public void iiWillBeDisplayedErrorForFileNameFileFormatFile(String error, String fileName, String fileFormat) {
        switch (error){
            case "Invalid file format error":
                Assert.assertEquals(uploadEHICFilePage.getUploadErrorMessage(), fileName + fileFormat + " must be a jpg, bmp, png or pdf");
                break;
            case "Invalid file name error":
                Assert.assertEquals(uploadEHICFilePage.getUploadErrorMessage(), fileName + fileFormat + " must use only letters, numbers, spaces, hyphens and underscore");
                break;
            case "Max file size error":
                Assert.assertEquals(uploadEHICFilePage.getUploadErrorMessage(), fileName + fileFormat + " must be less than 2MB in size");
                break;
            case "More than 130 characters error":
                Assert.assertEquals(uploadEHICFilePage.getUploadErrorMessage(), fileName + fileFormat + " must be 130 characters or less");
                break;
        }
    }
}
