package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static com.nhsbsa.ihs.stepdefs.Constants.*;

public class DependantDetailsStepDef {
    private WebDriver driver;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private IHSNumberPage ihsNumberPage;
    private UploadCASFilePage uploadCASFilePage;
    private VisaShareCodePage visaShareCodePage;
    private EnterAddressPage enterAddressPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;
    private AddDependantPage addDependantPage;
    private DependantPaidWorkPage dependantPaidWorkPage;
    private DependantLivingInUKPage dependantLivingInUKPage;
    private DependantNotEligiblePage dependantNotEligiblePage;
    private DependantNamePage dependantNamePage;
    private DependantDateOfBirthPage dependantDateOfBirthPage;
    private DependantIHSNumberPage dependantIHSNumberPage;
    private DependantVisaShareCodePage dependantVisaShareCodePage;
    private DependantSameAddressPage dependantSameAddressPage;
    private DependantEnterAddressPage dependantEnterAddressPage;
    private CheckDependantsPage checkDependantsPage;
    private UploadEHICFilePage uploadEHICFilePage;
    private UploadDependantEHICFilePage uploadDependantEHICFilePage;

    public DependantDetailsStepDef() {
        driver = Config.getDriver();
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        uploadCASFilePage = new UploadCASFilePage(driver);
        visaShareCodePage = new VisaShareCodePage(driver);
        enterAddressPage = new EnterAddressPage(driver);
        emailPage = new EmailPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        addDependantPage = new AddDependantPage(driver);
        dependantPaidWorkPage = new DependantPaidWorkPage(driver);
        dependantLivingInUKPage = new DependantLivingInUKPage(driver);
        dependantNotEligiblePage = new DependantNotEligiblePage(driver);
        dependantNamePage = new DependantNamePage(driver);
        dependantDateOfBirthPage = new DependantDateOfBirthPage(driver);
        dependantIHSNumberPage = new DependantIHSNumberPage(driver);
        dependantVisaShareCodePage = new DependantVisaShareCodePage(driver);
        dependantSameAddressPage = new DependantSameAddressPage(driver);
        dependantEnterAddressPage = new DependantEnterAddressPage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
        uploadEHICFilePage = new UploadEHICFilePage(driver);
        uploadDependantEHICFilePage = new UploadDependantEHICFilePage(driver);
    }

    @And("^I (.*) have dependant to add$")
    public void iDependantOptionHaveDependantToAdd(String dependantOption) {
        switch (dependantOption) {
            case "do":
                addDependantPage.selectYesRadioButtonAndClickContinue();
                break;
            case "do not":
                addDependantPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                addDependantPage.continueButton();
                break;
        }
    }

    @When("^My dependant (.*) carrying out paid work in UK$")
    public void myDependantWorkOptionCarryingOutPaidWorkInUK(String workOption) {
        switch (workOption) {
            case "is":
                dependantPaidWorkPage.selectYesRadioButtonAndClickContinue();
                break;
            case "is not":
                dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                dependantPaidWorkPage.continueButton();
                break;
        }
    }

    @When("^My dependant (.*) currently residing in UK$")
    public void myDependantResidingOptionCurrentlyResidingInUK(String residingOption) {
        switch (residingOption) {
            case "is":
                dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
                break;
            case "is not":
                dependantLivingInUKPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                dependantLivingInUKPage.continueButton();
                break;
        }
    }

    @When("^My dependant's firstname is (.*) and surname is (.*)$")
    public void myDependantSFirstnameIsGivenNameAndSurnameIsFamilyName(String GivenName, String FamilyName) {
        dependantNamePage.enterNameAndSubmit(GivenName, FamilyName);
    }

    @When("^I attempt to enter (.*) characters dependant's name$")
    public void iAttemptToEnterCountCharactersDependantSName(int count) {
        dependantNamePage.enterOverLimitText(count);
    }

    @Then("^I am restricted to enter (.*) characters dependant's name$")
    public void iAmRestrictedToEnterMaximumCharactersDependantSName(int maximum) {
        Assert.assertEquals(dependantNamePage.getEnteredTextCount(), maximum);
        dependantNamePage.continueButton();
    }

    @When("^My dependant's date of birth is (.*) (.*) (.*)$")
    public void myDependantSDateOfBirthIsDayMonthYear(String Day, String Month, String Year) {
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(Day, Month, Year);
    }

    @When("^My dependant's IHS number is (.*)$")
    public void myDependantSIHSNumberIsIHSNumber(String IHSNumber) {
        dependantIHSNumberPage.enterIHSNumberAndSubmit(IHSNumber);
    }

    @When("^I attempt to enter (.*) characters dependant IHS number$")
    public void iAttemptToEnterAnyCharactersDependantIHSNumber(int count) {
        dependantIHSNumberPage.enterOverLimitText("ihsc1234567890");
    }

    @Then("^I am restricted to enter (.*) characters dependant IHS number$")
    public void iAmRestrictedToEnterMaximumCharactersDependantIHSNumber(int maximum) {
        Assert.assertEquals(dependantIHSNumberPage.getEnteredTextCount(), maximum);
        ihsNumberPage.continueButton();
    }
    @When("^My dependant's visa share code is (.*)$")
    public void myDependantSVisaShareCodeIsVisaCode(String VisaCode) {
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(VisaCode);
    }

    @When("^My dependant (.*) live in the same UK address$")
    public void myDependantAddressOptionLiveInTheSameUKAddress(String addressOption) {
        switch (addressOption) {
            case "does":
                dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
                break;
            case "does not":
                dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                dependantSameAddressPage.continueButton();
                break;
        }
    }

    @When("^My dependant's address is (.*), (.*), (.*), (.*) and (.*)$")
    public void myDependantSAddressIsAddressLine1AddressLine2TownCountyAndPostcode(String AddressLine1, String AddressLine2, String Town, String County, String Postcode) {
        dependantEnterAddressPage.enterAddressAndSubmit(AddressLine1, AddressLine2, Town, County, Postcode);
    }

    @When("^I (.*) have another dependant to add$")
    public void iDependantMoreOptionHaveAnotherDependantToAdd(String dependantMoreOption) {
        switch (dependantMoreOption) {
            case "do":
                checkDependantsPage.selectYesRadioButtonAndClickContinue();
                break;
            case "do not":
                checkDependantsPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                checkDependantsPage.continueButton();
                break;
        }
    }

    @And("^I add (.*) dependants to my claim application$")
    public void iAddDependantsToMyClaimApplication(int count) {
        for (int i = 0; i < (count-1); i++) {
            dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
            dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
            dependantNamePage.enterNameAndSubmit(GIVEN_NAME, FAMILY_NAME);
            dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY, DOB_MONTH, DOB_YEAR);
            dependantIHSNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
            dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
            dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
            uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
            uploadEHICFilePage.continueOnViewEHIC();
            checkDependantsPage.selectYesRadioButtonAndClickContinue();
        }
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(GIVEN_NAME, FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY, DOB_MONTH, DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
    }

    @When("^I add a dependant who holds valid EHIC$")
    public void iAddADependantWhoHoldsValidEHIC() {
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(DEPENDANT_IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(DEPENDANT_VISA_SHARE_CODE);
        dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
        dependantEnterAddressPage.enterAddressAndSubmit(DEPENDANT_ADDRESS_LINE_1, DEPENDANT_ADDRESS_LINE_2, DEPENDANT_TOWN_NAME, DEPENDANT_COUNTY_NAME, DEPENDANT_POST_CODE);
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "123456" + ".pdf");
        uploadDependantEHICFilePage.continueOnViewEHIC();
    }

    @When("I add a dependant with same UK address")
    public void iAddADependantWithSameUKAddress() {
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(DEPENDANT_IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(DEPENDANT_VISA_SHARE_CODE);
        dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "123456" + ".pdf");
        uploadDependantEHICFilePage.continueOnViewEHIC();
    }

    @When("I add a dependant with different UK address")
    public void iAddADependantWithDifferentUKAddress() {
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(DEPENDANT_IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(DEPENDANT_VISA_SHARE_CODE);
        dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
        dependantEnterAddressPage.enterAddressAndSubmit(DEPENDANT_ADDRESS_LINE_1, DEPENDANT_ADDRESS_LINE_2, DEPENDANT_TOWN_NAME, DEPENDANT_COUNTY_NAME, DEPENDANT_POST_CODE);
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "123456" + ".pdf");
        uploadDependantEHICFilePage.continueOnViewEHIC();
    }

    @And("^I add dependant to my claim application$")
    public void iAddDependantToMyClaimApplication() throws IOException, InterruptedException {
         {
            dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
            dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
            dependantNamePage.enterNameAndSubmit(GIVEN_NAME, FAMILY_NAME);
            dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY, DOB_MONTH, DOB_YEAR);
            dependantIHSNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
            dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
            dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
            dependantEnterAddressPage.enterAddressAndSubmit("TE" ,"TE", "London", "Wiltshire ", "EC1A 1BB");
            uploadDependantEHICFilePage.chooseFileAndContinueBS(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
            uploadEHICFilePage.continueOnViewEHIC();
            checkDependantsPage.selectNoRadioButtonAndClickContinue();
        }
    }

    @And("^I select (.*) in Check Dependants screen$")
    public void iSelectChangeLinkInCheckDependantsScreen(String changeLink) {
        switch (changeLink) {
            case "Change Name link":
                checkDependantsPage.changeDependantName();
                break;
            case "Change Date of Birth link":
                checkDependantsPage.changeDependantDOB();
                break;
            case "Change IHS Number link":
                checkDependantsPage.changeDependantIHSNumber();
                break;
            case "Change Visa Share Code link":
                checkDependantsPage.changeDependantShareCode();
                break;
            case "Change Address link":
                checkDependantsPage.changeDependantAddress();
                break;
            case "Change Files Uploaded link":
                checkDependantsPage.changeDependantFilesUploaded();
                break;
        }
    }

    @And("^my dependant's (.*) is pre-populated$")
    public void myDependantSAnswerIsPrePopulated(String answer) {
        switch (answer) {
            case "name":
                Assert.assertEquals(DEPENDANT_GIVEN_NAME, dependantNamePage.getEnteredGivenName());
                Assert.assertEquals(DEPENDANT_FAMILY_NAME, dependantNamePage.getEnteredFamilyName());
                break;
            case "date of birth":
                Assert.assertEquals(DEPENDANT_DOB_DAY, dependantDateOfBirthPage.getEnteredDay());
                Assert.assertEquals(DEPENDANT_DOB_MONTH, dependantDateOfBirthPage.getEnteredMonth());
                Assert.assertEquals(DEPENDANT_DOB_YEAR, dependantDateOfBirthPage.getEnteredYear());
                break;
            case "IHS number":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER, dependantIHSNumberPage.getEnteredIHSNumber());
                break;
            case "visa share code":
                Assert.assertEquals(DEPENDANT_VISA_SHARE_CODE, dependantVisaShareCodePage.getEnteredShareCode());
                break;
            case "address":
                Assert.assertEquals(DEPENDANT_ADDRESS_LINE_1, dependantEnterAddressPage.getEnteredAddressLine1());
                Assert.assertEquals(DEPENDANT_ADDRESS_LINE_2, dependantEnterAddressPage.getEnteredAddressLine2());
                Assert.assertEquals(DEPENDANT_TOWN_NAME, dependantEnterAddressPage.getEnteredTown());
                Assert.assertEquals(DEPENDANT_COUNTY_NAME, dependantEnterAddressPage.getEnteredCounty());
                Assert.assertEquals(DEPENDANT_POST_CODE, dependantEnterAddressPage.getEnteredPostCode());
                break;
            case "blank name":
                Assert.assertEquals("", dependantNamePage.getEnteredGivenName());
                Assert.assertEquals(UPDATED_DEPENDANT_FAMILY_NAME, dependantNamePage.getEnteredFamilyName());
                break;
            case "invalid name":
                Assert.assertEquals(UPDATED_DEPENDANT_GIVEN_NAME, dependantNamePage.getEnteredGivenName());
                Assert.assertEquals(UPDATED_DEPENDANT_FAMILY_NAME+"123", dependantNamePage.getEnteredFamilyName());
                break;
            case "blank date of birth":
                Assert.assertEquals("", dependantDateOfBirthPage.getEnteredDay());
                Assert.assertEquals("", dependantDateOfBirthPage.getEnteredMonth());
                Assert.assertEquals("", dependantDateOfBirthPage.getEnteredYear());
                break;
            case "invalid date of birth":
                Assert.assertEquals("dd", dependantDateOfBirthPage.getEnteredDay());
                Assert.assertEquals("mm", dependantDateOfBirthPage.getEnteredMonth());
                Assert.assertEquals("yyyy", dependantDateOfBirthPage.getEnteredYear());
                break;
            case "blank IHS number":
                Assert.assertEquals("", dependantIHSNumberPage.getEnteredIHSNumber());
                break;
            case "invalid IHS number":
                Assert.assertEquals("!ihs98765432", dependantIHSNumberPage.getEnteredIHSNumber());
                break;
            case "invalid range IHS number":
                Assert.assertEquals("ihs98765432", dependantIHSNumberPage.getEnteredIHSNumber());
                break;
            case "blank visa share code":
                Assert.assertEquals("", dependantVisaShareCodePage.getEnteredShareCode());
                break;
            case "invalid visa share code":
                Assert.assertEquals("A1@34@67Z", dependantVisaShareCodePage.getEnteredShareCode());
                break;
            case "blank address":
                Assert.assertEquals("", dependantEnterAddressPage.getEnteredAddressLine1());
                Assert.assertEquals(UPDATED_DEPENDANT_ADDRESS_LINE_2, dependantEnterAddressPage.getEnteredAddressLine2());
                Assert.assertEquals(UPDATED_DEPENDANT_TOWN_NAME, dependantEnterAddressPage.getEnteredTown());
                Assert.assertEquals(UPDATED_DEPENDANT_COUNTY_NAME, dependantEnterAddressPage.getEnteredCounty());
                Assert.assertEquals(UPDATED_DEPENDANT_POST_CODE, dependantEnterAddressPage.getEnteredPostCode());
                break;
            case "invalid address":
                Assert.assertEquals(UPDATED_DEPENDANT_ADDRESS_LINE_1, dependantEnterAddressPage.getEnteredAddressLine1());
                Assert.assertEquals(UPDATED_DEPENDANT_ADDRESS_LINE_2, dependantEnterAddressPage.getEnteredAddressLine2());
                Assert.assertEquals(UPDATED_DEPENDANT_TOWN_NAME, dependantEnterAddressPage.getEnteredTown());
                Assert.assertEquals("(Wiltshire)", dependantEnterAddressPage.getEnteredCounty());
                Assert.assertEquals(UPDATED_DEPENDANT_POST_CODE, dependantEnterAddressPage.getEnteredPostCode());
                break;
        }
    }

    @And("^my applicant's (.*) is pre-populated$")
    public void myApplicantSParameterIsPrePopulated(String parameter) {
        Assert.assertEquals(ADDRESS_LINE_1, dependantEnterAddressPage.getEnteredAddressLine1());
        Assert.assertEquals(ADDRESS_LINE_2, dependantEnterAddressPage.getEnteredAddressLine2());
        Assert.assertEquals(TOWN_NAME, dependantEnterAddressPage.getEnteredTown());
        Assert.assertEquals(COUNTY_NAME, dependantEnterAddressPage.getEnteredCounty());
        Assert.assertEquals(POST_CODE, dependantEnterAddressPage.getEnteredPostCode());
    }

    @When("^I do not change the details and continue$")
    public void iDoNotChangeTheDetailsAndContinue() {
        checkDependantsPage.continueButton();
    }

    @And("^my dependant's (.*) is not changed$")
    public void myDependantSParameterIsNotChanged(String parameter) {
        switch (parameter) {
            case "name":
                Assert.assertEquals(checkDependantsPage.getDisplayedName(), DEPENDANT_GIVEN_NAME + " " + DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                Assert.assertEquals(checkDependantsPage.getDisplayedDoB(), DEPENDANT_DOB_DAY + " " + DEPENDANT_DOB_FULL_MONTH + " " + DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER, checkDependantsPage.getDisplayedIHSNumber());
                break;
            case "visa share code":
                Assert.assertEquals(DEPENDANT_VISA_SHARE_CODE, checkDependantsPage.getDisplayedShareCode());
                break;
            case "address":
                Assert.assertEquals(checkDependantsPage.getDisplayedAddress(), DEPENDANT_ADDRESS_LINE_1 + "\n" +
                        DEPENDANT_ADDRESS_LINE_2 + "\n" +
                        DEPENDANT_TOWN_NAME + "\n" +
                        DEPENDANT_COUNTY_NAME + "\n" +
                        DEPENDANT_POST_CODE);
                break;
        }
    }

    @When("^I change the dependant's (.*) and continue$")
    public void iChangeTheDependantSAnswerAndContinue(String answer) {
        switch (answer) {
            case "name":
                dependantNamePage.enterNameAndSubmit(UPDATED_DEPENDANT_GIVEN_NAME,UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                dependantDateOfBirthPage.enterDateOfBirthAndSubmit(UPDATED_DEPENDANT_DOB_DAY,UPDATED_DEPENDANT_DOB_MONTH,UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit(UPDATED_DEPENDANT_IHS_NUMBER);
                break;
            case "visa share code":
                dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(UPDATED_DEPENDANT_VISA_SHARE_CODE);
                break;
            case "address":
                dependantEnterAddressPage.enterAddressAndSubmit(UPDATED_DEPENDANT_ADDRESS_LINE_1, UPDATED_DEPENDANT_ADDRESS_LINE_2, UPDATED_DEPENDANT_TOWN_NAME, UPDATED_DEPENDANT_COUNTY_NAME, UPDATED_DEPENDANT_POST_CODE);
                break;
        }
    }

    @When("^I change the dependant's answer as (.*)$")
    public void iChangeTheDependantSAnswerAsInvalidAnswer(String invalidAnswer) {
        switch (invalidAnswer) {
            case "blank name":
                dependantNamePage.enterNameAndSubmit("",UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "invalid name":
                dependantNamePage.enterNameAndSubmit(UPDATED_DEPENDANT_GIVEN_NAME,UPDATED_DEPENDANT_FAMILY_NAME+"123");
                break;
            case "blank date of birth":
                dependantDateOfBirthPage.enterDateOfBirthAndSubmit("","","");
                break;
            case "invalid date of birth":
                dependantDateOfBirthPage.enterDateOfBirthAndSubmit("dd","mm","yyyy");
                break;
            case "blank IHS number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit("");
                break;
            case "invalid IHS number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit("!ihs98765432");
                break;
            case "invalid range IHS number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit("ihs98765432");
                break;
            case "blank visa share code":
                dependantVisaShareCodePage.enterVisaShareCodeAndSubmit("");
                break;
            case "invalid visa share code":
                dependantVisaShareCodePage.enterVisaShareCodeAndSubmit("A1@34@67Z");
                break;
            case "blank address":
                dependantEnterAddressPage.enterAddressAndSubmit("", UPDATED_DEPENDANT_ADDRESS_LINE_2, UPDATED_DEPENDANT_TOWN_NAME, UPDATED_DEPENDANT_COUNTY_NAME, UPDATED_DEPENDANT_POST_CODE);
                break;
            case "invalid address":
                dependantEnterAddressPage.enterAddressAndSubmit(UPDATED_DEPENDANT_ADDRESS_LINE_1, UPDATED_DEPENDANT_ADDRESS_LINE_2, UPDATED_DEPENDANT_TOWN_NAME, "(Wiltshire)", UPDATED_DEPENDANT_POST_CODE);
                break;
        }
    }

    @And("^my dependant's (.*) is changed$")
    public void myDependantSAnswerIsChanged(String answer) {
        switch (answer) {
            case "name":
                Assert.assertEquals(checkDependantsPage.getDisplayedName(), UPDATED_DEPENDANT_GIVEN_NAME + " " + UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                Assert.assertEquals(checkDependantsPage.getDisplayedDoB(), UPDATED_DEPENDANT_DOB_DAY + " " + UPDATED_DEPENDANT_DOB_FULL_MONTH + " " + UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                Assert.assertEquals(UPDATED_DEPENDANT_IHS_NUMBER, checkDependantsPage.getDisplayedIHSNumber());
                break;
            case "visa share code":
                Assert.assertEquals(UPDATED_DEPENDANT_VISA_SHARE_CODE, checkDependantsPage.getDisplayedShareCode());
                break;
            case "address":
                Assert.assertEquals(checkDependantsPage.getDisplayedAddress(), UPDATED_DEPENDANT_ADDRESS_LINE_1 + "\n" +
                        UPDATED_DEPENDANT_ADDRESS_LINE_2 + "\n" +
                        UPDATED_DEPENDANT_TOWN_NAME + "\n" +
                        UPDATED_DEPENDANT_COUNTY_NAME + "\n" +
                        UPDATED_DEPENDANT_POST_CODE);
                break;
        }
    }

    @Then("^I will see (.*) radio button selected$")
    public void iWillSeeRadioButtonRadioButtonSelected(String radioButton) {
        switch (radioButton) {
            case "No":
                Assert.assertTrue(dependantSameAddressPage.isNoRadioButtonSelected());
                break;
            case "Yes":
                Assert.assertTrue(dependantSameAddressPage.isYesRadioButtonSelected());
                break;
        }
    }

    @When("^I change my answer to (.*)$")
    public void iChangeMyAnswerToRadioButton(String radioButton) {
        dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^my dependant's address is (.*) as applicant's address$")
    public void myDependantSAddressIsUpdatedAsApplicantSAddress(String change) {
        Assert.assertEquals(checkDependantsPage.getDisplayedAddress(), ADDRESS_LINE_1 + "\n" +
                ADDRESS_LINE_2 + "\n" +
                TOWN_NAME + "\n" +
                COUNTY_NAME + "\n" +
                POST_CODE);
    }
    @When("My dependant is not eligible for IHS reimbursement")
    public void myDependantIsNotEligibleForIHSReimbursement() throws IOException, InterruptedException {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectYesRadioButtonAndClickContinue();
    }
}