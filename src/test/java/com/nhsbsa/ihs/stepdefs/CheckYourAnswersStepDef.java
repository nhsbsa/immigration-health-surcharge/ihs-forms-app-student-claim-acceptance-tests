package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;
import static com.nhsbsa.ihs.stepdefs.Constants.DEPENDANT_POST_CODE;


public class CheckYourAnswersStepDef {
    private WebDriver driver;
    private CheckYourAnswersPage checkYourAnswersPage;
    private CommonPage commonPage;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private IHSNumberPage ihsNumberPage;
    private VisaShareCodePage visaShareCodePage;
    private EnterAddressPage enterAddressPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;
    private UploadEHICFilePage uploadEHICFilePage;
    private UploadCASFilePage uploadCASFilePage;
    private AddDependantPage addDependantPage;
    private DependantPaidWorkPage dependantPaidWorkPage;
    private DependantLivingInUKPage dependantLivingInUKPage;
    private DependantNamePage dependantNamePage;
    private DependantDateOfBirthPage dependantDateOfBirthPage;
    private DependantIHSNumberPage dependantIHSNumberPage;
    private DependantVisaShareCodePage dependantVisaShareCodePage;
    private DependantSameAddressPage dependantSameAddressPage;
    private DependantEnterAddressPage dependantEnterAddressPage;
    private UploadDependantEHICFilePage uploadDependantEHICFilePage;
    private CheckDependantsPage checkDependantsPage;


    public CheckYourAnswersStepDef() {
        driver = Config.getDriver();
        checkYourAnswersPage = new CheckYourAnswersPage(driver);
        commonPage = new CommonPage(driver);
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        visaShareCodePage = new VisaShareCodePage(driver);
        enterAddressPage = new EnterAddressPage(driver);
        emailPage = new EmailPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        uploadEHICFilePage = new UploadEHICFilePage(driver);
        uploadCASFilePage = new UploadCASFilePage(driver);
        addDependantPage = new AddDependantPage(driver);
        dependantPaidWorkPage = new DependantPaidWorkPage(driver);
        dependantLivingInUKPage = new DependantLivingInUKPage(driver);
        dependantNamePage = new DependantNamePage(driver);
        dependantDateOfBirthPage = new DependantDateOfBirthPage(driver);
        dependantIHSNumberPage = new DependantIHSNumberPage(driver);
        dependantVisaShareCodePage = new DependantVisaShareCodePage(driver);
        dependantSameAddressPage = new DependantSameAddressPage(driver);
        dependantEnterAddressPage = new DependantEnterAddressPage(driver);
        uploadDependantEHICFilePage = new UploadDependantEHICFilePage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
    }

    @And("^My applicant details are captured until Check your answers screen$")
    public void myApplicantDetailsAreCapturedUntilCheckYourAnswersScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME, FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY, DOB_MONTH, DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectNoRadioButtonAndClickContinue();
    }

    @And("^My details are captured until Check your answers screen with Dependant$")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreenWithDependant() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "1-payslip_May Name" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(DEPENDANT_IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(DEPENDANT_VISA_SHARE_CODE);
        dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
        uploadDependantEHICFilePage.continueOnViewEHIC();
        checkDependantsPage.selectNoRadioButtonAndClickContinue();
    }

    @And("^My details are captured until Check your answers screen with different address for Dependant$")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreenWithDifferentAddressForDependant() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "1-payslip_May Name" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(DEPENDANT_IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(DEPENDANT_VISA_SHARE_CODE);
        dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
        dependantEnterAddressPage.enterAddressAndSubmit(DEPENDANT_ADDRESS_LINE_1,DEPENDANT_ADDRESS_LINE_2,DEPENDANT_TOWN_NAME,DEPENDANT_COUNTY_NAME,DEPENDANT_POST_CODE);
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
        uploadDependantEHICFilePage.continueOnViewEHIC();
        checkDependantsPage.selectNoRadioButtonAndClickContinue();
    }

    @When("^I select (.*) on Check Your Answers page$")
    public void iSelectChangeLinkOnCheckYourAnswersPage(String changeLink) {
        switch (changeLink) {
            case "Change Name link":
                checkYourAnswersPage.claimNameChangeLink();
                break;
            case "Change Date of birth link":
                checkYourAnswersPage.claimDOBChangeLink();
                break;
            case "Change IHS number link":
                checkYourAnswersPage.claimIhsNumberChangeLink();
                break;
            case "Change Visa Share Code link":
                checkYourAnswersPage.claimVisaShareCodeChangeLink();
                break;
            case "Change Address link":
                checkYourAnswersPage.claimAddressChangeLink();
                break;
            case "Change Email address link":
                checkYourAnswersPage.claimEmailChangeLink();
                break;
            case "Change Phone number link":
                checkYourAnswersPage.claimPhoneNumberChangeLink();
                break;
            case "Change Name link for Dependant 1":
                checkYourAnswersPage.depNameChangeLink();
                break;
            case "Change Date of birth link for Dependant 1":
                checkYourAnswersPage.depDOBChangeLink();
                break;
            case "Change IHS number link for Dependant 1":
                checkYourAnswersPage.depIHSNumberChangeLink();
                break;
            case "Change Visa Share Code link for Dependant 1":
                checkYourAnswersPage.depVisaShareCodeChangeLink();
                break;
            case "Change Address link for Dependant 1":
                checkYourAnswersPage.depAddressChangeLink();
                break;
            case "Change File uploaded link":
                checkYourAnswersPage.evidenceChangeLink();
                break;
            case "Change Files Uploaded link for Dependant 1":
                checkYourAnswersPage.depFilesUploadedChangeLink();
                break;

        }
    }

    @When("^I continue without changing my answer on (.*)$")
    public void iContinueWithoutChangingMyAnswerOnScreen(String output) {
        switch (output) {
            case "Name screen":
                namePage.continueButton();
                break;
            case "Date of birth screen":
                dateOfBirthPage.continueButton();
                break;
            case "IHS Number screen":
                ihsNumberPage.continueButton();
                break;
            case "Visa Share Code screen":
                visaShareCodePage.continueButton();
                break;
            case "Enter Address screen":
                enterAddressPage.continueButton();
                break;
            case "Email Address screen":
                emailPage.continueButton();
                break;
            case "Phone number screen":
                mobileNumberPage.continueButton();
                break;
            case "Dependant Living in UK screen":
                dependantLivingInUKPage.continueButton();
                break;
            case "Dependant Name screen":
                dependantNamePage.continueButton();
                break;
            case "Dependant Date of birth screen":
                dependantDateOfBirthPage.continueButton();
                break;
            case "Dependant IHS Number screen":
                dependantIHSNumberPage.continueButton();
                break;
            case "Dependant Visa Share Code screen":
                dependantVisaShareCodePage.continueButton();
                break;
            case "Same Address as Applicant screen":
                dependantSameAddressPage.continueButton();
                break;
            case "Enter Dependant Address screen":
                dependantEnterAddressPage.continueButton();
                break;
            case "View Uploaded Dependant EHIC screen":
                uploadDependantEHICFilePage.continueButton();
                break;
        }
    }

    @And("^My answer remains same as on (.*)$")
    public void myAnswerRemainsSameAsOnScreen(String screen) {
        switch (screen) {
            case "Name screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantName(),GIVEN_NAME +" "+ FAMILY_NAME);
                break;
            case "Date of birth screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantDOB(),DOB_DAY +" "+ DOB_FULL_MONTH +" "+ DOB_YEAR);
                break;
            case "IHS Number screen":
                Assert.assertEquals(IHS_NUMBER, checkYourAnswersPage.getApplicantIHSNumber());
                break;
            case "Visa Share Code screen":
                Assert.assertEquals(VISA_SHARE_CODE, checkYourAnswersPage.getApplicantVisaShareCode());
                break;
            case "Address screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantAddress(), ADDRESS_LINE_1 + "\n" +
                        ADDRESS_LINE_2 + "\n" +
                        TOWN_NAME + "\n" +
                        COUNTY_NAME + "\n +" +
                        POST_CODE);
                break;
            case "Email address screen":
                Assert.assertEquals(EMAIL_ADDRESS, checkYourAnswersPage.getApplicantEmail());
                break;
            case "Phone number screen":
                Assert.assertEquals(PHONE_NUMBER, checkYourAnswersPage.getApplicantPhoneNumber());
                break;
            case "Dependant Name screen":
                Assert.assertEquals(checkYourAnswersPage.getDepName(), DEPENDANT_GIVEN_NAME + " " + DEPENDANT_FAMILY_NAME);
                break;
            case "Dependant Date of birth screen":
                Assert.assertEquals(checkYourAnswersPage.getDepDOB(), DEPENDANT_DOB_DAY + " " + DEPENDANT_DOB_FULL_MONTH + " " + DEPENDANT_DOB_YEAR);
                break;
            case "Dependant IHS Number screen":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER, checkYourAnswersPage.getDepIHSNumber());
                break;
            case "Dependant Visa Share Code screen":
                Assert.assertEquals(DEPENDANT_VISA_SHARE_CODE, checkYourAnswersPage.getDepVisaShareCode());
                break;
            case "Same Address as Applicant screen":
                Assert.assertTrue(dependantSameAddressPage.isYesRadioButtonSelected());
                Assert.assertEquals(checkYourAnswersPage.getDepAddress(), ADDRESS_LINE_1 + "\n" +
                        ADDRESS_LINE_2 + "\n" +
                        TOWN_NAME + "\n" +
                        COUNTY_NAME + "\n +" +
                        POST_CODE);
                break;
            case "Enter Dependant Address screen":
                Assert.assertEquals(checkYourAnswersPage.getDepAddress(), ADDRESS_LINE_1 + "\n" +
                        ADDRESS_LINE_2 + "\n" +
                        TOWN_NAME + "\n" +
                        COUNTY_NAME + "\n" +
                        POST_CODE);
                break;
        }
    }

    @And("^My answer remains same on (.*) having different address$")
    public void myAnswerRemainsSameOnScreenHavingDifferentAddress(String screen) {
        switch (screen) {
            case "Name screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantName(),GIVEN_NAME +" "+ FAMILY_NAME);
                break;
            case "Date of birth screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantDOB(),DOB_DAY +" "+ DOB_FULL_MONTH +" "+ DOB_YEAR);
                break;
            case "IHS Number screen":
                Assert.assertEquals(IHS_NUMBER, checkYourAnswersPage.getApplicantIHSNumber());
                break;
            case "Visa Share Code screen":
                Assert.assertEquals(VISA_SHARE_CODE, checkYourAnswersPage.getApplicantVisaShareCode());
                break;
            case "Address screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantAddress(), ADDRESS_LINE_1 + "\n" +
                        ADDRESS_LINE_2 + "\n" +
                        TOWN_NAME + "\n" +
                        COUNTY_NAME + "\n +" +
                        POST_CODE);
                break;
            case "Email address screen":
                Assert.assertEquals(EMAIL_ADDRESS, checkYourAnswersPage.getApplicantEmail());
                break;
            case "Phone number screen":
                Assert.assertEquals(PHONE_NUMBER, checkYourAnswersPage.getApplicantPhoneNumber());
                break;
            case "Dependant Name screen":
                Assert.assertEquals(checkYourAnswersPage.getDepName(), DEPENDANT_GIVEN_NAME + " " + DEPENDANT_FAMILY_NAME);
                break;
            case "Dependant Date of birth screen":
                Assert.assertEquals(checkYourAnswersPage.getDepDOB(), DEPENDANT_DOB_DAY + " " + DEPENDANT_DOB_FULL_MONTH + " " + DEPENDANT_DOB_YEAR);
                break;
            case "Dependant IHS Number screen":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER, checkYourAnswersPage.getDepIHSNumber());
                break;
            case "Dependant Visa Share Code screen":
                Assert.assertEquals(DEPENDANT_VISA_SHARE_CODE, checkYourAnswersPage.getDepVisaShareCode());
                break;
            case "Enter Dependant Address screen":
                Assert.assertEquals(checkYourAnswersPage.getDepAddress(), DEPENDANT_ADDRESS_LINE_1 + "\n" +
                        DEPENDANT_ADDRESS_LINE_2 + "\n" +
                        DEPENDANT_TOWN_NAME + "\n" +
                        DEPENDANT_COUNTY_NAME + "\n" +
                        DEPENDANT_POST_CODE);
                break;
        }
    }

    @And("^My claimant's (.*) is pre-populated$")
    public void myClaimantSAnswerIsPrePopulated(String answer) {
        switch (answer) {
            case "Name":
                Assert.assertEquals(GIVEN_NAME, namePage.getEnteredGivenName());
                Assert.assertEquals(FAMILY_NAME, namePage.getEnteredFamilyName());
                break;
            case "Date of birth":
                Assert.assertEquals(DOB_DAY, dateOfBirthPage.getEnteredDay());
                Assert.assertEquals(DOB_MONTH, dateOfBirthPage.getEnteredMonth());
                Assert.assertEquals(DOB_YEAR, dateOfBirthPage.getEnteredYear());
                break;
            case "IHS Number":
                Assert.assertEquals(IHS_NUMBER, ihsNumberPage.getEnteredIHSNumber());
                break;
            case "Visa Share Code":
                Assert.assertEquals(VISA_SHARE_CODE, visaShareCodePage.getEnteredVisaShareCode());
                break;
            case "Address":
                Assert.assertEquals(ADDRESS_LINE_1, enterAddressPage.getEnteredAddressLine1());
                Assert.assertEquals(ADDRESS_LINE_2, enterAddressPage.getEnteredAddressLine2());
                Assert.assertEquals(TOWN_NAME, enterAddressPage.getEnteredTown());
                Assert.assertEquals(COUNTY_NAME, enterAddressPage.getEnteredCounty());
                Assert.assertEquals(POST_CODE, enterAddressPage.getEnteredPostCode());
                break;
            case "Email":
                Assert.assertEquals(EMAIL_ADDRESS, emailPage.getEnteredEmail());
                break;
            case "Phone":
                Assert.assertEquals(PHONE_NUMBER, mobileNumberPage.getEnteredPhoneNumber());
            case "Dep Name":
                Assert.assertEquals(DEPENDANT_GIVEN_NAME, dependantNamePage.getEnteredGivenName());
                Assert.assertEquals(DEPENDANT_FAMILY_NAME, dependantNamePage.getEnteredFamilyName());
                break;
            case "Dep Date of Birth":
                Assert.assertEquals(DEPENDANT_DOB_DAY, dependantDateOfBirthPage.getEnteredDay());
                Assert.assertEquals(DEPENDANT_DOB_MONTH, dependantDateOfBirthPage.getEnteredMonth());
                Assert.assertEquals(DEPENDANT_DOB_YEAR, dependantDateOfBirthPage.getEnteredYear());
                break;
            case "Dep IHS Number":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER, dependantIHSNumberPage.getEnteredIHSNumber());
                break;
            case "Dep Visa Share Code":
                Assert.assertEquals(DEPENDANT_VISA_SHARE_CODE, dependantVisaShareCodePage.getEnteredShareCode());
                break;
            case "Dep Same Address":
                Assert.assertEquals(true, dependantSameAddressPage.isYesRadioButtonSelected());
            case "Dep With Same Address":
                Assert.assertEquals(ADDRESS_LINE_1, dependantEnterAddressPage.getEnteredAddressLine1());
                Assert.assertEquals(ADDRESS_LINE_2, dependantEnterAddressPage.getEnteredAddressLine2());
                Assert.assertEquals(TOWN_NAME, dependantEnterAddressPage.getEnteredTown());
                Assert.assertEquals(COUNTY_NAME, dependantEnterAddressPage.getEnteredCounty());
                Assert.assertEquals(POST_CODE, dependantEnterAddressPage.getEnteredPostCode());
                break;
            case "Dep With Different Address":
                Assert.assertEquals(DEPENDANT_ADDRESS_LINE_1, dependantEnterAddressPage.getEnteredAddressLine1());
                Assert.assertEquals(DEPENDANT_ADDRESS_LINE_2, dependantEnterAddressPage.getEnteredAddressLine2());
                Assert.assertEquals(DEPENDANT_TOWN_NAME, dependantEnterAddressPage.getEnteredTown());
                Assert.assertEquals(DEPENDANT_COUNTY_NAME, dependantEnterAddressPage.getEnteredCounty());
                Assert.assertEquals(DEPENDANT_POST_CODE, dependantEnterAddressPage.getEnteredPostCode());
                break;
        }
    }

    @When("^I change my (.*) and continue$")
    public void iChangeMyAnswerAndContinue(String answer) {
        switch (answer){
            case "Name":
                namePage.enterNameAndSubmit(UPDATED_GIVEN_NAME,UPDATED_FAMILY_NAME);
                break;
            case "Date of birth":
                dateOfBirthPage.enterDateOfBirthAndSubmit(UPDATED_DOB_DAY,UPDATED_DOB_MONTH,UPDATED_DOB_YEAR);
                break;
            case "IHS Number":
                ihsNumberPage.enterIHSNumberAndSubmit(UPDATED_IHS_NUMBER);
                break;
            case "Visa Share Code":
                visaShareCodePage.enterVisaShareCodeAndSubmit(UPDATED_VISA_SHARE_CODE);
                break;
            case "Address":
                enterAddressPage.enterAddressAndSubmit(UPDATED_ADDRESS_LINE_1,UPDATED_ADDRESS_LINE_2,UPDATED_TOWN_NAME,UPDATED_COUNTY_NAME,UPDATED_POST_CODE);
                break;
            case "Email":
                emailPage.enterEmailAndSubmit(UPDATED_EMAIL_ADDRESS);
                break;
            case "Phone Number":
                mobileNumberPage.enterMobileNumberAndSubmit(UPDATED_PHONE_NUMBER);
                break;
            case "Dep Name":
                dependantNamePage.enterNameAndSubmit(UPDATED_DEPENDANT_GIVEN_NAME,UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "Dep Date of Birth":
                dependantDateOfBirthPage.enterDateOfBirthAndSubmit(UPDATED_DEPENDANT_DOB_DAY,UPDATED_DEPENDANT_DOB_MONTH,UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "Dep IHS Number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit(UPDATED_DEPENDANT_IHS_NUMBER);
                break;
            case "Dep Visa Share Code":
                dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(UPDATED_DEPENDANT_VISA_SHARE_CODE);
                break;
            case "Dep With Same Address":
                dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
                break;
            case "Dep With Different Address":
                dependantEnterAddressPage.enterAddressAndSubmit(UPDATED_DEPENDANT_ADDRESS_LINE_1, UPDATED_DEPENDANT_ADDRESS_LINE_2, UPDATED_DEPENDANT_TOWN_NAME, UPDATED_DEPENDANT_COUNTY_NAME, UPDATED_DEPENDANT_POST_CODE);
                break;
        }
    }

    @And("^I see the updated (.*) on Check Your Answers screen$")
    public void iSeeTheUpdatedAnswersOnCheckYourAnswersScreen(String answer) {
        switch (answer) {
            case "Name":
                Assert.assertEquals(checkYourAnswersPage.getApplicantName(),UPDATED_GIVEN_NAME + " " + UPDATED_FAMILY_NAME);
                break;
            case "Date of birth":
                Assert.assertEquals(checkYourAnswersPage.getApplicantDOB(), UPDATED_DOB_DAY + " " + UPDATED_FULL_DOB_MONTH + " " + UPDATED_DOB_YEAR);
                break;
            case "IHS Number":
                Assert.assertEquals(UPDATED_IHS_NUMBER, checkYourAnswersPage.getApplicantIHSNumber());
                break;
            case "Visa Share Code":
                Assert.assertEquals(UPDATED_VISA_SHARE_CODE, checkYourAnswersPage.getApplicantVisaShareCode());
                break;
            case "Address":
                Assert.assertEquals(checkYourAnswersPage.getApplicantAddress(), UPDATED_ADDRESS_LINE_1 + "\n" +
                        UPDATED_ADDRESS_LINE_2 + "\n" +
                        UPDATED_TOWN_NAME + "\n" +
                        UPDATED_COUNTY_NAME + "\n" +
                        UPDATED_POST_CODE);
                break;
            case "Email":
                Assert.assertEquals(UPDATED_EMAIL_ADDRESS, checkYourAnswersPage.getApplicantEmail());
                break;
            case "Phone Number":
                Assert.assertEquals(UPDATED_PHONE_NUMBER, checkYourAnswersPage.getApplicantPhoneNumber());
                break;
            case "Dep Name":
                Assert.assertEquals(checkYourAnswersPage.getDepName(),UPDATED_DEPENDANT_GIVEN_NAME + " " + UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "Dep Date of Birth":
                Assert.assertEquals(checkYourAnswersPage.getDepDOB(), UPDATED_DEPENDANT_DOB_DAY + " " + UPDATED_DEPENDANT_DOB_FULL_MONTH + " " + UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "Dep IHS Number":
                Assert.assertEquals(UPDATED_DEPENDANT_IHS_NUMBER, checkYourAnswersPage.getDepIHSNumber());
                break;
            case "Dep Visa Share Code":
                Assert.assertEquals(UPDATED_DEPENDANT_VISA_SHARE_CODE, checkYourAnswersPage.getDepVisaShareCode());
                break;
            case "Dep With Same Address":
                Assert.assertEquals(checkYourAnswersPage.getDepAddress(), UPDATED_ADDRESS_LINE_1 + "\n" +
                        UPDATED_ADDRESS_LINE_2 + "\n" +
                        UPDATED_TOWN_NAME + "\n" +
                        UPDATED_COUNTY_NAME + "\n" +
                        UPDATED_POST_CODE);
                break;
            case "Dep With Different Address":
                Assert.assertEquals(checkYourAnswersPage.getDepAddress(), UPDATED_DEPENDANT_ADDRESS_LINE_1 + "\n" +
                        UPDATED_DEPENDANT_ADDRESS_LINE_2 + "\n" +
                        UPDATED_DEPENDANT_TOWN_NAME + "\n" +
                        UPDATED_DEPENDANT_COUNTY_NAME + "\n" +
                        UPDATED_DEPENDANT_POST_CODE);
                break;
        }

    }

    @And("^I change my (.*) and do not continue$")
    public void iChangeMyAnswerAndDoNotContinue(String answer) {
        switch (answer){
            case "Name":
                namePage.enterName(UPDATED_GIVEN_NAME, UPDATED_FAMILY_NAME);
                break;
            case "Date of birth":
                dateOfBirthPage.enterDateOfBirth(UPDATED_DOB_DAY, UPDATED_DOB_MONTH, UPDATED_DOB_YEAR);
                break;
            case "IHS Number":
                ihsNumberPage.enterIHSNumber(UPDATED_IHS_NUMBER);
                break;
            case "Visa Share Code":
                visaShareCodePage.enterVisaShareCode(UPDATED_VISA_SHARE_CODE);
                break;
            case "Address":
                enterAddressPage.enterAddress(UPDATED_ADDRESS_LINE_1,UPDATED_ADDRESS_LINE_2,UPDATED_TOWN_NAME,UPDATED_COUNTY_NAME,UPDATED_POST_CODE);
                break;
            case "Email":
                emailPage.enterEmail(UPDATED_EMAIL_ADDRESS);
                break;
            case "Phone Number":
                mobileNumberPage.enterMobileNumber(UPDATED_PHONE_NUMBER);
                break;
            case "Dep Name":
                dependantNamePage.enterName(UPDATED_DEPENDANT_GIVEN_NAME,UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "Dep Date of Birth":
                dependantDateOfBirthPage.enterDateOfBirth(UPDATED_DOB_DAY, UPDATED_DOB_MONTH, UPDATED_DOB_YEAR);
                break;
            case "Dep IHS Number":
                dependantIHSNumberPage.enterIHSNumber(UPDATED_IHS_NUMBER);
                break;
            case "Dep Visa Share Code":
                dependantVisaShareCodePage.enterVisaShareCode(UPDATED_VISA_SHARE_CODE);
                break;
            case "Dep Address":
                dependantEnterAddressPage.enterAddress(UPDATED_DEPENDANT_ADDRESS_LINE_1,UPDATED_DEPENDANT_ADDRESS_LINE_2,UPDATED_DEPENDANT_TOWN_NAME,UPDATED_DEPENDANT_COUNTY_NAME,UPDATED_DEPENDANT_POST_CODE);
                break;
        }

    }

    @When("^I change the answer to (.*)$")
    public void iChangeTheAnswerToInvalidAnswer(String invalidAnswer) {
        switch (invalidAnswer) {
            case "Blank Given Name":
                namePage.enterNameAndSubmit("", UPDATED_FAMILY_NAME);
                break;
            case "Blank Family Name":
                namePage.enterNameAndSubmit(UPDATED_GIVEN_NAME, "");
                break;
            case "Invalid Given Name":
                namePage.enterNameAndSubmit("(first name)", UPDATED_FAMILY_NAME);
                break;
            case "Blank Date of birth":
                dateOfBirthPage.enterDateOfBirthAndSubmit("", "", "");
                break;
            case "Invalid Date of birth":
                dateOfBirthPage.enterDateOfBirthAndSubmit("32", "13", UPDATED_DOB_YEAR);
                break;
            case "Blank IHS Number":
                ihsNumberPage.enterIHSNumberAndSubmit("");
                break;
            case "Invalid IHS Number":
                ihsNumberPage.enterIHSNumberAndSubmit("abc@12345656abc");
                break;
            case "Invalid range IHS Number":
                ihsNumberPage.enterIHSNumberAndSubmit("ihsc1235656");
                break;
            case "Blank Visa Share Code":
                visaShareCodePage.enterVisaShareCodeAndSubmit("");
                break;
            case "Invalid Visa Share Code":
                visaShareCodePage.enterVisaShareCodeAndSubmit("A12:345:67Z");
                break;
            case "Blank Building":
                enterAddressPage.enterAddressAndSubmit("", UPDATED_ADDRESS_LINE_2, UPDATED_TOWN_NAME, UPDATED_COUNTY_NAME, UPDATED_POST_CODE);
                break;
            case "Blank Town":
                enterAddressPage.enterAddressAndSubmit(UPDATED_ADDRESS_LINE_1, UPDATED_ADDRESS_LINE_2, "", UPDATED_COUNTY_NAME, UPDATED_POST_CODE);
                break;
            case "Invalid Postcode":
                enterAddressPage.enterAddressAndSubmit(UPDATED_ADDRESS_LINE_1, UPDATED_ADDRESS_LINE_2, UPDATED_TOWN_NAME, UPDATED_COUNTY_NAME, "SW1A2 1AA");
                break;
            case "Blank Email":
                emailPage.enterEmailAndSubmit("");
                break;
            case "Invalid Email":
                emailPage.enterEmailAndSubmit("testemail");
                break;
            case "Invalid Phone Number":
                mobileNumberPage.enterMobileNumberAndSubmit("*7653456789");
                break;
            case "Blank Dependant Given Name":
                dependantNamePage.enterNameAndSubmit("", UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "Invalid Dependant Family Name":
                dependantNamePage.enterNameAndSubmit(UPDATED_DEPENDANT_GIVEN_NAME, "last name 123");
                break;
            case "Blank Dependant Date of Birth":
                dependantDateOfBirthPage.enterDateOfBirthAndSubmit("", "", "");
                break;
            case "Invalid Dependant Date of Birth":
                dependantDateOfBirthPage.enterDateOfBirthAndSubmit("dd", "mm", "yyyy");
                break;
            case "Blank Dependant IHS Number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit("");
                break;
            case "Invalid Dependant IHS Number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit("abc123456789");
                break;
            case "Invalid range Dependant IHS number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit("ihs92345678");
                break;
            case "Blank Dependant Visa Share Code":
                dependantVisaShareCodePage.enterVisaShareCodeAndSubmit("");
                break;
            case "Invalid Dependant Visa Share Code":
                dependantVisaShareCodePage.enterVisaShareCodeAndSubmit("A12-345-67Z");
                break;
            case "Blank Dependant Post Code":
                dependantEnterAddressPage.enterAddressAndSubmit(UPDATED_DEPENDANT_ADDRESS_LINE_1, UPDATED_DEPENDANT_ADDRESS_LINE_2, UPDATED_DEPENDANT_TOWN_NAME, UPDATED_DEPENDANT_COUNTY_NAME, "");
                break;
            case "Invalid Dependant Building":
                dependantEnterAddressPage.enterAddressAndSubmit("***", UPDATED_DEPENDANT_ADDRESS_LINE_2, UPDATED_DEPENDANT_TOWN_NAME, UPDATED_DEPENDANT_COUNTY_NAME, UPDATED_DEPENDANT_POST_CODE);
                break;
        }
    }

    @Then("^I will see file (.*) on Check Your Answers page$")
    public void iWillSeeNewlyUploadedFile(String fileName) {
        Assert.assertTrue(checkYourAnswersPage.getEvidence().contains(fileName));
    }

    @And("^I see the updated CAS (.*) and EHIC (.*) on Check Your Answers page")
    public void iSeeTheUpdatedCASFileNameAndEHICFileNameOnCheckYourAnswersPage(String casFileName, String ehicFileName) {
        Assert.assertTrue(checkYourAnswersPage.getEvidence().contains(casFileName));
        Assert.assertTrue(checkYourAnswersPage.getEvidence().contains(ehicFileName));
    }

    @And("^My dependant's uploaded EHIC file is pre-populated as (.*)(.*)$")
    public void myDependantSUploadedEHICFileIsPrePopulatedAsFileNameFileFormat(String fileName, String fileFormat) {
        Assert.assertEquals(uploadDependantEHICFilePage.getUploadedEHIC(),fileName + fileFormat);
    }

    @And("^My dependant's uploaded EHIC file remains same as (.*)(.*) on (.*)$")
    public void myDependantSUploadedEHICFileRemainsSameAsFileNameFileFormatOnScreen(String fileName, String fileFormat, String screen) {
        switch (screen){
            case "Check Dependant Details screen":
                Assert.assertEquals(checkDependantsPage.getDisplayedFilesUploaded(),fileName + fileFormat);
                break;
            case "Check Your Answers screen":
                Assert.assertEquals(checkYourAnswersPage.getDepFilesUploaded(),fileName + fileFormat);
                break;
        }
    }

    @And("^My dependant's uploaded EHIC file is changed to (.*)(.*) on (.*)$")
    public void myDependantSUploadedEHICFileIsChangedToFileNameFileFormatOnScreen(String fileName, String fileFormat, String screen) {
        switch (screen) {
            case "Check Dependant Details screen":
                Assert.assertEquals(checkDependantsPage.getDisplayedFilesUploaded(), fileName + fileFormat);
                break;
            case "Check Your Answers screen":
                Assert.assertEquals(checkYourAnswersPage.getDepFilesUploaded(),fileName + fileFormat);
                break;
        }
    }
}
