package com.nhsbsa.ihs.stepdefs;

import com.deque.html.axecore.results.Results;
import com.deque.html.axecore.results.Rule;
import com.deque.html.axecore.selenium.AxeBuilder;
import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import com.nhsbsa.ihs.utilities.ConfigReader;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.en.Given;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.json.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Accessibility {

    private WebDriver driver;
    private String baseUrl;
    String currentBrowser;
    private static final String reportPath = System.getProperty("user.dir") + File.separator + "target" + File.separator;
    String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new java.util.Date());
    String filePath = reportPath + "accessibilityReport_" + timeStamp + ".csv";
    static List<Map<String, String>> chromeResult = new ArrayList<>();
    static List<Map<String, String>> firefoxResult = new ArrayList<>();

    private final By waveError = By.xpath("//li[@id='error']/span");
    private final By contrastNum = By.xpath("//li[@id='contrastnum']/span");
    private final By alert = By.xpath("//li[@id='alert']/span");
    private final By feature = By.xpath("//li[@id='feature']/span");
    private final By structure = By.xpath("//li[@id='structure']/span");
    private final By ariaLocator = By.xpath("//li[@id='aria']/span");

    public Accessibility() {
        baseUrl = ConfigReader.getEnvironment();
    }

    @Given("^I launch the IHS Student claim application on (.*)$")
    public void iLaunchTheIHSStudentClaimApplication(String browser) {
        switch (browser) {
            case "chrome":
                driver = chromeDriver();
                break;
            case "firefox":
                driver = firefoxDriver();
                break;
        }
        driver.manage().deleteAllCookies();
        new Page(driver).navigateToUrl(baseUrl);
        currentBrowser = browser;
    }

    WebDriver chromeDriver() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        options.addArguments("--remote-debugging-port=9222");
        options.addArguments("--headless");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(50));
        driver.manage().window().maximize();
        Config.setWebDriver(driver);
        return driver;
    }

    WebDriver firefoxDriver() {
        ProfilesIni profileIni = new ProfilesIni();
        FirefoxProfile pro = profileIni.getProfile("WaveExtensionProfile");
        FirefoxOptions options = new FirefoxOptions();
        options.setProfile(pro);
        options.addArguments("-headless");
        driver = new FirefoxDriver(options);
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(50));
        driver.manage().window().maximize();
        Config.setWebDriver(driver);
        return driver;
    }

    @AfterStep(value="@Accessibility")
    public void testAccessibility() throws Exception {
        if (currentBrowser.equals("chrome")) {
// Run Axe
            AxeBuilder axeBuilder = new AxeBuilder();
            Results axeResults = axeBuilder.analyze(driver);
            String url = axeResults.getUrl();
            List<Rule> axeIssues = axeResults.getViolations();
            Map<String, String> map = new HashMap<>();
            String impact = "";
            int count = 0;
            if (!axeIssues.isEmpty()) {
                for (Rule rule : axeIssues) {
                    impact = rule.getImpact();
                    count = rule.getNodes().size();
                    if (map.containsKey(impact)) {
                        count = count + Integer.parseInt(map.get(impact));
                        map.put(impact, String.valueOf(count));
                    } else {
                        map.put(impact, String.valueOf(count));
                    }
                }
                map.put("URL", url);
            }

// Run LightHouse on desktop
            String URL = driver.getCurrentUrl();
            ProcessBuilder builder = new ProcessBuilder("lighthouse", URL, "--port=9222", "--preset=desktop",
                    "--only-categories=accessibility", "--quiet", "--output-path=./LightHouseReport.json", "--output=json");
// Run LightHouse on mobile
            ProcessBuilder builder2 = new ProcessBuilder("lighthouse", URL, "--port=9222", "--form-factor=mobile",
                    "--only-categories=accessibility", "--quiet", "--output-path=./LightHouseReport.json", "--output=json");
            List<ProcessBuilder> list = new ArrayList<>();
            list.add(builder);
            list.add(builder2);
            List<String> score = new ArrayList<>();
            for (ProcessBuilder obj : list) {
                obj.redirectErrorStream(true);
                Process p = obj.start();
                BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line;
                do {
                    line = r.readLine();
                } while (line != null);

                String jsonString = new String(Files.readAllBytes(Paths.get("./LightHouseReport.json")));
                JSONObject jsonObject = new JSONObject(jsonString);
                score.add(jsonObject.getJSONObject("categories").getJSONObject("accessibility").get("score").toString());
            }
            map.put("Desktop", score.get(0));
            map.put("Mobile", score.get(1));
            chromeResult.add(map);
        }
// Run Wave
        else if (currentBrowser.equals("firefox")) {
            Actions keyAction = new Actions(driver);
            ((JavascriptExecutor) driver).executeScript("window.open('www.mozilla.org/','_blank');");
            ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
            driver.switchTo().window(tabs.get(1));
            driver.switchTo().window(tabs.get(0));

            ((HasContext) driver).setContext(FirefoxCommandContext.CHROME);
            driver.switchTo().window(tabs.get(0));
            keyAction.keyDown(Keys.CONTROL).sendKeys("w").keyUp(Keys.CONTROL).build().perform();
            Thread.sleep(2000);
            ((HasContext) driver).setContext(FirefoxCommandContext.CONTENT);
            driver.switchTo().window(tabs.get(0));

            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
            wait.until(ExpectedConditions.presenceOfElementLocated(By.className("wave5icon")));
            driver.switchTo().frame("wave_sidebar_container");
            String error = driver.findElement(waveError).getText();
            String contrastError = driver.findElement(contrastNum).getText();
            String alerts = driver.findElement(alert).getText();
            String features = driver.findElement(feature).getText();
            String structuralElements = driver.findElement(structure).getText();
            String aria = driver.findElement(ariaLocator).getText();

            driver.switchTo().parentFrame();

            Map<String, String> map = new HashMap<>();
            String URL = driver.getCurrentUrl();
            map.put("URL", URL);
            map.put("Error", error);
            map.put("Contrast Error", contrastError);
            map.put("Alerts", alerts);
            map.put("Features", features);
            map.put("Structural Elements", structuralElements);
            map.put("Aria", aria);
            firefoxResult.add(map);
        }
    }

    public void createCSVReport() {
        List<HashMap<String, String>> result = chromeResult.stream()
                .flatMap(m1 -> firefoxResult.stream()
                        .filter(y -> m1.get("URL").equals(y.get("URL")))
                        .map(m2 -> new HashMap<String, String>() {{
                            putAll(m1);
                            putAll(m2);
                        }}))
                .collect(Collectors.toList());
        result = result.stream().distinct().collect(Collectors.toList());
        CSVPrinter csv = null;
        try {
            csv = new CSVPrinter(new FileWriter(filePath,true), CSVFormat.EXCEL);
            csv.printRecord("URL", "Wave Error", "Contrast Error", "Alerts", "Features", "SE", "Aria",
                    "LightHouse Mobile", "LightHouse Desktop", "Axe Critical", "Serious", "Moderate", "Minor");
            for (Map<String, String> map : result) {
                ArrayList<String> nextLine = new ArrayList<>();
                nextLine.add(map.get("URL"));
                nextLine.add(map.get("Error"));
                nextLine.add(map.get("Contrast Error"));
                nextLine.add(map.get("Alerts"));
                nextLine.add(map.get("Features"));
                nextLine.add(map.get("Structural Elements"));
                nextLine.add(map.get("Aria"));
                nextLine.add(map.get("Mobile"));
                nextLine.add(map.get("Desktop"));
                nextLine.add(map.get("critical"));
                nextLine.add(map.get("serious"));
                nextLine.add(map.get("moderate"));
                nextLine.add(map.get("minor"));
                csv.printRecord(nextLine);
            }
            csv.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After(value="@Accessibility")
    public void closeTheBrowser() {
        Page page = new Page(driver);
        page.tearDownDriver();
    }
}