package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import com.nhsbsa.ihs.utilities.ConfigReader;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;

public class BackgroundStepDef {

    private WebDriver driver;
    private String baseUrl;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private IHSNumberPage ihsNumberPage;
    private VisaShareCodePage visaShareCodePage;
    private EnterAddressPage enterAddressPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;
    private UploadEHICFilePage uploadEHICFilePage;
    private UploadCASFilePage uploadCASFilePage;
    private AddDependantPage addDependantPage;
    private DependantPaidWorkPage dependantPaidWorkPage;
    private DependantLivingInUKPage dependantLivingInUKPage;
    private DependantNamePage dependantNamePage;
    private DependantDateOfBirthPage dependantDateOfBirthPage;
    private DependantIHSNumberPage dependantIHSNumberPage;
    private DependantVisaShareCodePage dependantVisaShareCodePage;
    private DependantSameAddressPage dependantSameAddressPage;
    private DependantEnterAddressPage dependantEnterAddressPage;
    private UploadDependantEHICFilePage uploadDependantEHICFilePage;
    private CheckDependantsPage checkDependantsPage;

    public BackgroundStepDef() {
        driver = Config.getDriver();
        baseUrl = ConfigReader.getEnvironment();
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        visaShareCodePage = new VisaShareCodePage(driver);
        enterAddressPage = new EnterAddressPage(driver);
        emailPage = new EmailPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        uploadEHICFilePage = new UploadEHICFilePage(driver);
        uploadCASFilePage = new UploadCASFilePage(driver);
        addDependantPage = new AddDependantPage(driver);
        dependantPaidWorkPage = new DependantPaidWorkPage(driver);
        dependantLivingInUKPage = new DependantLivingInUKPage(driver);
        dependantNamePage = new DependantNamePage(driver);
        dependantDateOfBirthPage = new DependantDateOfBirthPage(driver);
        dependantIHSNumberPage = new DependantIHSNumberPage(driver);
        dependantVisaShareCodePage = new DependantVisaShareCodePage(driver);
        dependantSameAddressPage = new DependantSameAddressPage(driver);
        dependantEnterAddressPage = new DependantEnterAddressPage(driver);
        uploadDependantEHICFilePage = new UploadDependantEHICFilePage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
    }

    @Given("^I launch the IHS Student claim application$")
    public void iLaunchTheIHSStudentClaimApplication() {
        driver.manage().deleteAllCookies();
        new Page(driver).navigateToUrl(baseUrl);
    }

    @And("^My details are captured until Date of birth screen$")
    public void myDetailsAreCapturedUntilDateOfBirthScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
    }

    @And("^My details are captured until IHS Number screen$")
    public void myDetailsAreCapturedUntilIHSNumberScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
    }

    @And("^My details are captured until Visa Share Code screen$")
    public void myDetailsAreCapturedUntilVisaShareCodeScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
    }

    @And("^My details are captured until Enter Address screen$")
    public void myDetailsAreCapturedUntilEnterAddressScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
    }

    @And("^My details are captured until Email Address screen$")
    public void myDetailsAreCapturedUntilEmailAddressScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
    }

    @And("^My details are captured until Phone Number screen$")
    public void myDetailsAreCapturedUntilPhoneNumberScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
    }

    @And("^My details are captured until Upload EHIC screen$")
    public void myDetailsAreCapturedUntilUploadEHICScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
    }

    @And("^My details are captured until Upload CAS screen$")
    public void myDetailsAreCapturedUntilUploadCASScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
    }

    @And("^My details are captured until Add Dependant screen$")
    public void myDetailsAreCapturedUntilAddDependantScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_5" + ".jpeg");
        uploadCASFilePage.continueOnViewCAS();
    }

    @And("^My details are captured until Dependant Paid Work screen$")
    public void myDetailsAreCapturedUntilDependantPaidWorkScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^My details are captured until Dependant Living in UK screen$")
    public void myDetailsAreCapturedUntilDependantLivingInUKScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
    }

    @And("^My details are captured until Dependant Name screen$")
    public void myDetailsAreCapturedUntilDependantNameScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^My details are captured until Dependant Date of birth screen$")
    public void myDetailsAreCapturedUntilDependantDateOfBirthScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
    }

    @And("^My details are captured until Dependant IHS Number screen$")
    public void myDetailsAreCapturedUntilDependantIHSNumberScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
    }

    @And("^My details are captured until Dependant Visa Share Code screen$")
    public void myDetailsAreCapturedUntilDependantVisaShareCodeScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
    }

    @And("^My details are captured until Same Address screen$")
    public void myDetailsAreCapturedUntilSameAddressScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
    }

    @And("^My details are captured until Dependant Address screen$")
    public void myDetailsAreCapturedUntilDependantAddressScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
    }

    @And("^My details are captured until Upload Dependant EHIC screen$")
    public void myDetailsAreCapturedUntilUploadDependantEHICScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
        dependantEnterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
    }

    @And("^My details are captured until Check Dependants screen$")
    public void myDetailsAreCapturedUntilCheckDependantsScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWorkPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
        uploadDependantEHICFilePage.continueOnViewEHIC();
    }

    @And("^My details are captured until Check your answers screen$")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME, FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY, DOB_MONTH, DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(ADDRESS_LINE_1, ADDRESS_LINE_2, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectNoRadioButtonAndClickContinue();
    }
}


