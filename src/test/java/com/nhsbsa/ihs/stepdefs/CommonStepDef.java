package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;


public class CommonStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private DependantNotEligiblePage dependantNotEligiblePage;
    private VisaShareCodePage visaShareCodePage;
    private CheckDependantsPage checkDependantsPage;
    private UploadCASFilePage uploadCASFilePage;
    private MobileNumberPage mobileNumberPage;
    private UploadEHICFilePage uploadEHICFilePage;
    private DependantEnterAddressPage dependantEnterAddressPage;

    public CommonStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        dependantNotEligiblePage = new DependantNotEligiblePage(driver);
        visaShareCodePage = new VisaShareCodePage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
        uploadCASFilePage = new UploadCASFilePage(driver);
        uploadEHICFilePage = new UploadEHICFilePage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        dependantEnterAddressPage = new DependantEnterAddressPage(driver);
    }

    @When("^I select the (.*)$")
    public void iSelectTheHyperlink(String hyperlink) {
        switch (hyperlink) {
            case "Service Name link":
                driver.manage().deleteAllCookies();
                commonPage.serviceNameLink();
                break;
            case "Back link":
                commonPage.backLink();
                break;
            case "GOV.UK link":
                commonPage.govUKLink();
                break;
            case "Help link":
                commonPage.navigateToHelp();
                break;
            case "Cookies link":
                commonPage.navigateToCookies();
                break;
            case "Accessibility Statement link":
                commonPage.navigateToAccessibilityStatement();
                break;
            case "Contact link":
                commonPage.navigateToContactUs();
                break;
            case "Terms and Conditions link":
                commonPage.navigateToTermsConditions();
                break;
            case "Privacy link":
                commonPage.navigateToPrivacyNotice();
                break;
            case "Share code GOV.UK link":
                visaShareCodePage.navigateToShareCode();
                break;
            case "Contact Us link":
                dependantNotEligiblePage.navigateToContactUs();
                break;
            case "More Info link":
                dependantNotEligiblePage.navigateToMoreInfo();
                break;
            case "Delete Dependant link":
                checkDependantsPage.deleteDependantLink();
                break;
            case "Privacy info link":
                commonPage.declarationPrivacy();
                break;
            case "End of service survey link":
                commonPage.navigateToFeedbackForm();
                break;
            case "Confirmation Contact link":
                commonPage.confirmationContact();
                break;
            case "Accept cookies button":
                commonPage.acceptAnalyticsCookies();
                break;
            case "Reject cookies button":
                commonPage.rejectAnalyticsCookies();
                break;
            case "View cookies link":
                commonPage.navigateToViewCookies();
                break;
            case "Change accept cookie settings link":
                commonPage.acceptAnalyticsCookies();
                commonPage.navigateToAcceptCookieSettings();
                driver.manage().deleteAllCookies();
                break;
            case "Change reject cookie settings link":
                commonPage.rejectAnalyticsCookies();
                commonPage.navigateToRejectCookieSettings();
                driver.manage().deleteAllCookies();
                break;
            case "Hide accept message button":
                commonPage.hideAcceptCookieBanner();
                break;
            case "Hide reject message button":
                commonPage.hideRejectCookieBanner();
                break;
            case "Feedback link":
                commonPage.navigateToSurveyPreview();
                break;
            case "Visas and Immigration link":
                commonPage.navigateToVisasAndImmigrationLink();
                break;
        }
    }

    @When("^I see the cookies banner is displayed$")
    public void iSeeTheCookiesBannerIsDisplayed() {
        Assert.assertTrue(commonPage.isCookieBannerDisplayed());
    }

    @Then("^I see the cookies banner is hidden$")
    public void iSeeTheCookiesBannerIsHidden() {
        Assert.assertFalse(commonPage.isCookieBannerDisplayed());
        driver.manage().deleteAllCookies();
    }

    @And("^I start the survey$")
    public void iStartTheSurvey() {
        commonPage.navigateToSnapSurvey();
    }

    @When("^I click on continue button on (.*)$")
    public void iClickOnContinueButton(String screen) {
        switch (screen) {
            case "View Uploaded Evidence Screen":
                uploadEHICFilePage.continueButton();
                break;
            case "View Uploaded EHIC Screen":
                uploadEHICFilePage.continueButton();
                break;
            case "Phone Number Screen":
                mobileNumberPage.continueButton();
                break;
            case "Enter Dependant Address screen":
                dependantEnterAddressPage.continueButton();
                break;
        }
    }
}
