package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static com.nhsbsa.ihs.stepdefs.Constants.FILE_UPLOAD_FILEPATH;

public class UploadFileStepDef {
    private WebDriver driver;
    private UploadEHICFilePage uploadEHICFilePage;
    private UploadCASFilePage uploadCASFilePage;
    private UploadDependantEHICFilePage uploadDependantEHICFilePage;

    public UploadFileStepDef() {
        driver = Config.getDriver();
        uploadEHICFilePage = new UploadEHICFilePage(driver);
        uploadCASFilePage = new UploadCASFilePage(driver);
        uploadDependantEHICFilePage = new UploadDependantEHICFilePage(driver);
    }

    @When("^I upload the EHIC (.*) of format (.*)$")
    public void iUploadTheEHICFileNameOfFormatFileFormat(String fileName, String fileFormat) {
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the EHIC with name (.*) (.*)$")
    public void iUploadTheEHICWithNameFileNameFileFormat(String fileName, String fileFormat) {
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the EHIC (.*) (.*) of size (.*)$")
    public void iUploadTheEHICFile_NameFile_FormatOfSizeFileSize(String fileName, String fileFormat, String fileSize) {
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I (.*) upload the EHIC file$")
    public void iUploadFileUploadTheEHICFile(String uploadFile) {
        switch (uploadFile) {
            case "Do":
                uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
                break;
            case "Do Not":
                uploadEHICFilePage.continueButton();
                break;
        }
    }

    @And("^I view the uploaded EHIC (.*)$")
    public void iViewTheUploadedEHICFileName(String fileName) {
        Assert.assertTrue(uploadEHICFilePage.getUploadedEHIC().contains(fileName));
        uploadEHICFilePage.continueOnViewEHIC();
    }

    @When("^I upload the CAS (.*) of format (.*)$")
    public void iUploadTheCASFileNameOfFormatFileFormat(String fileName, String fileFormat) {
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the CAS with name (.*) (.*)$")
    public void iUploadTheCASWithNameFileNameFileFormat(String fileName, String fileFormat) {
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the CAS (.*) (.*) of size (.*)$")
    public void iUploadTheCASFile_NameFile_FormatOfSizeFileSize(String fileName, String fileFormat, String fileSize) {
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I (.*) upload the CAS file$")
    public void iUploadFileUploadTheCASFile(String uploadFile) {
        switch (uploadFile) {
            case "Do":
                uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
                break;
            case "Do Not":
                uploadCASFilePage.continueButton();
                break;
        }
    }

    @And("^I view the uploaded CAS (.*)$")
    public void iViewTheUploadedCASFileName(String fileName) {
        Assert.assertTrue(uploadCASFilePage.getUploadedCAS().contains(fileName));
        Assert.assertTrue(uploadCASFilePage.getUploadedEHIC().contains("Payslip-June"));
        uploadCASFilePage.continueOnViewCAS();
    }

    @When("^I upload the Dependant EHIC (.*) of format (.*)$")
    public void iUploadTheDependantEHICFileNameOfFormatFileFormat(String fileName, String fileFormat) {
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the Dependant EHIC with name (.*) (.*)$")
    public void iUploadTheDependantEHICWithNameFileNameFileFormat(String fileName, String fileFormat) {
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the Dependant EHIC (.*) (.*) of size (.*)$")
    public void iUploadTheDependantEHICFile_NameFile_FormatOfSizeFileSize(String fileName, String fileFormat, String fileSize) {
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I (.*) upload the Dependant EHIC file$")
    public void iUploadFileUploadTheDependantEHICFile(String uploadFile) {
        switch (uploadFile) {
            case "Do":
                uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
                break;
            case "Do Not":
                uploadDependantEHICFilePage.continueButton();
                break;
        }
    }

    @And("^I view the uploaded Dependant EHIC (.*)$")
    public void iViewTheUploadedDependantEHICFileName(String fileName) {
        Assert.assertTrue(uploadDependantEHICFilePage.getUploadedEHIC().contains(fileName));
        uploadDependantEHICFilePage.continueOnViewEHIC();
    }

    @When("^I upload EHIC (.*) of format (.*)$")
    public void iUploadEHICFileNameOfFormatFileFormat(String fileName, String fileFormat) throws IOException, InterruptedException {
        uploadEHICFilePage.chooseFileAndContinueBS(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload CAS (.*) of format (.*)$")
    public void iUploadCASFileNameOfFormatFileFormat(String fileName, String fileFormat) throws IOException, InterruptedException {
        uploadCASFilePage.chooseFileAndContinueBS(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I select delete (.*) link on Evidence Uploaded screen$")
    public void iSelectDeleteLinkOnScreen(String deleteLink) {
        switch (deleteLink) {
            case "EHIC":
                uploadCASFilePage.deleteEHICLink();
                break;
            case "CAS":
                uploadCASFilePage.deleteCASLink();
                break;

        }
    }

    @Then("^I will see successful deletion message for file (.*) of format (.*)$")
    public void iWillSeeSuccessfulDeletetionMessageForFile(String fileName, String fileFormat) {
        Assert.assertEquals(uploadCASFilePage.getDeleteFileMessage(), "File named " +fileName+fileFormat+" has been deleted" );

    }

    @And("^I view uploaded EHIC (.*) on Uploaded Evidence Screen$")
    public void iViewTheUploadedEHICOnUploadedEvidenceScreen(String fileName) {
        Assert.assertTrue(uploadCASFilePage.getUploadedEHIC().contains(fileName));
    }

    @And("^I view uploaded CAS (.*) on Uploaded Evidence Screen$")
    public void iViewTheUploadedCASOnUploadedEvidenceScreen(String fileName) {
        Assert.assertTrue(uploadCASFilePage.getUploadedCAS().contains(fileName));
    }

    @When("^I select (.*) on the (.*)$")
    public void iSelectDeleteLinkOnTheScreen(String deleteLink, String screen) {
        switch (deleteLink) {
            case "Delete EHIC Card link":
                uploadEHICFilePage.deleteEHICCardLink();
                break;
            case "Delete Dependant EHIC Card link":
                uploadDependantEHICFilePage.deleteDependantEHICCardLink();
                break;
        }
    }

    @And("^I will see successful deletion message for (.*)(.*) file on (.*)$")
    public void iWillSeeSuccessfulDeletionMessageForFileNameFileFormatFileOnScreen(String fileName, String fileFormat, String screen) {
        switch (screen) {
            case "Upload EHIC Card screen":
                Assert.assertEquals(uploadEHICFilePage.getDeletedEHICSuccessMessage(), "File named " + fileName + fileFormat + " has been deleted");
                break;
            case "Upload Dependant EHIC Card screen":
                Assert.assertEquals(uploadDependantEHICFilePage.getDependantDeletedEHICSuccessMessage(), "File named " + fileName + fileFormat + " has been deleted");
                break;
        }
    }

}

