package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static com.nhsbsa.ihs.stepdefs.Constants.*;
import static org.hamcrest.CoreMatchers.is;

public class ClaimDetailsStepDef {
    private WebDriver driver;
    private CommonPage commonPage;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private IHSNumberPage ihsNumberPage;
    private VisaShareCodePage visaShareCodePage;
    private EnterAddressPage enterAddressPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;

    public ClaimDetailsStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        visaShareCodePage = new VisaShareCodePage(driver);
        emailPage = new EmailPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        enterAddressPage = new EnterAddressPage(driver);
    }

    @And("^My firstname is (.*) and surname is (.*)$")
    public void myFirstnameIsGivenNameAndSurnameIsFamilyName(String GivenName, String FamilyName) {
        namePage.enterNameAndSubmit(GivenName, FamilyName);
    }

    @When("^I attempt to enter (.*) characters name$")
    public void iAttemptToEnterOverLimitCharactersName(int count) {
        namePage.enterOverLimitText(count);
    }

    @Then("^I am restricted to enter (.*) characters name$")
    public void iAmRestrictedToEnterMaximumCharactersName(int maximum) {
        Assert.assertEquals(namePage.getEnteredTextCount(), maximum);
        namePage.continueButton();
    }

    @And("^My date of birth is (.*) (.*) (.*)$")
    public void myDateOfBirthIsDayMonthYear(String Day, String Month, String Year) {
        dateOfBirthPage.enterDateOfBirthAndSubmit(Day, Month, Year);
    }

    @When("^My IHS number is (.*)$")
    public void myIHSNumberIsIHSNumber(String IHSNumber) {
        ihsNumberPage.enterIHSNumberAndSubmit(IHSNumber);
    }

    @When("^My visa share code is (.*)$")
    public void myVisaShareCodeIsVisaCode(String VisaCode) {
        visaShareCodePage.enterVisaShareCodeAndSubmit(VisaCode);
    }

    @When("^My address is (.*), (.*), (.*), (.*) and (.*)$")
    public void myAddressIsAddressLine1AddressLine2TownCountyAndPostcode(String AddressLine1, String AddressLine2, String Town, String County, String Postcode) throws InterruptedException {
        enterAddressPage.enterAddressAndSubmit(AddressLine1, AddressLine2, Town, County, Postcode);
    }

    @When("^My Email address is (.*)$")
    public void myEmailAddressIsEmail(String Email) {
        emailPage.enterEmailAndSubmit(Email);
    }

    @When("^My Phone number is (.*)$")
    public void myPhoneNumberIsPhoneNumber(String PhoneNumber) {
        mobileNumberPage.enterMobileNumberAndSubmit(PhoneNumber);
    }

    @When("^I verify my answers and continue$")
    public void iVerifyMyAnswersAndContinue() {
        commonPage.continueButton();
    }

    @And("^I check my answers and submit the claim application$")
    public void iCheckMyAnswersAndSubmitTheClaimApplication() {
        commonPage.continueButton();
        commonPage.acceptOnDeclaration();
    }

    @And("^I submit the claim application$")
    public void iSubmitTheClaimApplication() {
        commonPage.acceptOnDeclaration();
    }

    @Then("^My claim is submitted successfully$")
    public void myClaimIsSubmittedSuccessfully() {
        Assert.assertEquals(CONFIRMATION_PAGE_TITLE, commonPage.getCurrentPageTitle());
        Assert.assertTrue(commonPage.getCurrentURL().contains(CONFIRMATION_PAGE_URL));
        Assert.assertTrue(commonPage.getReferenceNumber().contains("BSA"));
    }

    @Then("^I validate the output$")
    public void iValidateTheOutput() {
        WebDriverWait driverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        try {
            driverWait.until(ExpectedConditions.titleIs(CONFIRMATION_PAGE_TITLE));
            Assert.assertEquals(CONFIRMATION_PAGE_TITLE, commonPage.getCurrentPageTitle());
            ((JavascriptExecutor)driver).executeScript("sauce:job-result=passed");
        }
        catch(AssertionError e) {
            Assert.assertThat(e.getMessage(),is("My Assertion error"));
            ((JavascriptExecutor)driver).executeScript("sauce:job-result=failed");
        }
    }

    @When("^I attempt to enter (.*) characters IHS number$")
    public void iAttemptToEnterAnyCharactersIHSNumber(int count) {
        ihsNumberPage.enterOverLimitText("ihsc1234567890");
    }

    @Then("^I am restricted to enter (.*) characters IHS number$")
    public void iAmRestrictedToEnterMaximumCharactersIHSNumber(int maximum) {
        Assert.assertEquals(ihsNumberPage.getEnteredTextCount(), maximum);
        ihsNumberPage.continueButton();
    }
}

