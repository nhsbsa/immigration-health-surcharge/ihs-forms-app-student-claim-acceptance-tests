package com.nhsbsa.ihs.stepdefs;

public class Constants {

    public static final String GIVEN_NAME = "Given Name";
    public static final String FAMILY_NAME = "Family Name";
    public static final String DOB_DAY = "01";
    public static final String DOB_MONTH = "01";
    public static final String DOB_FULL_MONTH = "January";
    public static final String DOB_YEAR = "1988";
    public static final String IHS_NUMBER = "IHS123456789";
    public static final String VISA_SHARE_CODE = "X88 888 88X";
    public static final String EMAIL_ADDRESS = "ihs-testing@nhsbsa.nhs.uk";
    public static final String PHONE_NUMBER = "07123456789";
    public static final String ADDRESS_LINE_1 = "123";
    public static final String ADDRESS_LINE_2 = "Test Street";
    public static final String TOWN_NAME = "Test Town";
    public static final String COUNTY_NAME = "Test County";
    public static final String POST_CODE = "SW1A 1AA";

    public static final String UPDATED_GIVEN_NAME = "Archie";
    public static final String UPDATED_FAMILY_NAME = "A D'Russell";
    public static final String UPDATED_DOB_DAY = "31";
    public static final String UPDATED_DOB_MONTH = "12";
    public static final String UPDATED_FULL_DOB_MONTH = "December";
    public static final String UPDATED_DOB_YEAR = "1998";
    public static final String UPDATED_IHS_NUMBER = "IHSC213456798";
    public static final String UPDATED_VISA_SHARE_CODE = "A88 888 88A";
    public static final String UPDATED_EMAIL_ADDRESS = "test@test.com";
    public static final String UPDATED_PHONE_NUMBER = "07814787492";
    public static final String UPDATED_ADDRESS_LINE_1 = "9580";
    public static final String UPDATED_ADDRESS_LINE_2 = "North Promenade";
    public static final String UPDATED_TOWN_NAME = "Donhead St Mary";
    public static final String UPDATED_COUNTY_NAME = "United Kingdom";
    public static final String UPDATED_POST_CODE = "SP7 2EA";

    public static final String DEPENDANT_GIVEN_NAME = "Mary";
    public static final String DEPENDANT_FAMILY_NAME = "O'hara";
    public static final String DEPENDANT_DOB_DAY = "04";
    public static final String DEPENDANT_DOB_MONTH = "11";
    public static final String DEPENDANT_DOB_FULL_MONTH = "November";
    public static final String DEPENDANT_DOB_YEAR = "1980";
    public static final String DEPENDANT_IHS_NUMBER = "IHS567876534";
    public static final String DEPENDANT_VISA_SHARE_CODE = "A76 345 55Z";
    public static final String DEPENDANT_ADDRESS_LINE_1 = "555";
    public static final String DEPENDANT_ADDRESS_LINE_2 = "Dependant Street";
    public static final String DEPENDANT_TOWN_NAME = "Dependant Town";
    public static final String DEPENDANT_COUNTY_NAME = "Dependant County";
    public static final String DEPENDANT_POST_CODE = "DN551PT";

    public static final String UPDATED_DEPENDANT_GIVEN_NAME = "Updated Mary";
    public static final String UPDATED_DEPENDANT_FAMILY_NAME = "Updated O'hara";
    public static final String UPDATED_DEPENDANT_DOB_DAY = "06";
    public static final String UPDATED_DEPENDANT_DOB_MONTH = "10";
    public static final String UPDATED_DEPENDANT_DOB_FULL_MONTH = "October";
    public static final String UPDATED_DEPENDANT_DOB_YEAR = "2000";
    public static final String UPDATED_DEPENDANT_IHS_NUMBER = "IHSC567877734";
    public static final String UPDATED_DEPENDANT_VISA_SHARE_CODE = "A76 111 55Z";
    public static final String UPDATED_DEPENDANT_ADDRESS_LINE_1 = "Updated Building Number";
    public static final String UPDATED_DEPENDANT_ADDRESS_LINE_2 = "Updated Dependant Street";
    public static final String UPDATED_DEPENDANT_TOWN_NAME = "Updated Dependant Town";
    public static final String UPDATED_DEPENDANT_COUNTY_NAME = "Updated Dependant County";
    public static final String UPDATED_DEPENDANT_POST_CODE = "DN661PT";

    public static String FILE_UPLOAD_FILEPATH = System.getProperty("user.dir") + "/File_uploads/";

    public static final String START_PAGE_URL = "https://www.gov.uk/apply-student-immigration-health-surcharge-refund";
    public static final String START_PAGE_TITLE = "Get an immigration health surcharge refund if you're a student from the EU, Switzerland, Norway, Iceland or Liechtenstein - GOV.UK";

    public static final String IHS_PAID_PAGE_URL = "/student-eligibility/paid-immigration-health-surcharge";
    public static final String NAME_PAGE_URL = "/student-claim/name";
    public static final String DOB_PAGE_URL = "/student-claim/date-of-birth";
    public static final String IHS_NUMBER_PAGE_URL = "/student-claim/immigration-health-surcharge-number";
    public static final String VISA_SHARE_CODE_PAGE_URL = "/student-claim/applicant-share-code";
    public static final String ENTER_ADDRESS_PAGE_URL = "/student-claim/enter-address";
    public static final String EMAIL_PAGE_URL = "/student-claim/email-address";
    public static final String PHONE_NUMBER_PAGE_URL = "/student-claim/telephone-number";
    public static final String UPLOAD_EHIC_PAGE_URL = "/student-claim/upload-european-health-insurance-card-ehic";
    public static final String VIEW_EHIC_PAGE_URL = "/student-claim/uploaded-files-european-health-insurance-card-ehic";
    public static final String UPLOAD_CAS_PAGE_URL = "/student-claim/upload-evidence";
    public static final String VIEW_EVIDENCE_PAGE_URL = "/student-claim/uploaded-files-evidence";
    public static final String ADD_DEPENDANT_PAGE_URL = "/student-claim/add-dependants-with-european-health-insurance-card-ehic";
    public static final String DEPENDANT_NOT_ELIGIBLE_PAGE_URL = "/student-claim/dependant-not-eligible";
    public static final String DEPENDANT_PAID_WORK_PAGE_URL = "/student-claim/dependant-paid-work-uk";
    public static final String DEPENDANT_LIVING_UK_PAGE_URL = "/student-claim/dependant-living-in-uk";
    public static final String DEPENDANT_NAME_PAGE_URL = "/student-claim/dependant-name";
    public static final String DEPENDANT_DOB_PAGE_URL = "/student-claim/dependant-date-of-birth";
    public static final String DEPENDANT_IHS_NUMBER_PAGE_URL = "/student-claim/dependant-immigration-health-surcharge-number";
    public static final String DEPENDANT_VISA_SHARE_CODE_PAGE_URL = "/student-claim/dependant-share-code";
    public static final String SAME_ADDRESS_AS_APPLICANT_PAGE_URL = "/student-claim/dependant-same-address-as-applicant";
    public static final String ENTER_DEPENDANT_ADDRESS_PAGE_URL = "/student-claim/dependant-enter-address";
    public static final String UPLOAD_DEPENDANT_EHIC_PAGE_URL = "/student-claim/upload-dependant-european-health-insurance-card-ehic";
    public static final String VIEW_DEPENDANT_EHIC_PAGE_URL = "/student-claim/uploaded-dependant-files-european-health-insurance-card-ehic";
    public static final String CHECK_DEPENDANTS_PAGE_URL = "/student-claim/check-dependant-answers";
    public static final String CHECK_ANSWERS_PAGE_URL = "/student-claim/check-your-answers";
    public static final String DECLARATION_PAGE_URL = "/student-claim/declaration";
    public static final String CONFIRMATION_PAGE_URL = "/student-claim/application-complete";

    public static final String COOKIES_POLICY_PAGE_URL = "/student-help/cookies";
    public static final String CONTACT_US_PAGE_URL = "/student-help/contact";
    public static final String ACCESSIBILITY_STATEMENT_PAGE_URL = "/student-help/accessibility-statement";
    public static final String TERMS_CONDITIONS_PAGE_URL = "/student-help/terms-conditions";
    public static final String PRIVACY_NOTICE_PAGE_URL = "/student-help/privacy-notice";

    public static final String IHS_PAID_PAGE_TITLE = "Has the applicant paid an immigration health surcharge for their visa? - GOV.UK";
    public static final String NAME_PAGE_TITLE = "What's the applicant's name? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String DOB_PAGE_TITLE = "What's the applicant's date of birth? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String IHS_NUMBER_PAGE_TITLE = "What's the applicant's immigration health surcharge (IHS) number? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String VISA_SHARE_CODE_PAGE_TITLE = "What's the applicant's visa share code? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String ENTER_ADDRESS_PAGE_TITLE = "What's the applicant's UK address? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String EMAIL_PAGE_TITLE = "What's the applicant's email address? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String PHONE_NUMBER_PAGE_TITLE = "What's the applicant's phone number? (optional) - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String UPLOAD_EHIC_PAGE_TITLE = "Upload a copy of the applicant's European Health Insurance Card (EHIC) - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String VIEW_EHIC_PAGE_TITLE = "European Health Insurance Card (EHIC) uploaded - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String UPLOAD_CAS_PAGE_TITLE = "Upload evidence of studying full-time in higher education - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String VIEW_EVIDENCE_PAGE_TITLE = "Evidence uploaded - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String ADD_DEPENDANT_PAGE_TITLE = "Does the applicant want to add any dependants who have a valid European Health Insurance Card (EHIC) issued from the EU, Switzerland, Norway, Iceland or Liechtenstein? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String DEPENDANT_NOT_ELIGIBLE_PAGE_TITLE = "The dependant is not eligible for an immigration health surcharge reimbursement - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String DEPENDANT_PAID_WORK_PAGE_TITLE = "Is the dependant carrying out any paid work in the UK? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String DEPENDANT_LIVING_UK_PAGE_TITLE = "Is the dependant currently living in the UK? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String DEPENDANT_NAME_PAGE_TITLE = "What's the dependant's name? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String DEPENDANT_DOB_PAGE_TITLE = "What's the dependant's date of birth? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String DEPENDANT_IHS_NUMBER_PAGE_TITLE = "What's the dependant's immigration health surcharge (IHS) number? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String DEPENDANT_VISA_SHARE_CODE_PAGE_TITLE = "What's the dependant's visa share code? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String SAME_ADDRESS_AS_APPLICANT_PAGE_TITLE = "Does the dependant live at the same UK address as the applicant? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String ENTER_DEPENDANT_ADDRESS_PAGE_TITLE = "What's the dependant's UK address? - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String UPLOAD_DEPENDANT_EHIC_PAGE_TITLE = "Upload a copy of the dependant's European Health Insurance Card (EHIC) - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String VIEW_DEPENDANT_EHIC_PAGE_TITLE = "Dependant's European Health Insurance Card (EHIC) uploaded - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String CHECK_DEPENDANTS_PAGE_TITLE = "Check dependant details - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String CHECK_ANSWERS_PAGE_TITLE = "Check your answers - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String DECLARATION_PAGE_TITLE = "Declaration - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String CONFIRMATION_PAGE_TITLE = "Application complete - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";

    public static final String COOKIES_POLICY_PAGE_TITLE = "Cookies - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String CONTACT_US_PAGE_TITLE = "Contact us - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String ACCESSIBILITY_STATEMENT_PAGE_TITLE = "Accessibility statement for Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String TERMS_CONDITIONS_PAGE_TITLE = "Terms and conditions - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String PRIVACY_NOTICE_PAGE_TITLE = "Privacy notice - Apply for your immigration health surcharge reimbursement as a student - GOV.UK";

    public static final String GOV_UK_PAGE = "Welcome to GOV.UK";
    public static final String HELP_PAGE = "Help using GOV.UK - Help Pages - GOV.UK";
    public static final String REQUEST_SHARE_CODE_PAGE_TITLE = "View and prove your immigration status: get a share code - GOV.UK";
    public static final String REQUEST_SHARE_CODE_PAGE_URL = "https://www.gov.uk/view-prove-immigration-status";
    public static final String GOV_UK_STUDENT_FEEDBACK_PAGE = "Give feedback on Apply for your immigration health surcharge reimbursement as a student - GOV.UK";
    public static final String SNAP_SURVEY_PREVIEW_PAGE = "Survey Preview - Snap Surveys";
    public static final String STUDENT_SNAP_SURVEY_PAGE = "IHS Students Digital Feedback";
    public static final String CONTACT_UK_VISAS_AND_IMMIGRATION_PAGE_TITLE = "Contact UK Visas and Immigration for help - GOV.UK";
    public static final String CONTACT_UK_VISAS_AND_IMMIGRATION_PAGE_URL = "https://www.gov.uk/contact-ukvi-inside-outside-uk";

}