package com.nhsbsa.ihs.runners;

import com.nhsbsa.ihs.stepdefs.Accessibility;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"com.nhsbsa.ihs.stepdefs"},
        features = {"src/test/resources/features/"},
        plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" , "pretty" , "json:target/cucumber-json/cucumber.json"},
        tags = "@Accessibility"
)

public class AccessibilityTestRunner {

    @AfterClass
    public static void createReport() {
        Accessibility accessibility = new Accessibility();
        accessibility.createCSVReport();
    }
}