@DependantAddress @IHSRI-1102 @Regression

Feature: Validation of the Enter Dependant Address page on IHS claim app to enable student applicants to enter their dependant's address for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Dependant Address screen

  @IHSRI-1754 @Retest-1893 @Retest-3466
  Scenario Outline: Validate the enter address functionality for positive and negative scenarios
    When My dependant's address is <AddressLine1>, <AddressLine2>, <Town>, <County> and <Postcode>
    Then I will see the <output>
    Examples:
      | AddressLine1        | AddressLine2        | Town        | County           | Postcode  | output                             |
      #positive tests
      | TE                  | TE                  | London      | Wiltshire        | EC1A 1BB  | Upload Dependant EHIC Card screen  |
      | 123 Test Address    |                     | London      |                  | W1A 0AX   | Upload Dependant EHIC Card screen  |
      | 123Test Address1    | 123Test Address2    | London      | Wiltshire        | M1 1AE    | Upload Dependant EHIC Card screen  |
      | Test's              | Test's              | London      |                  | B33 8TH   | Upload Dependant EHIC Card screen  |
      | Test-Address1       | Test-Address2       | London-town | Wiltshire-county | CR2 6XH   | Upload Dependant EHIC Card screen  |
      | Test's-Address1     | Test's-Address2     | London-town | Wiltshire-county | DN551PT   | Upload Dependant EHIC Card screen  |
      | Test Address Line 1 | Test Address Line 2 | London town | Wiltshire county | DN551PT   | Upload Dependant EHIC Card screen  |
      #negative tests
      |                     |                     | Swindon     | Wiltshire        | DN551PT   | Blank dependant address line 1 error         |
      | address-one         | address-two         |             | Wiltshire        | DN551PT   | Blank dependant town error                    |
      | Address1            | Address2            | London      | Wiltshire        |           | Blank dependant post code error              |
      | Address1            | Address2            | London      | Wiltshire        | SW1A2 1AA | Wrong dependant post code error              |
      | 123                 | Address2            | London      | Wiltshire        | SW1A1AA12 | Wrong dependant post code error              |
      | Address1            | 123                 | London      | Wiltshire        | TESTPOST  | Wrong dependant post code error              |
      | b                   |                     | London      | Wiltshire        | W1A 0AX   | Invalid range dependant address line 1 error |
      | 123                 | s                   | London      | Wiltshire        | B33 8TH   | Invalid range dependant address line 2 error |
      | 123                 | Address2            | t           | Wiltshire        | B33 8TH   | Invalid range dependant town error           |
      | 123                 | Address2            | London      | c                | B33 8TH   | Invalid range dependant county error         |
      | ***                 | Address2            | London      | Wiltshire        | W1A 0AX   | Invalid dependant address line 1 error       |
      | Test Address1       | "Test Address2"     | London      | Wiltshire        | W1A 0AX   | Invalid dependant address line 2 error       |
      | 123                 | Address2            | London@123  | Wiltshire        | W1A 0AX   | Invalid dependant town error                 |
      | Address1            | 123                 | London      | (Wiltshire       | W1A 0AX   | Invalid dependant county error               |
      | $ Address1          | Address2            | London      | Wiltshire        | W1A 0AX   | Invalid dependant address line 1 error       |
      | test 123            | $ Address2          | London      | Wiltshire        | W1A 0AX   | Invalid dependant address line 2 error       |
      | Address1            | 123                 | London      | Wiltshire@       | W1A 0AX   | Invalid dependant county error               |

  Scenario Outline: Validate the hyperlinks on Enter Dependant Address page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                           |
      | Back link         | Same Address as Applicant screen |
      | Service Name link | Start screen                     |
      | GOV.UK link       | GOV UK screen                    |