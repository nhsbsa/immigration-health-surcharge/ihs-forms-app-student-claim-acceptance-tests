@CheckYourAnswers @IHSRI-2050 @Regression

Feature: Validation of Change uploaded evidence functionality on claim web app to enable users to change their evidence before submitting the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Check your answers screen

  @IHSRI-2912
  Scenario Outline: Validate change uploaded evidence link functionality for EHIC evidence on check your answer page
    When I select Change File uploaded link on Check Your Answers page
    Then I will see the View Uploaded Evidence screen
    When I select delete EHIC link on Evidence Uploaded screen
    Then I will see the Upload EHIC Card screen
    When I upload the EHIC <fileName> of format <fileFormat>
    Then I will see the View Uploaded Evidence screen
    When I click on continue button on View Uploaded Evidence Screen
    Then I will see file <fileName><fileFormat> on Check Your Answers page
    Examples:
      | fileName                                                                                                                           | fileFormat |
      | Sample_1                                                                                                                           | .jpg       |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       |

  @IHSRI-2913
  Scenario Outline: Validate change uploaded evidence link functionality for CAS evidence on check your answer page
    When I select Change File uploaded link on Check Your Answers page
    Then I will see the View Uploaded Evidence screen
    When I select delete CAS link on Evidence Uploaded screen
    Then I will see the Upload CAS Letter screen
    When I upload the CAS <fileName> of format <fileFormat>
    Then I will see the View Uploaded Evidence screen
    When I click on continue button on View Uploaded Evidence Screen
    Then I will see file <fileName><fileFormat> on Check Your Answers page
    Examples:
      | fileName                                                                                                                           | fileFormat |
      | Sample_4                                                                                                                           | .bmp       |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       |

  Scenario: Validate change link functionality for uploaded evidence on Check Your Answers page when EHIC and CAS files are changed
    When I select Change File uploaded link on Check Your Answers page
    Then I will see the View Uploaded Evidence screen
    When I select delete CAS link on Evidence Uploaded screen
    Then I will see the Upload CAS Letter screen
    When I upload the CAS Sample_4 of format .bmp
    And I select delete EHIC link on Evidence Uploaded screen
    Then I will see the Upload EHIC Card screen
    When I upload the EHIC Payslip-June of format .png
    Then I will see the View Uploaded Evidence screen
    When I click on continue button on View Uploaded Evidence Screen
    Then I see the updated CAS Sample_4.bmp and EHIC Payslip-June.png on Check Your Answers page

  Scenario Outline: Validate the error for upload EHIC functionality using change link on Check Your Answers page when invalid file is uploaded
    When I select Change File uploaded link on Check Your Answers page
    Then I will see the View Uploaded Evidence screen
    When I select delete EHIC link on Evidence Uploaded screen
    Then I will see the Upload EHIC Card screen
    When I upload the EHIC Payslip.Jan of format .pdf
    Then I will be displayed <error> for Payslip.Jan.pdf file
    Examples:
      | error                   |
      | Invalid file name error |

  Scenario Outline: Validate the error for upload CAS functionality using change link on Check Your Answers page when invalid file is uploaded
    When I select Change File uploaded link on Check Your Answers page
    Then I will see the View Uploaded Evidence screen
    When I select delete CAS link on Evidence Uploaded screen
    Then I will see the Upload CAS Letter screen
    When I upload the CAS Sample excel of format .xlsx
    Then I will be displayed <error> for Sample excel.xlsx file
    Examples:
      | error                     |
      | Invalid file format error |

  @IHSRI-2139
  Scenario: Validate the back link functionality for CAS file from the change links on Check Your answers page
    When I select Change File uploaded link on Check Your Answers page
    Then I will see the View Uploaded Evidence screen
    When I select delete CAS link on Evidence Uploaded screen
    Then I will see the Upload CAS Letter screen
    When I select the Back link
    And I click on continue button on View Uploaded EHIC Screen
    Then I will see the Upload CAS Letter screen

  @IHSRI-2139
  Scenario: Validate the back link functionality for EHIC file from the change links on Check Your answers page
    When I select Change File uploaded link on Check Your Answers page
    Then I will see the View Uploaded Evidence screen
    When I select delete EHIC link on Evidence Uploaded screen
    Then I will see the Upload EHIC Card screen
    When I select the Back link
    And I click on continue button on Phone Number Screen
    Then I will see the Upload EHIC Card screen