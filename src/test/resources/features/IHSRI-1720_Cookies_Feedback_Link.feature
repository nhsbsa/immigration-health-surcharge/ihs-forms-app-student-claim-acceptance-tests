@CookiesBanner @FeedbackLink @NeedJavaScript @Regression

Feature: Validation of the cookies banner and feedback link in IHS Student reimbursement service

  Background:
    Given I launch the IHS Student claim application

  @Smoke @IHSRI-1670
  Scenario Outline: Validate the functionality of the cookies banner on IHS Student forms app
    When I see the cookies banner is displayed
    And I select the <button1>
    Then I will see the <message>
    When I select the <button2>
    Then I see the cookies banner is hidden

    Examples:
      | button1               | message                       | button2                    |
      | Accept cookies button | Cookies accepted confirmation | Hide accept message button |
      | Reject cookies button | Cookies rejected confirmation | Hide reject message button |

  @IHSRI-1670 @Retest-1679
  Scenario Outline: Validate the hyperlinks on the cookies banner
    When I see the cookies banner is displayed
    And I select the <hyperlink>
    Then I will see the <screen>

    Examples:
      | hyperlink                          | screen              |
      | View cookies link                  | Cookies Policy page |
      | Change accept cookie settings link | Cookies Policy page |
      | Change reject cookie settings link | Cookies Policy page |

  @Smoke @IHSRI-1720 @IHSRI-2864
  Scenario Outline: Validate the in-service feedback survey on IHS student service
    When I select the <hyperlink>
    And I will see the Snap survey preview
    And I start the survey
    Then I will see the <output>
    Examples:
      | hyperlink     | output                             |
      | Feedback link | IHS Students Service Survey screen |