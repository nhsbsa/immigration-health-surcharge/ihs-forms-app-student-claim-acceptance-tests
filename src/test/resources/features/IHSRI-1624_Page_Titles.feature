@PageTitles @IHSRI-1624 @Regression

Feature: Validation of page title on the IHS student claim application screens

  Background:
    Given I launch the IHS Student claim application

  @IHSRI-3054
  Scenario Outline: Verify the page title displayed on student claim application pages
    And My Page Title is Name screen title
    And My firstname is <GivenName> and surname is <FamilyName>
    And My Page Title is Date of birth screen title
    And My date of birth is <Day> <Month> <Year>
    And My Page Title is IHS Number screen title
    And My IHS number is <IHSNumber>
    And My Page Title is Visa Share Code screen title
    And My visa share code is <VisaCode>
    And My Page Title is Enter Address screen title
    And My address is <AddressLine1>, <AddressLine2>, <Town>, <County> and <Postcode>
    And My Page Title is Email Address screen title
    And My Email address is <Email>
    And My Page Title is Phone number screen title
    And My Phone number is <PhoneNumber>
    And My Page Title is Upload EHIC Card screen title
    And I upload the EHIC <fileName> of format <fileFormat>
    And My Page Title is View Uploaded EHIC screen title
    And I view the uploaded EHIC <fileName>
    And My Page Title is Upload CAS Letter screen title
    And I upload the CAS <fileName> of format <fileFormat>
    And My Page Title is View Uploaded Evidence screen title
    And I view the uploaded CAS <fileName>
    And My Page Title is Add Dependant screen title
    And I do have dependant to add
    And My Page Title is Dependant Paid Work screen title
    And My dependant is not carrying out paid work in UK
    And My Page Title is Dependant Living in UK screen title
    And My dependant is currently residing in UK
    And My Page Title is Dependant Name screen title
    And My dependant's firstname is <GivenName> and surname is <FamilyName>
    And My Page Title is Dependant Date of birth screen title
    And My dependant's date of birth is <Day> <Month> <Year>
    And My Page Title is Dependant IHS Number screen title
    And My dependant's IHS number is <IHSNumber>
    And My Page Title is Dependant Visa Share Code screen title
    And My dependant's visa share code is <VisaCode>
    And My Page Title is Same Address as Applicant screen title
    And My dependant does not live in the same UK address
    And My Page Title is Enter Dependant Address screen title
    And My dependant's address is <AddressLine1>, <AddressLine2>, <Town>, <County> and <Postcode>
    And My Page Title is Upload Dependant EHIC Card screen title
    And I upload the Dependant EHIC <fileName> of format <fileFormat>
    And My Page Title is View Uploaded Dependant EHIC screen title
    And I view the uploaded Dependant EHIC <fileName>
    And My Page Title is Check Dependant Details screen title
    And I do not have another dependant to add
    And My Page Title is Check Your Answers screen title
    And I verify my answers and continue
    And My Page Title is Declaration screen title
    When I submit the claim application
    Then My claim is submitted successfully
    And My Page Title is Confirmation screen title

    Examples:
      | GivenName | FamilyName | Day | Month | Year | IHSNumber    | VisaCode    | AddressLine1  | AddressLine2  | Town   | County    | Postcode | Email                      | PhoneNumber | fileName     | fileFormat |
      | Melissa   | Cooper     | 21  | 11    | 1988 | IHS567456734 | A98 765 43Z | Test Address1 | Test Address2 | London | Wiltshire | EC1A 1BB | nhsbsa.ihs-testing@nhs.net | 07123456789 | Payslip-June | .png       |


  Scenario Outline: Verify the page title on Dependant Not Eligible page
    And My details are captured until Dependant Paid Work screen
    When My dependant is carrying out paid work in UK
    Then I will see the <screen>
    And My Page Title is <screen> title

    Examples:
      | screen                        |
      | Dependant Not Eligible screen |

  Scenario Outline: Verify the page title displayed on footer link pages
    When I select the <hyperlink>
    Then My Page Title is <screen> title

    Examples:
      | hyperlink                    | screen                         |
      | Contact link                 | Contact Us screen              |
      | Cookies link                 | Cookies Policy screen          |
      | Accessibility Statement link | Accessibility Statement screen |
      | Terms and Conditions link    | Terms and Conditions screen    |
      | Privacy link                 | Privacy Notice screen          |