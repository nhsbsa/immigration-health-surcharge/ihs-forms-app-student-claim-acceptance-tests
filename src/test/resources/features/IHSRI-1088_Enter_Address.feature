@EnterAddress @IHSRI-1088 @Regression

Feature: Validation of the Enter Address page on IHS claim app to enable student applicants to enter their address for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Enter Address screen

  @IHSRI-1754 @Retest-1889 @Retest-3466
  Scenario Outline: Validate the enter address functionality for positive and negative scenarios
    When My address is <AddressLine1>, <AddressLine2>, <Town>, <County> and <Postcode>
    Then I will see the <output>
    Examples:
      | AddressLine1        | AddressLine2        | Town        | County           | Postcode  | output                             |
      #positive tests
      | TE                  | TE                  | London      | Wiltshire        | EC1A 1BB  | Email Address screen               |
      | 123 Test Address    |                     | London      |                  | W1A 0AX   | Email Address screen               |
      | 123Test Address1    | 123Test Address2    | London      | Wiltshire        | M1 1AE    | Email Address screen               |
      | Test's              | Test's              | London      |                  | B33 8TH   | Email Address screen               |
      | Test-Address1       | Test-Address2       | London-town | Wiltshire-county | CR2 6XH   | Email Address screen               |
      | Test's-Address1     | Test's-Address2     | London-town | Wiltshire-county | DN551PT   | Email Address screen               |
      | Test Address Line 1 | Test Address Line 2 | London town | Wiltshire county | DN551PT   | Email Address screen               |
      #negative tests
      |                     |                     | Swindon     | Wiltshire        | DN551PT   | Blank address line 1 error         |
      | address-one         | address-two         |             | Wiltshire        | DN551PT   | Blank town error                   |
      | Address1            | Address2            | London      | Wiltshire        |           | Blank post code error              |
      | Address1            | Address2            | London      | Wiltshire        | SW1A2 1AA | Wrong post code error              |
      | 123                 | Address2            | London      | Wiltshire        | SW1A1AA12 | Wrong post code error              |
      | Address1            | 123                 | London      | Wiltshire        | TESTPOST  | Wrong post code error              |
      | b                   |                     | London      | Wiltshire        | W1A 0AX   | Invalid range address line 1 error |
      | 123                 | s                   | London      | Wiltshire        | B33 8TH   | Invalid range address line 2 error |
      | 123                 | Address2            | t           | Wiltshire        | B33 8TH   | Invalid range town error           |
      | 123                 | Address2            | London      | c                | B33 8TH   | Invalid range county error         |
      | ***                 | Address2            | London      | Wiltshire        | W1A 0AX   | Invalid address line 1 error       |
      | Test Address1       | "Test Address2"     | London      | Wiltshire        | W1A 0AX   | Invalid address line 2 error       |
      | 123                 | Address2            | London@123  | Wiltshire        | W1A 0AX   | Invalid town error                 |
      | Address1            | 123                 | London      | (Wiltshire       | W1A 0AX   | Invalid county error               |
      | $ Address1          | Address2            | London      | Wiltshire        | W1A 0AX   | Invalid address line 1 error       |
      | test 123            | $ Address2          | London      | Wiltshire        | W1A 0AX   | Invalid address line 2 error       |
      | Address1            | 123                 | London      | Wiltshire@       | W1A 0AX   | Invalid county error               |

  Scenario Outline: Validate the hyperlinks on Enter Address page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                 |
      | Back link         | Visa Share Code screen |
      | Service Name link | Start screen           |
      | GOV.UK link       | GOV UK screen          |