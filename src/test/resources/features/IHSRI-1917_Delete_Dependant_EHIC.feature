@IHSRI-1917 @DeleteDependantEHIC @Regression

Feature: Validation of the Delete dependant uploaded EHIC Card functionality on the IHS claim app to enable student applicants to correct their dependants uploaded EHIC card.

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Upload Dependant EHIC screen

  Scenario Outline: Validate the user is able to delete the dependant uploaded EHIC file
    And I upload the Dependant EHIC <fileName> of format <fileFormat>
    When I select <deleteLink> on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName><fileFormat> file on <screen>
    Examples:
      | fileName             | fileFormat | deleteLink                      | screen                            |
      | Sample_4             | .bmp       | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen |
      | sample_3             | .pdf       | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen |
      | Payslip-June         | .png       | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen |
      | Large_size_jpeg_file | .jpeg      | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen |
      | Large_size_jpg_file  | .jpg       | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen |
      | Large_size_png_file  | .png       | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen |

  Scenario Outline: Validate the user is able to delete the dependant EHIC file and upload another file
    And I upload the Dependant EHIC <fileName1> of format <fileFormat1>
    When I select <deleteLink> on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    And I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    Then I will see the View Uploaded Dependant EHIC screen
    And I view the uploaded Dependant EHIC <fileName2>
    Examples:
      | fileName1 | fileFormat1 | fileName2 | fileFormat2 | deleteLink                      | screen                            |
      | Sample_4  | .bmp        | sample_3  | .pdf        | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen |

  Scenario Outline: Validate the user is able to delete the dependant EHIC file and not able to upload invalid file
    And I upload the Dependant EHIC <fileName1> of format <fileFormat1>
    When I select <deleteLink> on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    Then I will be displayed <error> for <fileName2><fileFormat2> file
    Examples:
      | fileName1 | fileFormat1 | fileName2     | fileFormat2 | deleteLink                      | screen                            | error                     |
      | Sample_4  | .bmp        | Sample excel  | .xlsx       | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen | Invalid file format error |
      | Sample_4  | .bmp        | 'My Payslip'  | .jpg        | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen | Invalid file name error   |
      | Sample_4  | .bmp        | More_than_2MB | .bmp        | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen | Max file size error       |

  Scenario Outline: Validate the error when the dependant EHIC file is deleted and user continues without uploading another file
    And I upload the Dependant EHIC <fileName> of format <fileFormat>
    When I select <deleteLink> on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName><fileFormat> file on <screen>
    When I Do Not upload the Dependant EHIC file
    Then I will see the <error>
    Examples:
      | fileName | fileFormat | deleteLink                      | screen                            | error                  |
      | Sample_4 | .bmp       | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen | No file selected error |

  Scenario Outline: Validate the back link functionality on Upload EHIC page when the EHIC file is deleted
    And I upload the Dependant EHIC <fileName> of format <fileFormat>
    When I select <deleteLink> on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName><fileFormat> file on <screen>
    When I select the Back link
    Then I will see the Enter Dependant Address screen
    Examples:
      | fileName | fileFormat | deleteLink                      | screen                            |
      | Sample_4 | .bmp       | Delete Dependant EHIC Card link | Upload Dependant EHIC Card screen |

  @IHSRI-2918
  Scenario Outline: Validate the user is able to delete uploaded EHIC file with 'File Name' of maximum 130 characters
    When I upload the Dependant EHIC <fileName> of format <fileFormat>
    And I select <deleteLink> on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName><fileFormat> file on <screen>
    Examples:
      | fileName                                                                                                                           | fileFormat | screen                            | deleteLink                      |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       | Upload Dependant EHIC Card screen | Delete Dependant EHIC Card link |
      | FileNameWith130Characters1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123a  | .pdf       | Upload Dependant EHIC Card screen | Delete Dependant EHIC Card link |