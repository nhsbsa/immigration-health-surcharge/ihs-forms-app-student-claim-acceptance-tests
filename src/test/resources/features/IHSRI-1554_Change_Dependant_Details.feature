@ChangeDependants @IHSRI-1554 @Regression

Feature: Validation of the Change Dependant Details functionality on the IHS claim app to enable student applicants to change the details of their dependants

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Add Dependant screen

  Scenario Outline: Validate the change links in Check Dependants screen when answers are not changed
    When I add a dependant who holds valid EHIC
    And I select <changeLink> in Check Dependants screen
    Then I will see the <screen>
    And my dependant's <answer> is pre-populated
    When I do not change the details and continue
    Then I will see the Check Dependant Details screen
    And my dependant's <answer> is not changed

    Examples:
      | changeLink                  | screen                           | answer          |
      | Change Name link            | Dependant Name screen            | name            |
      | Change Date of Birth link   | Dependant Date of birth screen   | date of birth   |
      | Change IHS Number link      | Dependant IHS Number screen      | IHS number      |
      | Change Visa Share Code link | Dependant Visa Share Code screen | visa share code |
      | Change Address link         | Enter Dependant Address screen   | address         |

  Scenario Outline: Validate the change links in Check Dependants screen when answers are changed
    When I add a dependant who holds valid EHIC
    And I select <changeLink> in Check Dependants screen
    Then I will see the <screen>
    And my dependant's <answer> is pre-populated
    When I change the dependant's <answer> and continue
    Then I will see the Check Dependant Details screen
    And my dependant's <answer> is changed

    Examples:
      | changeLink                  | screen                           | answer          |
      | Change Name link            | Dependant Name screen            | name            |
      | Change Date of Birth link   | Dependant Date of birth screen   | date of birth   |
      | Change IHS Number link      | Dependant IHS Number screen      | IHS number      |
      | Change Visa Share Code link | Dependant Visa Share Code screen | visa share code |
      | Change Address link         | Enter Dependant Address screen   | address         |

  Scenario: Dependant is added with same UK address - Validate user is able to change the dependant's address to different UK address
    When I add a dependant with same UK address
    And I select Change Address link in Check Dependants screen
    Then I will see the Enter Dependant Address screen
    And my applicant's address is pre-populated
    When I change the dependant's address and continue
    Then I will see the Check Dependant Details screen
    And my dependant's address is changed

  Scenario: Dependant is added with different UK address - Validate user is able to change the dependant's address to same UK address
    When I add a dependant with different UK address
    And I select Change Address link in Check Dependants screen
    Then I will see the Enter Dependant Address screen
    And my dependant's address is pre-populated
    When I select the Back link
    Then I will see No radio button selected
    When I change my answer to Yes
    Then I will see the Check Dependant Details screen
    And my dependant's address is updated as applicant's address

  Scenario: Dependant is added with same UK address - Validate user is not able to change the dependant's address through selection but can change the address on enter address form
    When I add a dependant with same UK address
    And I select Change Address link in Check Dependants screen
    Then I will see the Enter Dependant Address screen
    And my applicant's address is pre-populated
    When I select the Back link
    Then I will see Yes radio button selected
    When I change my answer to No
    Then I will see the Check Dependant Details screen
    And my dependant's address is same as applicant's address

  Scenario Outline: Validate the functionality of the Back link from the change links
    When I add a dependant who holds valid EHIC
    And I select <changeLink> in Check Dependants screen
    Then I will see the <screen>
    When I select the Back link
    Then I will see the <previousScreen>
    When I do not change the details and continue
    Then I will see the Check Dependant Details screen

    Examples:
      | changeLink                  | screen                           | previousScreen                   |
      | Change Name link            | Dependant Name screen            | Dependant Living in UK screen    |
      | Change Date of Birth link   | Dependant Date of birth screen   | Dependant Name screen            |
      | Change IHS Number link      | Dependant IHS Number screen      | Dependant Date of birth screen   |
      | Change Visa Share Code link | Dependant Visa Share Code screen | Dependant IHS Number screen      |
      | Change Address link         | Enter Dependant Address screen   | Same Address as Applicant screen |

  @IHSRI-1607 @IHSRI-3399 @IHSRI-3416
  Scenario Outline: Validate that the text field validations are working from the change links
    When I add a dependant who holds valid EHIC
    And I select <changeLink> in Check Dependants screen
    Then I will see the <screen>
    And my dependant's <answer> is pre-populated
    When I change the dependant's answer as <invalidAnswer>
    Then I will see the <output>
    And my dependant's <invalidAnswer> is pre-populated

    Examples:
      | changeLink                  | screen                           | answer          | invalidAnswer            | output                                       |
      | Change Name link            | Dependant Name screen            | name            | blank name               | Blank dependant given name error             |
      | Change Name link            | Dependant Name screen            | name            | invalid name             | Invalid dependant family name error          |
      | Change Date of Birth link   | Dependant Date of birth screen   | date of birth   | blank date of birth      | Blank dependant date of birth error          |
      | Change Date of Birth link   | Dependant Date of birth screen   | date of birth   | invalid date of birth    | Invalid format dependant date of birth error |
      | Change IHS Number link      | Dependant IHS Number screen      | IHS number      | blank IHS number         | Blank dependant ihs number error             |
      | Change IHS Number link      | Dependant IHS Number screen      | IHS number      | invalid IHS number       | Invalid dependant ihs number error           |
      | Change IHS Number link      | Dependant IHS Number screen      | IHS number      | invalid range IHS number | Invalid range dependant ihs number error     |
      | Change Visa Share Code link | Dependant Visa Share Code screen | visa share code | blank visa share code    | Blank dependant share code error             |
      | Change Visa Share Code link | Dependant Visa Share Code screen | visa share code | invalid visa share code  | Invalid dependant share code error           |
      | Change Address link         | Enter Dependant Address screen   | address         | blank address            | Blank dependant building error               |
      | Change Address link         | Enter Dependant Address screen   | address         | invalid address          | Invalid dependant county error               |