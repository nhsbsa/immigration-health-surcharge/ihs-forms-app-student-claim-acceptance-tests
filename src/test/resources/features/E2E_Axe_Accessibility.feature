@Accessibility

Feature: Validation of the accessibility of IHS Student claim app using Axe tool

  Scenario Outline: Validate the end-to-end accessibility of IHS Student claim app
    Given I launch the IHS Student claim application on <browser>
    When My firstname is Henry and surname is O'neil
    And My date of birth is 07 04 1988
    And My IHS number is IHS098765432
    And My visa share code is A12 345 67Z
    And My address is  Test Address1,  Test Address2, London, Wiltshire and EC1A 1BB
    And My Email address is nhsbsa.ihs-testing@nhs.net
    And My Phone number is 07123456789
    And I upload the EHIC Payslip-June of format .png
    And I view the uploaded EHIC Payslip-June
    And I upload the CAS Payslip-June of format .png
    And I view the uploaded CAS Payslip-June
    And I do have dependant to add
    And My dependant is not carrying out paid work in UK
    And My dependant is currently residing in UK
    And My dependant's firstname is John and surname is Dey
    And My dependant's date of birth is 11 03 1990
    And My dependant's IHS number is IHS123456789
    And My dependant's visa share code is A12 345 67Z
    And My dependant does not live in the same UK address
    And My dependant's address is TE, TE, London, Wiltshire and EC1A 1BB
    And I upload the Dependant EHIC Sample_1 of format .jpg
    And I view the uploaded Dependant EHIC Sample_1
    And I do not have another dependant to add
    And I verify my answers and continue
    And I submit the claim application

    Examples:
      | browser |
      | chrome  |
      | firefox |

  Scenario Outline: Validate the accessibility for dependant Not Eligible Reimbursement Refund screen
    Given I launch the IHS Student claim application on <browser>
    And My dependant is not eligible for IHS reimbursement

    Examples:
      | browser |
      | chrome  |
      | firefox |