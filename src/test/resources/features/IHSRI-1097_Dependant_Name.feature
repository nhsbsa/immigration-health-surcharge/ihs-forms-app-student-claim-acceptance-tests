@DependantName @IHSRI-1097 @Regression

Feature: Validation of the Dependant Name Page on IHS claim app to enable student applicants to enter their dependant's given name and family name for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Dependant Name screen

  Scenario Outline: Validate the enter name functionality for positive and negative scenarios
    When My dependant's firstname is <GivenName> and surname is <FamilyName>
    Then I will see the <output>
    Examples:
      | GivenName            | FamilyName          | output                                    |
      #positive tests
      | firstName            | lastName            | Dependant Date of birth screen            |
      | firstName            | O'hara              | Dependant Date of birth screen            |
      | firstName-middleName | lastName            | Dependant Date of birth screen            |
      | FirstName middleName | LastName            | Dependant Date of birth screen            |
      | firstName            | middleName lastName | Dependant Date of birth screen            |
      #negative tests
      |                      | lastName            | Blank dependant given name error          |
      | firstName            |                     | Blank dependant family name error         |
      | (first name)         | last name           | Invalid dependant given name error        |
      | first name 1         | last name           | Invalid dependant given name error        |
      | first name           | "last name"         | Invalid dependant family name error       |
      | first name           | last name 123       | Invalid dependant family name error       |
      | A                    | last name           | Invalid range dependant given name error  |
      | first name           | l                   | Invalid range dependant family name error |

  Scenario Outline: Validate user is not allowed to enter dependant's name more than 50 characters
    When I attempt to enter <count> characters dependant's name
    Then I am restricted to enter <maximum> characters dependant's name
    And I will see the <output>
    Examples:
      | count | maximum | output                         |
      | 51    | 50      | Dependant Date of birth screen |

  Scenario Outline: Validate the hyperlinks on Dependant Name page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                        |
      | Back link         | Dependant Living in UK screen |
      | Service Name link | Start screen                  |
      | GOV.UK link       | GOV UK screen                 |