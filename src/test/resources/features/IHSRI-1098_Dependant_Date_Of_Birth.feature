@DependantDateOfBirth @IHSRI-1098 @Regression

Feature: Validation of the Dependant Date of Birth Page on the IHS claim app to enable student applicants to enter their dependant's date of birth for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Dependant Date of birth screen

  Scenario Outline: Validate the enter date of birth functionality for positive and negative scenarios
    When My dependant's date of birth is <Day> <Month> <Year>
    Then I will see the <output>
    Examples:
      | Day | Month | Year | output                                       |
      #positive tests
      | 30  | 12    | 1983 | Dependant IHS Number screen                  |
      | 1   | 1     | 1989 | Dependant IHS Number screen                  |
      | 02  | 8     | 1990 | Dependant IHS Number screen                  |
      | 12  | 11    | 2021 | Dependant IHS Number screen                  |
      #negative tests
      |     |       |      | Blank dependant date of birth error          |
      |     | 7     |      | Invalid format dependant date of birth error |
      |     |       | 89   | Invalid format dependant date of birth error |
      |     | 8     | 2004 | Invalid format dependant date of birth error |
      | 32  | 12    | 1989 | Invalid format dependant date of birth error |
      | 30  | 02    | 1989 | Invalid format dependant date of birth error |
      | 1-1 | 8     | 1988 | Invalid format dependant date of birth error |
      | DD  | MM    | YYYY | Invalid format dependant date of birth error |
      | 11  | "08"  | 1987 | Invalid format dependant date of birth error |
      | 12  | 03    | 2028 | Future date dependant error                  |

  Scenario Outline: Validate the hyperlinks on Dependant Date of Birth page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                |
      | Back link         | Dependant Name screen |
      | Service Name link | Start screen          |
      | GOV.UK link       | GOV UK screen         |
