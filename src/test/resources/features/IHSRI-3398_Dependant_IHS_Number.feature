@DependantIHSNumber @IHSRI-1099 @IHSRI-3398 @Regression

Feature: Validation of Dependant IHS Number Page on the IHS claim app to enable student applicants to enter their dependant's IHS number for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Dependant IHS Number screen

  Scenario Outline: Validate the enter IHS number functionality with 13 characters for positive and negative scenarios
    When My dependant's IHS number is <IHSNumber>
    Then I will see the <output>

    Examples:
      | IHSNumber     | output                                   |
      #positive tests 13 characters
      | IHSC123456789 | Dependant Visa Share Code screen         |
      | ihsc887744339 | Dependant Visa Share Code screen         |
      | IHSC789654214 | Dependant Visa Share Code screen         |
      | iHsC984888433 | Dependant Visa Share Code screen         |
      #negative tests
      |               | Blank dependant ihs number error         |
      | IHSC12345678  | Invalid dependant ihs number error       |
      | !IHSC1234567  | Invalid dependant ihs number error       |
      | IH1234567890  | Invalid dependant ihs number error       |
      | 1231234567898 | Invalid dependant ihs number error       |
      | IHS1234567890 | Invalid dependant ihs number error       |
      | IGSC123456789 | Invalid dependant ihs number error       |
      | IHSC12345678@ | Invalid dependant ihs number error       |
      | IH$Ć!23485678 | Invalid dependant ihs number error       |
      | 4123232423ihs | Invalid dependant ihs number error       |
      | !ihsc12345678 | Invalid dependant ihs number error       |
      | 1ihsc12345678 | Invalid dependant ihs number error       |
      | ihsc1234567   | Invalid range dependant ihs number error |
      | ihs8c848435   | Invalid range dependant ihs number error |
      | ihs12345678   | Invalid range dependant ihs number error |

  @Retest-3467
  Scenario Outline: Validate the enter IHS number functionality with 12 characters for positive and negative scenarios
    When My dependant's IHS number is <IHSNumber>
    Then I will see the <output>

    Examples:
      | IHSNumber     | output                                   |
      #positive tests 12 characters
      | IHS123456789  | Dependant Visa Share Code screen         |
      | ihs887744339  | Dependant Visa Share Code screen         |
      | IhS789654214  | Dependant Visa Share Code screen         |
      | iHs984888433  | Dependant Visa Share Code screen         |
      #negative tests
      |               | Blank dependant ihs number error         |
      | IH1234567890  | Invalid dependant ihs number error       |
      | @IH1345678900 | Invalid dependant ihs number error       |
      | 123123456789  | Invalid dependant ihs number error       |
      | IGSC12345678  | Invalid dependant ihs number error       |
      | IHS12345678@  | Invalid dependant ihs number error       |
      | ihC123423422  | Invalid dependant ihs number error       |
      | igs723456709  | Invalid dependant ihs number error       |
      | 123932423ihs  | Invalid dependant ihs number error       |
      | !ihs123456789 | Invalid dependant ihs number error       |
      | 1ihs123456789 | Invalid dependant ihs number error       |
      | ihs12345678   | Invalid range dependant ihs number error |
      | ihs1234567    | Invalid range dependant ihs number error |
      | IH123456789   | Invalid range dependant ihs number error |


  Scenario Outline: Validate that user is not allowed to enter ihs number of more than 13 characters
    When I attempt to enter <count> characters dependant IHS number
    Then I am restricted to enter <maximum> characters dependant IHS number
    And I will see the <output>

    Examples:
      | count | maximum | output                           |
      | 14    | 13      | Dependant Visa Share Code screen |

  @IHSRI-2816
  Scenario Outline: Validate the hyperlinks on IHS Number page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink                  | output                                  |
      | Back link                  | Dependant Date of birth screen          |
      | Service Name link          | Start screen                            |
      | GOV.UK link                | GOV UK screen                           |
      | Visas and Immigration link | Contact UK Visas and Immigration screen |