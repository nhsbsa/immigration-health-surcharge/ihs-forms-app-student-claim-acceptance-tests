@IHSRI-1101 @SameAddress @Regression

Feature: Validation of the Same Address Page on the IHS claim app to enable student applicants to select if their dependants live in the same UK address

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Same Address screen

  Scenario Outline: Validate the user is able to select address of the dependants for the claim application
    When My dependant <addressOption> live in the same UK address
    Then I will see the <output>
    Examples:
      | addressOption | output                            |
      | does          | Upload Dependant EHIC Card screen |
      | does not      | Enter Dependant Address screen    |
      |               | Address question error            |

  Scenario Outline: Validate the hyperlinks on Same Address page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                           |
      | Back link         | Dependant Visa Share Code screen |
      | Service Name link | Start screen                     |
      | GOV.UK link       | GOV UK screen                    |