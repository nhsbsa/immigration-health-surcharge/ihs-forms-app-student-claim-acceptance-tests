@IHSRI-1911 @DeleteEHIC @Regression

Feature: Validation of the Delete uploaded EHIC Card functionality on the IHS claim app to enable student applicants to correct their uploaded EHIC card.

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Upload EHIC screen

  Scenario Outline: Validate the user is able to delete the uploaded EHIC file
    And I upload the EHIC <fileName> of format <fileFormat>
    When I select <deleteLink> on the Uploaded EHIC Card screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName><fileFormat> file on <screen>
    Examples:
      | fileName             | fileFormat | deleteLink            | screen                  |
      | Sample_4             | .bmp       | Delete EHIC Card link | Upload EHIC Card screen |
      | sample_3             | .pdf       | Delete EHIC Card link | Upload EHIC Card screen |
      | Payslip-June         | .png       | Delete EHIC Card link | Upload EHIC Card screen |
      | Large_size_jpeg_file | .jpeg      | Delete EHIC Card link | Upload EHIC Card screen |
      | Large_size_jpg_file  | .jpg       | Delete EHIC Card link | Upload EHIC Card screen |
      | Large_size_png_file  | .png       | Delete EHIC Card link | Upload EHIC Card screen |

  Scenario Outline: Validate the user is able to upload another EHIC file after deleting the uploaded file
    And I upload the EHIC <fileName1> of format <fileFormat1>
    When I select <deleteLink> on the Uploaded EHIC Card screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I upload the EHIC <fileName2> of format <fileFormat2>
    Then I will see the View Uploaded EHIC screen
    And I view the uploaded EHIC <fileName2>
    Examples:
      | fileName1 | fileFormat1 | fileName2 | fileFormat2 | deleteLink            | screen                  |
      | Sample_4  | .bmp        | sample_3  | .pdf        | Delete EHIC Card link | Upload EHIC Card screen |

  Scenario Outline: Validate the user is able to delete the uploaded EHIC file and not able to upload invalid file
    And I upload the EHIC <fileName1> of format <fileFormat1>
    When I select <deleteLink> on the Uploaded EHIC Card screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I upload the EHIC <fileName2> of format <fileFormat2>
    Then I will be displayed <error> for <fileName2><fileFormat2> file
    Examples:
      | fileName1 | fileFormat1 | fileName2     | fileFormat2 | deleteLink            | screen                  | error                     |
      | Sample_4  | .bmp        | 'My Payslip'  | .jpg        | Delete EHIC Card link | Upload EHIC Card screen | Invalid file name error   |
      | Sample_4  | .bmp        | More_than_2MB | .bmp        | Delete EHIC Card link | Upload EHIC Card screen | Max file size error       |
      | Sample_4  | .bmp        | Sample excel  | .xlsx       | Delete EHIC Card link | Upload EHIC Card screen | Invalid file format error |

  Scenario Outline: Validate the error when user deletes the EHIC file and continues without uploading another file
    And I upload the EHIC <fileName> of format <fileFormat>
    When I select <deleteLink> on the Uploaded EHIC Card screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName><fileFormat> file on <screen>
    When I Do Not upload the EHIC file
    Then I will see the <error>
    Examples:
      | fileName | fileFormat | deleteLink            | screen                  | error                  |
      | Sample_4 | .bmp       | Delete EHIC Card link | Upload EHIC Card screen | No file selected error |

  Scenario Outline: Validate the back link functionality on Upload EHIC page when the EHIC file is deleted
    And I upload the EHIC <fileName> of format <fileFormat>
    When I select <deleteLink> on the Uploaded EHIC Card screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName><fileFormat> file on <screen>
    When I select the Back link
    Then I will see the Phone number screen
    Examples:
      | fileName | fileFormat | deleteLink            | screen                  |
      | Sample_4 | .bmp       | Delete EHIC Card link | Upload EHIC Card screen |

  @IHSRI-2912
  Scenario Outline: Validate the user is able to delete uploaded EHIC file with 'File Name' of maximum 130 characters
    When I upload the EHIC <fileName> of format <fileFormat>
    And I select <deleteLink> on the Uploaded EHIC Card screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName><fileFormat> file on <screen>
    Examples:
      | fileName                                                                                                                           | fileFormat | screen                  | deleteLink            |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       | Upload EHIC Card screen | Delete EHIC Card link |
      | FileNameWith130Characters1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123a  | .pdf       | Upload EHIC Card screen | Delete EHIC Card link |