@DependantPaidWork @IHSRI-1254 @Regression

Feature: Validation of Dependant Paid Work page to verify the dependant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Dependant Paid Work screen

  @Retest-1300
  Scenario Outline: Validate the various options an applicant can select on Dependant Paid Work page
    When My dependant <workOption> carrying out paid work in UK
    Then I will see the <output>
    Examples:
      | workOption | output                            |
      | is         | Dependant Not Eligible screen     |
      | is not     | Dependant Living in UK screen     |
      |            | Dependant Paid Work Error Message |

  Scenario Outline: Validate the hyperlinks on Dependant Paid Work page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output               |
      | Back link         | Add Dependant screen |
      | Service Name link | Start screen         |
      | GOV.UK link       | GOV UK screen        |