@CheckDependants @DeleteDependant @IHSRI-1273 @IHSRI-1916 @Regression
Feature: Validation of the Check Dependants Page on the IHS claim app to enable student applicants to check the details of their dependants

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Check Dependants screen


  Scenario Outline: Validate the user is able to add multiple dependants to the claim application
    When I <dependantMoreOption> have another dependant to add
    Then I will see the <output>
    Examples:
      | dependantMoreOption | output                         |
      | do                  | Dependant Paid Work screen     |
      | do not              | Check Your Answers screen      |
      |                     | More Dependants question error |

  @Smoke
  Scenario Outline: Validate the hyperlinks on Check Dependants page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink             | output                                                        |
      | Delete Dependant link | Dependant deleted success message for single dependant delete |
      | Service Name link     | Start screen                                                  |
      | GOV.UK link           | GOV UK screen                                                 |

  Scenario Outline: Validate delete dependant hyperlink when multiple dependants are present
    And I do have another dependant to add
    And I add 1 dependants to my claim application
    When I select the Delete Dependant link
    Then I will see the <output>
    Examples:
      | output                                                              |
      | Dependant deleted success message on Check dependant details screen |

