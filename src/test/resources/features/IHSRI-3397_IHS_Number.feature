@IHSNumber @IHSRI-1086 @IHSRI-3397 @Regression

Feature: Validation of IHS Number Page on the IHS claim app to enable student applicants to enter their IHS number for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until IHS Number screen

  Scenario Outline: Validate the enter IHS number functionality with 13 characters for positive and negative scenarios
    When My IHS number is <IHSNumber>
    Then I will see the <output>

    Examples:
      | IHSNumber     | output                         |
      #positive tests 13 characters
      | IHSC123456789 | Visa Share Code screen         |
      | ihsc887744339 | Visa Share Code screen         |
      | IHSC789654214 | Visa Share Code screen         |
      | iHsC984888433 | Visa Share Code screen         |
      #negative tests
      |               | Blank ihs number error         |
      | IHSC12345678  | Invalid ihs number error       |
      | !IHSC12345671 | Invalid ihs number error       |
      | IH11234567890 | Invalid ihs number error       |
      | 1231234567898 | Invalid ihs number error       |
      | IHS1234567890 | Invalid ihs number error       |
      | IGSC123456789 | Invalid ihs number error       |
      | IHSC12345678@ | Invalid ihs number error       |
      | IH$Ć!23485678 | Invalid ihs number error       |
      | 4123232423ihs | Invalid ihs number error       |
      | !IHSC12345678 | Invalid ihs number error       |
      | 1IHS1C2345678 | Invalid ihs number error       |
      | ihsc1234567   | Invalid range ihs number error |
      | ihs8c848435   | Invalid range ihs number error |
      | ihsc123456    | Invalid range ihs number error |

  @Retest-3467
  Scenario Outline: Validate the enter IHS number functionality with 12 characters for positive and negative scenarios
    When My IHS number is <IHSNumber>
    Then I will see the <output>

    Examples:
      | IHSNumber     | output                         |
      #positive tests 12 characters
      | IHS123456789  | Visa Share Code screen         |
      | ihs887744339  | Visa Share Code screen         |
      | IhS789654214  | Visa Share Code screen         |
      | iHs984888433  | Visa Share Code screen         |
      #negative tests
      |               | Blank ihs number error         |
      | IH1234567890  | Invalid ihs number error       |
      | @IH134567890  | Invalid ihs number error       |
      | 123123456789  | Invalid ihs number error       |
      | IGSC12345678  | Invalid ihs number error       |
      | IHS12345678@  | Invalid ihs number error       |
      | ihC123423422  | Invalid ihs number error       |
      | igs123456709  | Invalid ihs number error       |
      | 123232423ihs  | Invalid ihs number error       |
      | @IHS123459810 | Invalid ihs number error       |
      | 1IHS123456780 | Invalid ihs number error       |
      | ihs12345678   | Invalid range ihs number error |
      | ihs1234567    | Invalid range ihs number error |
      | IH123456789   | Invalid range ihs number error |

  Scenario Outline: Validate that user is not allowed to enter ihs number of more than 13 characters
    When I attempt to enter <count> characters IHS number
    Then I am restricted to enter <maximum> characters IHS number
    And I will see the <output>

    Examples:
      | count | maximum | output                 |
      | 14    | 13      | Visa Share Code screen |

  @IHSRI-2816
  Scenario Outline: Validate the hyperlinks on IHS Number page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink                  | output                                  |
      | Back link                  | Date of birth screen                    |
      | Service Name link          | Start screen                            |
      | GOV.UK link                | GOV UK screen                           |
      | Visas and Immigration link | Contact UK Visas and Immigration screen |