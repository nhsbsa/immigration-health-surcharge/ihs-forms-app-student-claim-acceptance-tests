@IHSRI-1475

Feature: Validation of the IHS Student Claim end-to-end functionality in various SauceLabs devices to meet the compatibility test criteria

@Compatibility
  Scenario Outline: Verify that the user is able to submit a new student claim with a dependant
    Given I launch the IHS Student claim application
    And My details are captured until Phone Number screen
    And My Phone number is <PhoneNumber>
    And I upload EHIC <fileName> of format <fileFormat>
    And I view the uploaded EHIC <fileName>
    And I upload CAS <fileName> of format <fileFormat>
    And I view the uploaded CAS <fileName>
    And I <dependantOption> have dependant to add
    And I add dependant to my claim application
    And I check my answers and submit the claim application
    Then My claim is submitted successfully
    And I validate the output

    Examples:
      | PhoneNumber | dependantOption | fileName     | fileFormat |
      | 07123456789 | do              | Payslip-June | .png       |