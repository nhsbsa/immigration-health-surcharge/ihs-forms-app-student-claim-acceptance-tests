@ChangeDependantEHIC @IHSRI-2051 @Regression

Feature: Validation of Change links functionality for Dependant files uploaded on the IHS Claim app to enable student applicants to change the files of their dependants before submitting the claim

  Background:
    Given I launch the IHS Student claim application

  Scenario Outline: Validate the functionality of change link on Check Dependants page for uploaded EHIC file when the file is not changed
    And My details are captured until Check Dependants screen
    When I select <changeLink> in Check Dependants screen
    Then I will see the View Uploaded Dependant EHIC screen
    And  My dependant's uploaded EHIC file is pre-populated as <fileName><fileFormat>
    When I do not change the details and continue
    Then I will see the Check Dependant Details screen
    And My dependant's uploaded EHIC file remains same as <fileName><fileFormat> on <screen>

    Examples:
      | fileName | fileFormat | changeLink                 | screen                         |
      | payslip4 | .png       | Change Files Uploaded link | Check Dependant Details screen |

  Scenario Outline: Validate the functionality of change link on Check Dependants page for uploaded EHIC file when the file is changed
    And My details are captured until Check Dependants screen
    When I select Change Files Uploaded link in Check Dependants screen
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName1><fileFormat1>
    When I select Delete Dependant EHIC Card link on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    And I view the uploaded Dependant EHIC <fileName2>
    Then I will see the Check Dependant Details screen
    And My dependant's uploaded EHIC file is changed to <fileName2><fileFormat2> on Check Dependant Details screen

    Examples:
      | fileName1 | fileFormat1 | fileName2 | fileFormat2 | screen                            |
      | payslip4  | .png        | 123456    | .pdf        | Upload Dependant EHIC Card screen |

  Scenario Outline: Validate the error when invalid file is uploaded using the change link on Check Dependants page for uploaded EHIC file
    And My details are captured until Check Dependants screen
    When I select Change Files Uploaded link in Check Dependants screen
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName1><fileFormat1>
    When I select Delete Dependant EHIC Card link on the Uploaded Dependant EHIC screen
    Then I will see the Upload Dependant EHIC Card screen
    And I will see successful deletion message for <fileName1><fileFormat1> file on Upload Dependant EHIC Card screen
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    Then I will be displayed <error> for <fileName2><fileFormat2> file

    Examples:
      | fileName1 | fileFormat1 | fileName2     | fileFormat2 | error                     |
      | payslip4  | .png        | Sample excel  | .xlsx       | Invalid file format error |
      | payslip4  | .png        | 'My Payslip'  | .jpg        | Invalid file name error   |
      | payslip4  | .png        | More_than_2MB | .bmp        | Max file size error       |

  @IHSRI-2136
  Scenario Outline: Validate the functionality of back link on Upload Dependant EHIC Card page using the change link on Check Dependants page when dependant's address is same as applicant
    And My details are captured until Check Dependants screen
    When I select Change Files Uploaded link in Check Dependants screen
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName1><fileFormat1>
    When I select Delete Dependant EHIC Card link on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I select the Back link
    Then I will see the Same Address as Applicant screen
    When My dependant  live in the same UK address
    Then I will see the Upload Dependant EHIC Card screen
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    And I view the uploaded Dependant EHIC <fileName2>
    Then I will see the Check Dependant Details screen
    And My dependant's uploaded EHIC file is changed to <fileName2><fileFormat2> on Check Dependant Details screen

    Examples:
      | fileName1 | fileFormat1 | fileName2 | fileFormat2 | screen                            |
      | payslip4  | .png        | 123456    | .pdf        | Upload Dependant EHIC Card screen |

  @IHSRI-2136
  Scenario Outline: Validate the functionality of back link on Upload Dependant EHIC Card page using the change link on Check Dependants page when dependant's address is different
    And My details are captured until Add Dependant screen
    And I add a dependant who holds valid EHIC
    When I select Change Files Uploaded link in Check Dependants screen
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName1><fileFormat1>
    When I select Delete Dependant EHIC Card link on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I select the Back link
    Then I will see the Enter Dependant Address screen
    When I continue without changing my answer on Enter Dependant Address screen
    Then I will see the Upload Dependant EHIC Card screen
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    And I view the uploaded Dependant EHIC <fileName2>
    Then I will see the Check Dependant Details screen
    And My dependant's uploaded EHIC file is changed to <fileName2><fileFormat2> on Check Dependant Details screen

    Examples:
      | fileName1 | fileFormat1 | fileName2 | fileFormat2 | screen                            |
      | 123456    | .pdf        | payslip4  | .png        | Upload Dependant EHIC Card screen |

  @IHSRI-2136
  Scenario Outline: Validate the functionality of back link on Upload Dependant EHIC Card page using the change link on Check Dependants page when dependant's address is same as applicant
    And My details are captured until Check Dependants screen
    When I select Change Files Uploaded link in Check Dependants screen
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName1><fileFormat1>
    When I select Delete Dependant EHIC Card link on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I select the Back link
    Then I will see the Same Address as Applicant screen
    When My dependant does not live in the same UK address
    Then I will see the Upload Dependant EHIC Card screen
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    And I view the uploaded Dependant EHIC <fileName2>
    Then I will see the Check Dependant Details screen
    And My dependant's uploaded EHIC file is changed to <fileName2><fileFormat2> on Check Dependant Details screen

    Examples:
      | fileName1 | fileFormat1 | fileName2 | fileFormat2 | screen                            |
      | payslip4  | .png        | 123456    | .pdf        | Upload Dependant EHIC Card screen |

  @Retest-2138
  Scenario Outline: Validate the functionality of change link on Check Your Answers page for dependant's uploaded EHIC file when the file is not changed
    When My details are captured until Check your answers screen with Dependant
    And I select <changeLink> on Check Your Answers page
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName><fileFormat>
    When I continue without changing my answer on View Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And My dependant's uploaded EHIC file remains same as <fileName><fileFormat> on <screen>

    Examples:
      | fileName | fileFormat | changeLink                                 | screen                    |
      | payslip4 | .png       | Change Files Uploaded link for Dependant 1 | Check Your Answers screen |

  @Retest-2138
  Scenario Outline: Validate the functionality of change link on Check Your Answers page for dependant's uploaded EHIC file when the file is changed
    When My details are captured until Check your answers screen with Dependant
    And I select Change Files Uploaded link for Dependant 1 on Check Your Answers page
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName1><fileFormat1>
    When I select Delete Dependant EHIC Card link on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    And I view the uploaded Dependant EHIC <fileName2>
    Then I will see the Check Your Answers screen
    And My dependant's uploaded EHIC file is changed to <fileName2><fileFormat2> on Check Your Answers screen

    Examples:
      | fileName1 | fileFormat1 | fileName2 | fileFormat2 | screen                            |
      | payslip4  | .png        | 123456    | .pdf        | Upload Dependant EHIC Card screen |

  Scenario Outline: Validate the error when an invalid file is uploaded using the change link on Check Your Answers page for dependant's uploaded EHIC file
    When My details are captured until Check your answers screen with Dependant
    And I select Change Files Uploaded link for Dependant 1 on Check Your Answers page
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName1><fileFormat1>
    When I select Delete Dependant EHIC Card link on the Uploaded Dependant EHIC screen
    Then I will see the Upload Dependant EHIC Card screen
    And I will see successful deletion message for <fileName1><fileFormat1> file on Upload Dependant EHIC Card screen
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    Then I will be displayed <error> for <fileName2><fileFormat2> file

    Examples:
      | fileName1 | fileFormat1 | fileName2     | fileFormat2 | error                     |
      | payslip4  | .png        | Sample excel  | .xlsx       | Invalid file format error |
      | payslip4  | .png        | 'My Payslip'  | .jpg        | Invalid file name error   |
      | payslip4  | .png        | More_than_2MB | .bmp        | Max file size error       |

  @IHSRI-2136
  Scenario Outline: Validate the functionality of back link on Upload Dependant EHIC Card page using the change link on Check Your Answers page when dependant's address is same as applicant
    When My details are captured until Check your answers screen with Dependant
    And I select <changeLink> on Check Your Answers page
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName1><fileFormat1>
    When I select Delete Dependant EHIC Card link on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I select the Back link
    Then I will see the Same Address as Applicant screen
    When My dependant  live in the same UK address
    Then I will see the Upload Dependant EHIC Card screen
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    And I view the uploaded Dependant EHIC <fileName2>
    Then I will see the Check Your Answers screen
    And My dependant's uploaded EHIC file is changed to <fileName2><fileFormat2> on Check Your Answers screen

    Examples:
      | fileName1 | fileFormat1 | fileName2 | fileFormat2 | changeLink                                 | screen                            |
      | payslip4  | .png        | 123456    | .pdf        | Change Files Uploaded link for Dependant 1 | Upload Dependant EHIC Card screen |

  @IHSRI-2136
  Scenario Outline: Validate the functionality of back link on Upload Dependant EHIC Card page using the change link on Check Your Answers page when dependant's address is different
    When My details are captured until Check your answers screen with different address for Dependant
    And I select <changeLink> on Check Your Answers page
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName1><fileFormat1>
    When I select Delete Dependant EHIC Card link on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I select the Back link
    Then I will see the Enter Dependant Address screen
    When I click on continue button on Enter Dependant Address screen
    Then I will see the Upload Dependant EHIC Card screen
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    And I view the uploaded Dependant EHIC <fileName2>
    Then I will see the Check Your Answers screen
    And My dependant's uploaded EHIC file is changed to <fileName2><fileFormat2> on Check Your Answers screen

    Examples:
      | fileName1 | fileFormat1 | fileName2 | fileFormat2 | changeLink                                 | screen                            |
      | payslip4  | .png        | 123456    | .pdf        | Change Files Uploaded link for Dependant 1 | Upload Dependant EHIC Card screen |

  @IHSRI-2136 @Retest-2157
  Scenario Outline: Validate the functionality of back link on Upload Dependant EHIC Card page using the change link on Check Your Answers page when dependant changes the same address to different address
    When My details are captured until Check your answers screen with Dependant
    And I select <changeLink> on Check Your Answers page
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName1><fileFormat1>
    When I select Delete Dependant EHIC Card link on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I select the Back link
    Then I will see the Same Address as Applicant screen
    When My dependant does not live in the same UK address
    Then I will see the Upload Dependant EHIC Card screen
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    And I view the uploaded Dependant EHIC <fileName2>
    Then I will see the Check Your Answers screen
    And My dependant's uploaded EHIC file is changed to <fileName2><fileFormat2> on Check Your Answers screen

    Examples:
      | fileName1 | fileFormat1 | fileName2 | fileFormat2 | changeLink                                 | screen                            |
      | payslip4  | .png        | 123456    | .pdf        | Change Files Uploaded link for Dependant 1 | Upload Dependant EHIC Card screen |

  @IHSRI-2918
  Scenario Outline: Validate the functionality of change link on Check Your Answers page for dependant's uploaded EHIC file when the 'File Name' with maximum 130 characters is changed
    When My details are captured until Upload Dependant EHIC screen
    And I upload the Dependant EHIC with name <fileName1> <fileFormat1>
    And I view the uploaded Dependant EHIC <fileName1><fileFormat1>
    And I do not have another dependant to add
    And I select Change Files Uploaded link for Dependant 1 on Check Your Answers page
    Then I will see the View Uploaded Dependant EHIC screen
    And My dependant's uploaded EHIC file is pre-populated as <fileName1><fileFormat1>
    When I select Delete Dependant EHIC Card link on the Uploaded Dependant EHIC screen
    Then I will see the <screen>
    And I will see successful deletion message for <fileName1><fileFormat1> file on <screen>
    When I upload the Dependant EHIC <fileName2> of format <fileFormat2>
    And I view the uploaded Dependant EHIC <fileName2><fileFormat2>
    Then I will see the Check Your Answers screen
    And My dependant's uploaded EHIC file is changed to <fileName2><fileFormat2> on Check Your Answers screen

    Examples:
      | fileName1                                                                                                                         | fileFormat1 | fileName2                                                                                                                          | fileFormat2 | screen                            |
      | FileNameWith130Characters1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123a | .pdf        | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png        | Upload Dependant EHIC Card screen |