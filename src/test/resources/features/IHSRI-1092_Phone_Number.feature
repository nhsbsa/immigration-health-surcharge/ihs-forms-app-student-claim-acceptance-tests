@PhoneNumber @IHSRI-1092 @Regression

Feature: Validation of Phone Number Page on the IHS claim app to enable student applicants to enter their phone number for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Phone Number screen

  @Retest-1299
  Scenario Outline: Validate the enter phone number functionality for positive and negative scenarios
    When My Phone number is <PhoneNumber>
    Then I will see the <output>
    Examples:
      | PhoneNumber        | output                           |
      #positive tests
      |                    | Upload EHIC Card screen          |
      | 09876543245        | Upload EHIC Card screen          |
      | 0987 654 3245      | Upload EHIC Card screen          |
      | 765 456 7897       | Upload EHIC Card screen          |
      | +449876543245      | Upload EHIC Card screen          |
      | (212)9876543245    | Upload EHIC Card screen          |
      | 0-987-654-3245     | Upload EHIC Card screen          |
      | +44-987-654-3245   | Upload EHIC Card screen          |
      | +44 987 654 3245   | Upload EHIC Card screen          |
      | (212) 987 654 3245 | Upload EHIC Card screen          |
      #negative tests
      | *7653456789        | Invalid phone number error       |
      | [212]9876543245    | Invalid phone number error       |
      | {44}9876543245     | Invalid phone number error       |
      | '447654567897'     | Invalid phone number error       |
      | my phone number    | Invalid phone number error       |
      | 7654567897         | Invalid range phone number error |

  Scenario Outline: Validate the hyperlinks on Phone Number page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output               |
      | Back link         | Email Address screen |
      | Service Name link | Start screen         |
      | GOV.UK link       | GOV UK screen        |