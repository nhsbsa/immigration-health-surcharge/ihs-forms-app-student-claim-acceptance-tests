@DependantResidingUK @IHSRI-1255 @Regression

Feature: Validation of Dependant Living in UK page to verify the dependant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Dependant Living in UK screen

  Scenario Outline: Validate the various options an applicant can select on Dependant Living in UK page
    When My dependant <residingOption> currently residing in UK
    Then I will see the <output>
    Examples:
      | residingOption | output                               |
      | is             | Dependant Name screen                |
      | is not         | Dependant Not Eligible screen        |
      |                | Dependant Living in UK Error Message |

  Scenario Outline: Validate the hyperlinks on Dependant Living in UK page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                     |
      | Back link         | Dependant Paid Work screen |
      | Service Name link | Start screen               |
      | GOV.UK link       | GOV UK screen              |

  @IHSRI-1304 @IHSRI-2816
  Scenario Outline: Validate the hyperlinks on Dependant Not Eligible page
    When My dependant is not currently residing in UK
    Then I will see the Dependant Not Eligible screen
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output               |
      | More Info link    | Service Start screen |