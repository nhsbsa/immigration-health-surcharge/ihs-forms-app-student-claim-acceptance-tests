@CheckYourAnswers @Declaration @Confirmation @Regression

Feature: Validation of Check Your Answers page on Claim web app to enable the users to check the summary of their answers before submitting the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Check your answers screen

  @IHSRI-1274
  Scenario Outline: Validate the hyperlinks on Check Your Answers page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output               |
      | Back link         | Add Dependant screen |
      | Service Name link | Start screen         |
      | GOV.UK link       | GOV UK screen        |

  @IHSRI-1275 @IHSRI-2922
  Scenario Outline: Validate the hyperlinks on Declaration page
    When I verify my answers and continue
    And I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                    |
      | Back link         | Check Your Answers screen |
      | Privacy info link | Privacy Notice screen     |
      | Service Name link | Start screen              |
      | GOV.UK link       | GOV UK screen             |

  @IHSRI-1280 @IHSRI-1912
  Scenario Outline: Validate the hyperlinks on Confirmation page
    When I check my answers and submit the claim application
    And I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink                  | output                         |
      | End of service survey link | Gov UK Student Feedback screen |
      | Confirmation Contact link  | Contact Us screen              |
      | Service Name link          | Start screen                   |
      | GOV.UK link                | GOV UK screen                  |