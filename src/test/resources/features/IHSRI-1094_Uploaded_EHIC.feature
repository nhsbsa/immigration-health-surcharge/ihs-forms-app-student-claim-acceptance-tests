@ViewEHIC @IHSRI-1094 @Regression

Feature: Validation of the Uploaded EHIC Card page on the IHS claim app to enable student applicants to view their uploaded EHIC card to prove their eligibility.

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Upload EHIC screen

  @Smoke @IHSRI-1571
  Scenario Outline: Validate the user is able to view the uploaded EHIC file
    When I upload the EHIC <fileName> of format <fileFormat>
    And I view the uploaded EHIC <fileName>
    Then I will see the <output>
    Examples:
      | fileName             | fileFormat | output                   |
      | Sample_4             | .bmp       | Upload CAS Letter screen |
      | sample_3             | .pdf       | Upload CAS Letter screen |
      | Payslip-June         | .png       | Upload CAS Letter screen |
      | Large_size_jpeg_file | .jpeg      | Upload CAS Letter screen |
      | Large_size_jpg_file  | .jpg       | Upload CAS Letter screen |
      | Large_size_png_file  | .png       | Upload CAS Letter screen |

  @IHSRI-2912
  Scenario Outline: Validate the user is able to view uploaded EHIC file when 'File Name' with maximum 130 characters is added
    When I upload the EHIC <fileName> of format <fileFormat>
    And I view the uploaded EHIC <fileName><fileFormat>
    Then I will see the Upload CAS Letter screen
    Examples:
      | fileName                                                                                                                           | fileFormat |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       |
      | FileNameWith130Characters1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123a  | .pdf       |

  Scenario Outline: Validate the hyperlinks on View EHIC page
    When I <uploadFile> upload the EHIC file
    And I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | uploadFile | hyperlink         | output        |
      | Do         | Service Name link | Start screen  |
      | Do         | GOV.UK link       | GOV UK screen |