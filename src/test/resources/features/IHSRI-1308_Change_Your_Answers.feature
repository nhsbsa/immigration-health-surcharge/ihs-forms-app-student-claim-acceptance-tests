@CheckYourAnswers @IHSRI-1308 @Regression

Feature: Validation of Change Your Answers functionality on Claim web app to enable users to change their answers before submitting the claim application

  Background:
    Given I launch the IHS Student claim application

  Scenario Outline: Validate the change links on Check Your Answers page without Dependant - do not change the answers and verify answer remains same
    And My applicant details are captured until Check your answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                  | screen                 | answer          |
      | Change Name link            | Name screen            | Name            |
      | Change Date of birth link   | Date of birth screen   | Date of birth   |
      | Change IHS number link      | IHS Number screen      | IHS Number      |
      | Change Visa Share Code link | Visa Share Code screen | Visa Share Code |
      | Change Address link         | Enter Address screen   | Address         |
      | Change Email address link   | Email Address screen   | Email           |
      | Change Phone number link    | Phone number screen    | Phone Number    |

  Scenario Outline: Validate the change links on Check Your Answers page with Dependant having same address as applicant - do not change the answers and verify answer remains same
    And My details are captured until Check your answers screen with Dependant
    And I will see the Check Your Answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                                  | screen                           | answer                |
      | Change Name link                            | Name screen                      | Name                  |
      | Change Date of birth link                   | Date of birth screen             | Date of birth         |
      | Change IHS number link                      | IHS Number screen                | IHS Number            |
      | Change Visa Share Code link                 | Visa Share Code screen           | Visa Share Code       |
      | Change Address link                         | Enter Address screen             | Address               |
      | Change Email address link                   | Email Address screen             | Email                 |
      | Change Phone number link                    | Phone number screen              | Phone Number          |
      | Change Name link for Dependant 1            | Dependant Name screen            | Dep Name              |
      | Change Date of birth link for Dependant 1   | Dependant Date of birth screen   | Dep Date of Birth     |
      | Change IHS number link for Dependant 1      | Dependant IHS Number screen      | Dep IHS Number        |
      | Change Visa Share Code link for Dependant 1 | Dependant Visa Share Code screen | Dep Visa Share Code   |
      | Change Address link for Dependant 1         | Enter Dependant Address screen   | Dep With Same Address |

  Scenario Outline: Validate the change links on Check Your Answers page with Dependant having different address - do not change the answers and verify answer remains same
    And My details are captured until Check your answers screen with different address for Dependant
    And I will see the Check Your Answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the Check Your Answers screen
    And My answer remains same on <screen> having different address

    Examples:
      | changeLink                          | screen                         | answer                     |
      | Change Address link for Dependant 1 | Enter Dependant Address screen | Dep With Different Address |

  Scenario Outline: Validate the back link functionality on Check Your Answers page without changing the answers
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I select the Back link
    Then I will see the <previous screen>
    When I continue without changing my answer on <previous screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                                  | screen                           | previous screen                  | answer                |
      | Change Date of birth link                   | Date of birth screen             | Name screen                      | Date of birth         |
      | Change IHS number link                      | IHS Number screen                | Date of birth screen             | IHS Number            |
      | Change Visa Share Code link                 | Visa Share Code screen           | IHS Number screen                | Visa Share Code       |
      | Change Address link                         | Enter Address screen             | Visa Share Code screen           | Address               |
      | Change Email address link                   | Email Address screen             | Enter Address screen             | Email                 |
      | Change Phone number link                    | Phone number screen              | Email Address screen             | Phone Number          |
      | Change Name link for Dependant 1            | Dependant Name screen            | Dependant Living in UK screen    | Dep Name              |
      | Change Date of birth link for Dependant 1   | Dependant Date of birth screen   | Dependant Name screen            | Dep Date of Birth     |
      | Change IHS number link for Dependant 1      | Dependant IHS Number screen      | Dependant Date of birth screen   | Dep IHS Number        |
      | Change Visa Share Code link for Dependant 1 | Dependant Visa Share Code screen | Dependant IHS Number screen      | Dep Visa Share Code   |
      | Change Address link for Dependant 1         | Enter Dependant Address screen   | Same Address as Applicant screen | Dep With Same Address |

  Scenario Outline: Validate the back link functionality on Check Your Answers page by changing the answers
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change my <answer> and do not continue
    And I select the Back link
    Then I will see the <previous screen>
    And I continue without changing my answer on <previous screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                                  | screen                           | previous screen                  | answer                |
      | Change Date of birth link                   | Date of birth screen             | Name screen                      | Date of birth         |
      | Change IHS number link                      | IHS Number screen                | Date of birth screen             | IHS Number            |
      | Change Visa Share Code link                 | Visa Share Code screen           | IHS Number screen                | Visa Share Code       |
      | Change Address link                         | Enter Address screen             | Visa Share Code screen           | Address               |
      | Change Email address link                   | Email Address screen             | Enter Address screen             | Email                 |
      | Change Phone number link                    | Phone number screen              | Email Address screen             | Phone Number          |
      | Change Name link for Dependant 1            | Dependant Name screen            | Dependant Living in UK screen    | Dep Name              |
      | Change Date of birth link for Dependant 1   | Dependant Date of birth screen   | Dependant Name screen            | Dep Date of Birth     |
      | Change IHS number link for Dependant 1      | Dependant IHS Number screen      | Dependant Date of birth screen   | Dep IHS Number        |
      | Change Visa Share Code link for Dependant 1 | Dependant Visa Share Code screen | Dependant IHS Number screen      | Dep Visa Share Code   |
      | Change Address link for Dependant 1         | Enter Dependant Address screen   | Same Address as Applicant screen | Dep With Same Address |

  Scenario Outline: Validate the change links on Check Your Answers page without Dependant - change the answer and verify answers are updated
    And My applicant details are captured until Check your answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    When I change my <answer> and continue
    Then I will see the Check Your Answers screen
    And I see the updated <answer> on Check Your Answers screen

    Examples:
      | changeLink                  | screen                 | answer          |
      | Change Name link            | Name screen            | Name            |
      | Change Date of birth link   | Date of birth screen   | Date of birth   |
      | Change IHS number link      | IHS Number screen      | IHS Number      |
      | Change Visa Share Code link | Visa Share Code screen | Visa Share Code |
      | Change Address link         | Enter Address screen   | Address         |
      | Change Email address link   | Email Address screen   | Email           |
      | Change Phone number link    | Phone number screen    | Phone Number    |

  Scenario Outline: Validate the change links on Check Your Answers page with Dependant - change the answer and verify answers are updated
    And My details are captured until Check your answers screen with different address for Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    When I change my <answer> and continue
    Then I will see the Check Your Answers screen
    And I see the updated <answer> on Check Your Answers screen

    Examples:
      | changeLink                                  | screen                           | answer                     |
      | Change Name link                            | Name screen                      | Name                       |
      | Change Date of birth link                   | Date of birth screen             | Date of birth              |
      | Change IHS number link                      | IHS Number screen                | IHS Number                 |
      | Change Visa Share Code link                 | Visa Share Code screen           | Visa Share Code            |
      | Change Address link                         | Enter Address screen             | Address                    |
      | Change Email address link                   | Email Address screen             | Email                      |
      | Change Phone number link                    | Phone number screen              | Phone Number               |
      | Change Name link for Dependant 1            | Dependant Name screen            | Dep Name                   |
      | Change Date of birth link for Dependant 1   | Dependant Date of birth screen   | Dep Date of Birth          |
      | Change IHS number link for Dependant 1      | Dependant IHS Number screen      | Dep IHS Number             |
      | Change Visa Share Code link for Dependant 1 | Dependant Visa Share Code screen | Dep Visa Share Code        |
      | Change Address link for Dependant 1         | Enter Dependant Address screen   | Dep With Different Address |

  Scenario Outline: Validate the Dependant Address change link on Check Your Answers page - change the address and verify address is updated
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <address> is pre-populated
    When I change my <answer> and continue
    Then I will see the Check Your Answers screen
    And I see the updated <answer> on Check Your Answers screen

    Examples:
      | changeLink                          | screen                         | answer                     | address               |
      | Change Address link for Dependant 1 | Enter Dependant Address screen | Dep With Different Address | Dep With Same Address |

  @IHSRI-1604
  Scenario Outline: Validate the Dependant Address change link on Check Your Answers page - change answer on Same Address screen and verify address remains same
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I select the Back link
    Then I will see the Same Address as Applicant screen
    When I change my <answer> and continue
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                          | answer                | screen                         |
      | Change Address link for Dependant 1 | Dep With Same Address | Enter Dependant Address screen |

  @IHSRI-1605
  Scenario Outline: Validate the Applicant Address change link on Check Your Answers page - change Applicant address and verify Dependant address gets updated
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's Address is pre-populated
    When I change my Address and continue
    And I see the updated Address on Check Your Answers screen
    And I see the updated Dep With Same Address on Check Your Answers screen

    Examples:
      | changeLink          | screen               |
      | Change Address link | Enter Address screen |

  @IHSRI-3400
  Scenario Outline: Validate the errors when incorrect answers are entered using change links on Check Your Answers page
    And My details are captured until Check your answers screen with Dependant
    And I will see the Check Your Answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <detail> is pre-populated
    When I change the answer to <invalidAnswer>
    Then I will see the <output>

    Examples:
      | changeLink                                  | screen                           | invalidAnswer                      | output                                       | detail                |
      | Change Name link                            | Name screen                      | Blank Given Name                   | Blank given name error                       | Name                  |
      | Change Name link                            | Name screen                      | Blank Family Name                  | Blank family name error                      | Name                  |
      | Change Name link                            | Name screen                      | Invalid Given Name                 | Invalid given name error                     | Name                  |
      | Change Date of birth link                   | Date of birth screen             | Blank Date of birth                | Blank date of birth error                    | Date of birth         |
      | Change Date of birth link                   | Date of birth screen             | Invalid Date of birth              | Invalid date of birth error                  | Date of birth         |
      | Change IHS number link                      | IHS Number screen                | Blank IHS Number                   | Blank ihs number error                       | IHS Number            |
      | Change IHS number link                      | IHS Number screen                | Invalid IHS Number                 | Invalid ihs number error                     | IHS Number            |
      | Change IHS number link                      | IHS Number screen                | Invalid range IHS Number           | Invalid range ihs number error               | IHS Number            |
      | Change Visa Share Code link                 | Visa Share Code screen           | Blank Visa Share Code              | Blank share code error                       | Visa Share Code       |
      | Change Visa Share Code link                 | Visa Share Code screen           | Invalid Visa Share Code            | Invalid share code error                     | Visa Share Code       |
      | Change Address link                         | Enter Address screen             | Blank Building                     | Blank building error                         | Address               |
      | Change Address link                         | Enter Address screen             | Blank Town                         | Blank town error                             | Address               |
      | Change Address link                         | Enter Address screen             | Invalid Postcode                   | Wrong post code error                        | Address               |
      | Change Email address link                   | Email Address screen             | Blank Email                        | Blank email address error                    | Email                 |
      | Change Email address link                   | Email Address screen             | Invalid Email                      | Invalid email address error                  | Email                 |
      | Change Phone number link                    | Phone number screen              | Invalid Phone Number               | Invalid phone number error                   | Phone Number          |
      | Change Name link for Dependant 1            | Dependant Name screen            | Blank Dependant Given Name         | Blank dependant given name error             | Dep Name              |
      | Change Name link for Dependant 1            | Dependant Name screen            | Invalid Dependant Family Name      | Invalid dependant family name error          | Dep Name              |
      | Change Date of birth link for Dependant 1   | Dependant Date of birth screen   | Blank Dependant Date of Birth      | Blank dependant date of birth error          | Dep Date of Birth     |
      | Change Date of birth link for Dependant 1   | Dependant Date of birth screen   | Invalid Dependant Date of Birth    | Invalid format dependant date of birth error | Dep Date of Birth     |
      | Change IHS number link for Dependant 1      | Dependant IHS Number screen      | Blank Dependant IHS Number         | Blank dependant ihs number error             | Dep IHS Number        |
      | Change IHS number link for Dependant 1      | Dependant IHS Number screen      | Invalid Dependant IHS Number       | Invalid dependant ihs number error           | Dep IHS Number        |
      | Change IHS number link for Dependant 1      | Dependant IHS Number screen      | Invalid range Dependant IHS number | Invalid range dependant ihs number error     | Dep IHS Number        |
      | Change Visa Share Code link for Dependant 1 | Dependant Visa Share Code screen | Blank Dependant Visa Share Code    | Blank dependant share code error             | Dep Visa Share Code   |
      | Change Visa Share Code link for Dependant 1 | Dependant Visa Share Code screen | Invalid Dependant Visa Share Code  | Invalid dependant share code error           | Dep Visa Share Code   |
      | Change Address link for Dependant 1         | Enter Dependant Address screen   | Blank Dependant Post Code          | Blank dependant post code error              | Dep With Same Address |
      | Change Address link for Dependant 1         | Enter Dependant Address screen   | Invalid Dependant Building         | Invalid dependant building error             | Dep With Same Address |