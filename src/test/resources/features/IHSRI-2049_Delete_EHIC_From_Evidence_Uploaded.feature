@DeleteUploadedEHIC @IHSRI-2049 @Regression

Feature: Validation of the Delete uploaded EHIC Card functionality on the Uploaded Evidence Screen to enable students to correct their uploaded EHIC letter.

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Upload EHIC screen

  @Smoke
  Scenario Outline: Validate the user is able to upload another EHIC file after deleting it from Uploaded Evidence screen
    When I upload the EHIC <fileName1> of format <fileFormat1>
    And I view the uploaded EHIC <fileName1>
    And I upload the CAS <fileName1> of format <fileFormat1>
    And I will see the View Uploaded Evidence screen
    And I select delete EHIC link on Evidence Uploaded screen
    Then I will see the Upload EHIC Card screen
    And I will see successful deletion message for file <fileName1> of format <fileFormat1>
    When I upload the EHIC <fileName2> of format <fileFormat2>
    And I will see the View Uploaded Evidence screen
    And I view uploaded EHIC <fileName2> on Uploaded Evidence Screen
    And I view uploaded CAS <fileName1> on Uploaded Evidence Screen
    Examples:
      | fileName1     | fileFormat1 | fileName2     | fileFormat2 |
      | Sample_1      | .jpg        | sample_2      | .png        |

  Scenario Outline: Validate the delete EHIC functionality for all the acceptable 'File' formats
    When I upload the EHIC <fileName> of format <fileFormat>
    And I view the uploaded EHIC <fileName>
    And I upload the CAS <fileName> of format <fileFormat>
    And I will see the View Uploaded Evidence screen
    And I select delete EHIC link on Evidence Uploaded screen
    Then I will see the Upload EHIC Card screen
    And I will see successful deletion message for file <fileName> of format <fileFormat>
    Examples:
      | fileName     | fileFormat |
      | Sample_1     | .jpg       |
      | sample_2     | .png       |
      | sample_3     | .pdf       |
      | Sample_4     | .bmp       |
      | Sample_5     | .jpeg      |

  Scenario Outline: Validate that user is not able to continue without uploading the EHIC file once its deleted from Uploaded Evidence screen
    When I upload the EHIC <fileName> of format <fileFormat>
    And I view the uploaded EHIC <fileName>
    And I upload the CAS <fileName> of format <fileFormat>
    And I will see the View Uploaded Evidence screen
    And I select delete EHIC link on Evidence Uploaded screen
    Then I will see the Upload EHIC Card screen
    And I will see successful deletion message for file <fileName> of format <fileFormat>
    When I Do Not upload the EHIC file
    Then I will see the No file selected error
    Examples:
      | fileName     | fileFormat |
      | Sample_1     | .jpg       |

  Scenario Outline: Validate the back link on Upload EHIC page once EHIC file has been deleted from Uploaded Evidence screen
    When I upload the EHIC <fileName> of format <fileFormat>
    And I view the uploaded EHIC <fileName>
    And I upload the CAS <fileName> of format <fileFormat>
    And I will see the View Uploaded Evidence screen
    And I select delete EHIC link on Evidence Uploaded screen
    Then I will see the Upload EHIC Card screen
    And I will see successful deletion message for file <fileName> of format <fileFormat>
    When I select the Back link
    Then I will see the Phone number screen
    Examples:
      | fileName     | fileFormat |
      | Sample_1     | .jpg       |

  @IHSRI-2912
  Scenario Outline: Validate the user is able to delete uploaded EHIC file with 'File Name' of maximum 130 characters from Uploaded Evidence screen
    When I upload the EHIC <fileName> of format <fileFormat>
    And I view the uploaded EHIC <fileName><fileFormat>
    And I upload the CAS <fileName> of format <fileFormat>
    Then I will see the View Uploaded Evidence screen
    When I select delete EHIC link on Evidence Uploaded screen
    Then I will see the Upload EHIC Card screen
    And I will see successful deletion message for file <fileName> of format <fileFormat>
    Examples:
      | fileName                                                                                                                           | fileFormat |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       |
      | FileNameWith130Characters1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123a  | .pdf       |