@DeleteUploadedCAS @IHSRI-1915 @Regression

Feature: Validation of the Delete uploaded CAS Letter functionality on the IHS claim app to enable student applicants to correct their CAS letter.

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Upload CAS screen

  @Smoke
  Scenario Outline: Validate the user is able to upload another CAS file after deleting it
    When I upload the CAS <fileName1> of format <fileFormat1>
    Then I will see the View Uploaded Evidence screen
    When I select delete CAS link on Evidence Uploaded screen
    Then I will see the Upload CAS Letter screen
    Then I will see successful deletion message for file <fileName1> of format <fileFormat1>
    When I upload the CAS <fileName2> of format <fileFormat2>
    Then I will see the View Uploaded Evidence screen
    And I view the uploaded CAS <fileName2>
    Examples:
      | fileName1     | fileFormat1 | fileName2     | fileFormat2 |
      | Sample_1      | .jpg        | sample_2      | .png        |

  Scenario Outline: Validate the delete CAS functionality for all the acceptable 'File' formats
    When I upload the CAS <fileName> of format <fileFormat>
    Then I will see the View Uploaded Evidence screen
    When I select delete CAS link on Evidence Uploaded screen
    Then I will see the Upload CAS Letter screen
    Then I will see successful deletion message for file <fileName> of format <fileFormat>
    Examples:
      | fileName     | fileFormat |
      | Sample_1     | .jpg       |
      | sample_2     | .png       |
      | sample_3     | .pdf       |
      | Sample_4     | .bmp       |
      | Sample_5     | .jpeg      |

  Scenario Outline: Validate that user is not able to continue without uploading the CAS file once its deleted
    When I upload the CAS <fileName> of format <fileFormat>
    Then I will see the View Uploaded Evidence screen
    When I select delete CAS link on Evidence Uploaded screen
    Then I will see the Upload CAS Letter screen
    Then I will see successful deletion message for file <fileName> of format <fileFormat>
    When I Do Not upload the CAS file
    Then I will see the No file selected error
    Examples:
      | fileName     | fileFormat |
      | Sample_1     | .jpg       |

  Scenario Outline: Validate the back link on Upload CAS page once CAS file has been deleted
    When I upload the CAS <fileName> of format <fileFormat>
    Then I will see the View Uploaded Evidence screen
    When I select delete CAS link on Evidence Uploaded screen
    Then I will see the Upload CAS Letter screen
    Then I will see successful deletion message for file <fileName> of format <fileFormat>
    When I select the Back link
    Then I will see the View Uploaded EHIC screen
    Examples:
      | fileName     | fileFormat |
      | Sample_1     | .jpg       |

  @IHSRI-2913
  Scenario Outline: Validate the user is able to delete uploaded CAS file with 'File Name' of maximum 130 characters
    When I upload the CAS <fileName> of format <fileFormat>
    Then I will see the View Uploaded Evidence screen
    When I select delete CAS link on Evidence Uploaded screen
    Then I will see the Upload CAS Letter screen
    And I will see successful deletion message for file <fileName> of format <fileFormat>
    Examples:
      | fileName                                                                                                                           | fileFormat |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       |
      | FileNameWith130Characters1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123a  | .pdf       |