@UploadDependantEHIC @IHSRI-1103 @Regression

Feature: Validation of the Upload Dependant EHIC Card page on the IHS claim app to enable student applicants to upload their dependant's EHIC card to prove their eligibility.

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Upload Dependant EHIC screen

  @Smoke
  Scenario Outline: Validate the dependant's upload EHIC functionality for all the acceptable 'File' formats
    When I upload the Dependant EHIC <fileName> of format <fileFormat>
    Then I will see the <output>
    Examples:
      | fileName     | fileFormat | output                              |
      #valid file formats
      | Sample_1     | .jpg       | View Uploaded Dependant EHIC screen |
      | sample_2     | .png       | View Uploaded Dependant EHIC screen |
      | sample_3     | .pdf       | View Uploaded Dependant EHIC screen |
      | Sample_4     | .bmp       | View Uploaded Dependant EHIC screen |
      | Sample_5     | .jpeg      | View Uploaded Dependant EHIC screen |

  Scenario Outline: Validate the dependant's upload EHIC functionality for the unacceptable 'File' formats
    When I upload the Dependant EHIC <fileName> of format <fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> file
    Examples:
      | fileName     | fileFormat | error                               |
      #invalid file formats
      | Sample excel | .xlsx      | Invalid file format error           |
      | Sample text  | .rtf       | Invalid file format error           |
      | Sample word  | .doc       | Invalid file format error           |

  Scenario Outline: Validate the dependant's upload EHIC functionality for all the acceptable 'File Name' formats
    When I upload the Dependant EHIC with name <fileName> <fileFormat>
    Then I will see the <output>
    Examples:
      | fileName           | fileFormat | output                              |
      #valid file names
      | Payslip-June       | .png       | View Uploaded Dependant EHIC screen |
      | My Payslip_1       | .pdf       | View Uploaded Dependant EHIC screen |
      | Payslip July       | .bmp       | View Uploaded Dependant EHIC screen |
      | 123456             | .pdf       | View Uploaded Dependant EHIC screen |
      | 1-payslip_May Name | .jpg       | View Uploaded Dependant EHIC screen |
      | Payslip   Mar      | .pdf       | View Uploaded Dependant EHIC screen |

  Scenario Outline: Validate the dependant's upload EHIC functionality for the unacceptable 'File Name' formats
    When I upload the Dependant EHIC <fileName> of format <fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> file
    Examples:
      | fileName           | fileFormat | error                               |
      #invalid file names
      | 'My Payslip'       | .jpg       | Invalid file name error             |
      | Payslip(Nov)       | .png       | Invalid file name error             |
      | Payslip.Jan        | .pdf       | Invalid file name error             |

  @Retest-2924
  Scenario Outline: Validate the error when dependant's EHIC file with size larger than 2MB is uploaded
    When I upload the Dependant EHIC <fileName> <fileFormat> of size <fileSize>
    Then I will be displayed <error> for <fileName><fileFormat> file
    Examples:
      | fileName       | fileFormat | fileSize | error               |
      | File_size_2_1MB| .png       | > 2MB    | Max file size error |
      | File_size_2_3MB| .png       | > 2MB    | Max file size error |
      | More_than_2MB  | .bmp       | > 2MB    | Max file size error |
      | More_than_2MB  | .jpeg      | > 2MB    | Max file size error |
      | More_than_2MB  | .jpg       | > 2MB    | Max file size error |
      | More_than_2MB  | .pdf       | > 2MB    | Max file size error |
      | More_than_2MB  | .png       | > 2MB    | Max file size error |
      | File_size_3MB  | .png       | > 2MB    | Max file size error |
      | File_size_4MB  | .pdf       | > 3MB    | Max file size error |
      | More_than_5MB  | .bmp       | > 5MB    | Max file size error |
      | More_than_5MB  | .pdf       | > 5MB    | Max file size error |
      | More_than_10MB | .bmp       | > 10MB   | Max file size error |
      | More_than_10MB | .jpeg      | > 10MB   | Max file size error |
      | More_than_10MB | .jpg       | > 10MB   | Max file size error |
      | More_than_10MB | .pdf       | > 10MB   | Max file size error |
      | More_than_30MB | .pdf       | > 30MB   | Max file size error |
      | More_than_30MB | .png       | > 30MB   | Max file size error |
      | More_than_50MB | .bmp       | > 50MB   | Max file size error |

  @Retest-2924
  Scenario Outline: Validate the dependant's upload EHIC functionality for acceptable 'File Size' formats
    When I upload the Dependant EHIC <fileName> <fileFormat> of size <fileSize>
    Then I will see the <output>
    Examples:
      | fileName               | fileFormat | fileSize | output                    |
       #valid file sizes
      | small_size_file        | .bmp       | > 35KB   | View Uploaded Dependant EHIC screen |
      | SampleFile             | .jpg       | > 500KB  | View Uploaded Dependant EHIC screen |
      | thisIsMy_PDF_1MB       | .pdf       | > 1MB    | View Uploaded Dependant EHIC screen |
      | Large_size_jpg_file    | .jpg       | > 1MB    | View Uploaded Dependant EHIC screen |
      | Large_size_png_file    | .png       | > 1MB    | View Uploaded Dependant EHIC screen |
      | Sample-jpg-image-1-1mb | .jpeg      | > 1MB    | View Uploaded Dependant EHIC screen |
      | Sample-jpg-image-1-2mb | .jpeg      | > 1.2MB  | View Uploaded Dependant EHIC screen |
      | Sample-jpg-image-1-3mb | .jpeg      | > 1.3MB  | View Uploaded Dependant EHIC screen |
      | Sample-jpg-image-1-5mb | .jpeg      | > 1.5MB  | View Uploaded Dependant EHIC screen |
      | Sample-jpg-image-1-7mb | .jpeg      | > 1.7MB  | View Uploaded Dependant EHIC screen |
      | Sample-jpg-image-1-8mb | .jpeg      | > 1.8MB  | View Uploaded Dependant EHIC screen |
      | Sample-jpg-image-1-9mb | .jpeg      | > 1.9MB  | View Uploaded Dependant EHIC screen |
      | File_size_2MB          | .bmp       | 2MB      | View Uploaded Dependant EHIC screen |

  @IHSRI-2918
  Scenario Outline: Validate the dependant's upload EHIC functionality for the 'File Name' with maximum 130 characters
    When I upload the Dependant EHIC with name <fileName> <fileFormat>
    Then I will see the <output>
    Examples:
      | fileName                                                                                                                           | fileFormat | output                              |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       | View Uploaded Dependant EHIC screen |
      | FileNameWith130Characters1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123a  | .pdf       | View Uploaded Dependant EHIC screen |

  @IHSRI-2918
  Scenario Outline: Validate the dependant's upload EHIC functionality for the 'File Name' with more than 130 characters
    When I upload the Dependant EHIC with name <fileName> <fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> file
    Examples:
      | fileName                                                                                                                             | fileFormat | error                          |
      | File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-123  | .png       | More than 130 characters error |
      | File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-abcd | .pdf       | More than 130 characters error |

  Scenario Outline: Validate the user is able to upload the dependant's EHIC file
    When I <uploadFile> upload the Dependant EHIC file
    Then I will see the <output>
    Examples:
      | uploadFile | output                              |
      | Do         | View Uploaded Dependant EHIC screen |
      | Do Not     | No file selected error              |

  Scenario Outline: Validate the hyperlinks on Upload Dependant EHIC page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                         |
      | Back link         | Enter Dependant Address screen |
      | Service Name link | Start screen                   |
      | GOV.UK link       | GOV UK screen                  |