package com.nhsbsa.ihs.utilities;

import com.nhsbsa.ihs.shared.SharedData;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigReader {

    private ConfigReader() {
        throw new IllegalStateException("ConfigReader Utility Class");
    }
    static Logger logger = Logger.getLogger(ConfigReader.class.getName());
    private static String baseURL;

    public static String getEnvironment() {
        SharedData.environment = System.getProperty("env");
        String getURL = System.getenv("IHS_STUDENT_CLAIM");

        switch (SharedData.environment) {
            case "dev", "tst", "stage":
                baseURL = getURL.replace("{env}", SharedData.environment);
                break;
            case "prod":
                baseURL = (System.getenv("IHS_STUDENT_PROD"));
                break;
            case "local":
                baseURL = (System.getenv("IHS_STUDENT_LOCAL"));
                break;
            default:
                logger.log(Level.SEVERE,"Please provide environment details");
                break;
        }
        return baseURL;
    }

    public static String getUser() {
        return System.getenv("SAUCE_USERNAME");
    }

    public static String getAccessKey() {
        return System.getenv("SAUCE_ACCESS_KEY");
    }

    public static String getServer() {
        return System.getenv("ONDEMAND_SERVER");
    }

    public static String getTunnelName(){
        return System.getenv("SAUCE_TUNNEL_NAME");
    }
}