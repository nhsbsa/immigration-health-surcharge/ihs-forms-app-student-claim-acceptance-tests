package com.nhsbsa.ihs.pageobjects;

import io.cucumber.java.Scenario;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.Random;

public class Page {

    protected WebDriver driver;
    private WebElement element;
    public static final int EXPLICIT_WAIT_TIME = 60;
    public WebDriverWait webDriverWait;
    private static final Random random = new Random();

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateToUrl(String url) {
        driver.get(url);
    }

    public WebDriverWait getWebDriverWait() {
        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(EXPLICIT_WAIT_TIME));
        webDriverWait.ignoring(NoSuchElementException.class);
        webDriverWait.ignoring(StaleElementReferenceException.class);
        return webDriverWait;
    }

    public void waitForPageLoad() {
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(EXPLICIT_WAIT_TIME));
        wait.until(d -> {((JavascriptExecutor) driver).executeScript("return document.readyState");
            return (((JavascriptExecutor) driver).executeScript("return document.readyState"))
                    .equals("complete");
        });
    }

    public void clickEvent(By by) {
        waitForPageLoad();
        webDriverWait = getWebDriverWait();
        WebElement ele = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(by));
        ele.click();
    }

    public void sendTextValues(By by, String text) {
        waitForPageLoad();
        webDriverWait = getWebDriverWait();
        WebElement ele = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(by));
        ele.clear();
        ele.sendKeys(text);
    }

    public void checkScenarioRunStatus(Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) driver)
                    .getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot,"image/jpeg", "failed screen" );
        }
    }

    public boolean getElementIsDisplayed(By by) {
        return driver.findElement(by).isDisplayed();
    }

    public String getElementText() {
        return element.getText();
    }

    public String getElementText(By by) {
        return driver.findElement(by).getText();
    }

    public String generateOverLimitText(int count) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder(20);
        for (int i = 0; i < count; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public int getLengthOfEnteredText(By by) {
        return driver.findElement(by).getAttribute("value").length();
    }

    public String getPageTitles(){
        return driver.getTitle();
    }

    public void click() {
        element.click();
    }

    public void tearDownDriver() {
        driver.quit();
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }

    public void goBack() {
        driver.navigate().back();
    }

    public void navigateToElementBy(By by) {
        element = element.findElement(by);
    }

    public String getAttributeValue(String val) {
        return element.getAttribute(val);
    }

    public String getElementValue(By by) {
        return driver.findElement(by).getAttribute("value");
    }

    public void clickEventJS(By by){
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement ele = driver.findElement(by);
        js.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 500);");
        ele.click();
    }

    public void sendTextValuesJS(By by, String text) {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement ele = driver.findElement(by);
        js.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 500);");
        ele.clear();
        ele.sendKeys(text);
    }
}
