package com.nhsbsa.ihs.pageobjects;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;

public class UploadEHICFilePage extends Page {

    private By uploadEHICButtonLocator = By.id("upload-european-health-insurance-card-ehic");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.xpath("//div[@class=\"govuk-error-summary__body\"]/ul/li");
    private By uploadedEHICLocator = By.xpath("//*[@id=\"uploaded-files-european-health-insurance-card-ehic\"]/dl/div/dd");
    private By deleteEHICLocator = By.id("delete-ehic-file");
    private By deletedEHICSuccessLocator = By.xpath("//*[@id=\"gov-grid-row-content\"]/div/form/div[1]/div[2]/p");

    public UploadEHICFilePage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void chooseEHICAndContinue(String fileName) {
        sendTextValues(uploadEHICButtonLocator, fileName);
        clickEvent(continueButtonLocator);
    }

    public String getUploadErrorMessage() {
        return getElementText(errorMessageLocator);
    }

    public String getUploadedEHIC() {
        return getElementText(uploadedEHICLocator);
    }

    public void continueOnViewEHIC() {
        clickEvent(continueButtonLocator);
    }

    public void chooseFileAndContinueBS(String fileName) throws IOException {
        String sPlatform = System.getProperty("platform_name");
        if (sPlatform.contains("Win") || sPlatform.contains("mac")) {
            ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
            sendTextValues(uploadEHICButtonLocator, fileName);
        } else {
            ((AndroidDriver) driver).pushFile("/data/local/tmp/Payslip-June.png", new File(fileName));
            sendTextValues(uploadEHICButtonLocator, "/data/local/tmp/Payslip-June.png");
        }
        continueButton();
    }

    public void deleteEHICCardLink() { clickEvent(deleteEHICLocator); }

    public String getDeletedEHICSuccessMessage() {
        return getElementText(deletedEHICSuccessLocator);
    }

}
