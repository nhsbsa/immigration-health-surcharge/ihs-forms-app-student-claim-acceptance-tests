package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckYourAnswersPage extends Page{

    private By continueButtonLocator = By.id("continue");
    private By claimNameChangeLocator = By.id("name-change");
    private By claimNameLocator = By.id("name-value");
    private By claimDobChangeLocator = By.id("date-of-birth-change");
    private By claimDobLocator = By.id("date-of-birth-value");
    private By ihsNumberChangeLocator = By.id("immigration-health-surcharge-number-change");
    private By ihsNumberLocator = By.id("immigration-health-surcharge-number-value");
    private By visaShareCodeChangeLocator = By.id("applicant-share-code-change");
    private By visaShareCodeLocator = By.id("applicant-share-code-value");
    private By addressChangeLocator = By.id("enter-address-change");
    private By addressLocator = By.id("enter-address-value");
    private By emailChangeLocator = By.id("email-address-change");
    private By emailLocator = By.id("email-address-value");
    private By phoneNumberChangeLocator = By.id("telephone-number-change");
    private By phoneNumberLocator = By.id("telephone-number-value");
    private By depNameChangeLocator = By.id("dependant-name-1-change");
    private By depNameLocator = By.id("dependant-name-1-value");
    private By depDOBChangeLocator = By.id("dependant-date-of-birth-1-change");
    private By depDOBLocator = By.id("dependant-date-of-birth-1-value");
    private By depIhsNumberChangeLocator = By.id("dependant-immigration-health-surcharge-number-1-change");
    private By depIhsNumberLocator = By.id("dependant-immigration-health-surcharge-number-1-value");
    private By depVisaShareCodeChangeLocator = By.id("dependant-share-code-1-change");
    private By depVisaShareCodeLocator = By.id("dependant-share-code-1-value");
    private By depAddressChangeLocator = By.id("dependant-enter-address-1-change");
    private By depAddressLocator = By.id("dependant-enter-address-1-value");
    private By evidenceChangeLocator = By.id("applicant-file-upload-change");
    private By evidenceLocator = By.id("applicant-file-upload-value");
    private By depFilesUploadedChangeLocator = By.id("upload-dependant-european-health-insurance-card-ehic-1-change");
    private By depFilesUploadedLocator = By.id("upload-dependant-european-health-insurance-card-ehic-1-value");


    public CheckYourAnswersPage(WebDriver driver) {
        super(driver);
    }


    public void continueButton() { clickEvent(continueButtonLocator); }

    public void claimNameChangeLink() {
        clickEvent(claimNameChangeLocator);
    }

    public void claimDOBChangeLink() { clickEvent(claimDobChangeLocator); }

    public void claimIhsNumberChangeLink() { clickEvent(ihsNumberChangeLocator); }

    public void claimVisaShareCodeChangeLink() { clickEvent(visaShareCodeChangeLocator); }

    public void claimAddressChangeLink() { clickEvent(addressChangeLocator); }

    public void claimEmailChangeLink() { clickEvent(emailChangeLocator); }

    public void claimPhoneNumberChangeLink() { clickEvent(phoneNumberChangeLocator); }

    public void depNameChangeLink() { clickEvent(depNameChangeLocator); }

    public void depDOBChangeLink() { clickEvent(depDOBChangeLocator); }

    public void depIHSNumberChangeLink() { clickEvent(depIhsNumberChangeLocator); }

    public void depVisaShareCodeChangeLink() { clickEvent(depVisaShareCodeChangeLocator); }

    public void depAddressChangeLink() { clickEvent(depAddressChangeLocator); }

    public void evidenceChangeLink() { clickEvent(evidenceChangeLocator); }

    public void depFilesUploadedChangeLink() { clickEvent(depFilesUploadedChangeLocator);}


    public String getApplicantName() { return getElementText(claimNameLocator); }

    public String getApplicantDOB() { return getElementText(claimDobLocator); }

    public String getApplicantIHSNumber() { return getElementText(ihsNumberLocator); }

    public String getApplicantVisaShareCode() { return getElementText(visaShareCodeLocator); }

    public String getApplicantAddress() { return getElementText(addressLocator); }

    public String getApplicantEmail() { return getElementText(emailLocator); }

    public String getApplicantPhoneNumber() { return getElementText(phoneNumberLocator); }

    public String getDepName() { return getElementText(depNameLocator); }

    public String getDepDOB() { return getElementText(depDOBLocator); }

    public String getDepIHSNumber() { return getElementText(depIhsNumberLocator);}

    public String getDepVisaShareCode() { return getElementText(depVisaShareCodeLocator); }

    public String getDepAddress() { return getElementText(depAddressLocator); }

    public String getEvidence(){return getElementText(evidenceLocator);}

    public String getDepFilesUploaded() { return getElementText(depFilesUploadedLocator); }
}