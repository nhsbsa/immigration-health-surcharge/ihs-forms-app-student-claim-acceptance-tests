package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MobileNumberPage extends Page {

    private By mobileNumberLocator = By.id("telephone-number");
    private By continueButtonLocator = By.id("continue-button");
    private By mobileNumberErrorMessageLocator = By.partialLinkText("Enter");

    public MobileNumberPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterMobileNumberAndSubmit(String mobileNumber) {
        sendTextValues(mobileNumberLocator, mobileNumber);
        continueButton();
    }

    public String getMobileNumberErrorMessage() {
        return getElementText(mobileNumberErrorMessageLocator);
    }

    public String getEnteredPhoneNumber() {
        return getElementValue(mobileNumberLocator);
    }

    public void enterMobileNumber(String mobileNumber) {
        sendTextValues(mobileNumberLocator, mobileNumber);
    }

}

