package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DependantPaidWorkPage extends Page {

    private By yesRadioButtonLocator = By.id("dependant-paid-work-uk-yes");
    private By noRadioButtonLocator = By.id("dependant-paid-work-uk-no");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select 'Yes' if");

    public DependantPaidWorkPage(WebDriver driver) {
        super(driver);
    }

    public void selectYesRadioButtonAndClickContinue() {
        clickEvent(yesRadioButtonLocator);
        continueButton();
    }

    public void selectNoRadioButtonAndClickContinue() {
        clickEvent(noRadioButtonLocator);
        continueButton();
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public String getErrorMessage() {
        return getElementText(errorMessageLocator);
    }
}
