package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class VisaShareCodePage extends Page {

    private By visaShareCodeLocator = By.id("applicant-share-code");
    private By continueButtonLocator = By.id("continue-button");
    private By shareCodeHyperlinkLocator = By.id("viewProveImmigrationStatus");
    private By visaShareCodeErrorMessageLocator = By.partialLinkText("Enter a correct");
    private By emptyVisaShareCodeErrorMessageLocator = By.partialLinkText("Enter the applicant's");


    public VisaShareCodePage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterVisaShareCodeAndSubmit(String visaShareCode) {
        sendTextValues(visaShareCodeLocator, visaShareCode);
        continueButton();
    }

    public void navigateToShareCode() {
        clickEvent(shareCodeHyperlinkLocator);
    }

    public String getVisaShareCodeErrorMessage() {
        return getElementText(visaShareCodeErrorMessageLocator);
    }

    public String getEmptyVisaShareCodeErrorMessage() {
        return getElementText(emptyVisaShareCodeErrorMessageLocator);
    }

    public String getEnteredVisaShareCode() {
        return getElementValue(visaShareCodeLocator);
    }

    public void enterVisaShareCode(String visaShareCode) {
        sendTextValues(visaShareCodeLocator, visaShareCode);
    }

}
