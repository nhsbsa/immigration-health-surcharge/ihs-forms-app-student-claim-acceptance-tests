package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DependantEnterAddressPage extends Page {

    private By addressLine1Locator = By.id("dependant-address-line-1");
    private By addressLine2Locator = By.id("dependant-address-line-2");
    private By townOrCityLocator = By.id("dependant-address-town");
    private By countyLocator = By.id("dependant-address-county");
    private By postCodeLocator = By.id("dependant-address-postcode");
    private By continueButtonLocator = By.id("continue-button");
    private By addressLineErrorMessageLocator = By.partialLinkText("Enter the dependant's address line");
    private By townErrorMessageLocator = By.partialLinkText("Enter the dependant's town");
    private By countyErrorMessageLocator = By.partialLinkText("Enter the dependant's county");
    private By postCodeErrorMessageLocator = By.partialLinkText("Enter the dependant's postcode");
    private By wrongPostCodeErrorMessageLocator = By.partialLinkText("Enter a");

    public DependantEnterAddressPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterAddressAndSubmit(String addressLine1, String addressLine2, String town, String county, String postCode) {
        sendTextValues(addressLine1Locator, addressLine1);
        sendTextValues(addressLine2Locator, addressLine2);
        sendTextValues(townOrCityLocator, town);
        sendTextValues(countyLocator, county);
        sendTextValues(postCodeLocator, postCode);
        continueButton();
    }

    public String getAddressLineErrorMessage() {
        return getElementText(addressLineErrorMessageLocator);
    }

    public String getTownErrorMessage() {
        return getElementText(townErrorMessageLocator);
    }

    public String getCountyErrorMessage() {
        return getElementText(countyErrorMessageLocator);
    }

    public String getPostCodeErrorMessage() {
        return getElementText(postCodeErrorMessageLocator);
    }

    public String getWrongPostCodeErrorMessage() {
        return getElementText(wrongPostCodeErrorMessageLocator);
    }

    public String getEnteredAddressLine1() { return getElementValue(addressLine1Locator); }

    public String getEnteredAddressLine2() { return getElementValue(addressLine2Locator); }

    public String getEnteredTown() { return getElementValue(townOrCityLocator); }

    public String getEnteredCounty() { return getElementValue(countyLocator); }

    public String getEnteredPostCode() { return getElementValue(postCodeLocator); }

    public void enterAddress(String building, String street, String town, String county, String postCode) {
        sendTextValues(addressLine1Locator, building);
        sendTextValues(addressLine2Locator, street);
        sendTextValues(townOrCityLocator, town);
        sendTextValues(countyLocator, county);
        sendTextValues(postCodeLocator, postCode);
    }


}
