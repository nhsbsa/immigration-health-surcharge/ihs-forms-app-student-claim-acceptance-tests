package com.nhsbsa.ihs.pageobjects;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;

public class UploadDependantEHICFilePage extends Page {

    private By uploadDependantEHICButtonLocator = By.id("upload-dependant-european-health-insurance-card-ehic");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select a file");
    private By uploadedEHICLocator = By.xpath("//*[@id=\"uploaded-dependant-files-european-health-insurance-card-ehic\"]/dl/div/dd");
    private By deleteDependantEHICLocator = By.id("delete-ehic-file");
    private By deletedDependantEHICSuccessLocator = By.xpath("//*[@id=\"gov-grid-row-content\"]/div/form/div[1]/div[2]/p");

    public UploadDependantEHICFilePage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void chooseDependantEHICAndContinue(String fileName) {
        sendTextValues(uploadDependantEHICButtonLocator, fileName);
        continueButton();
    }

    public String getUploadErrorMessage() {
        return getElementText(errorMessageLocator);
    }

    public String getUploadedEHIC() {
        return getElementText(uploadedEHICLocator);
    }

    public void continueOnViewEHIC() {
        clickEvent(continueButtonLocator);
    }

    public void chooseFileAndContinueBS(String fileName) throws IOException {
        String sPlatform = System.getProperty("platform_name");
        if(sPlatform.contains("Win") || sPlatform.contains("mac")) {
            ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
            sendTextValues(uploadDependantEHICButtonLocator, fileName);
        }
        else {
            ((AndroidDriver) driver).pushFile("/data/local/tmp/Payslip-June.png", new File(fileName));
            sendTextValues(uploadDependantEHICButtonLocator, "/data/local/tmp/Payslip-June.png");
        }
        continueButton();
    }

    public void deleteDependantEHICCardLink() { clickEvent(deleteDependantEHICLocator); }

    public String getDependantDeletedEHICSuccessMessage() {
        return getElementText(deletedDependantEHICSuccessLocator);
    }

}
