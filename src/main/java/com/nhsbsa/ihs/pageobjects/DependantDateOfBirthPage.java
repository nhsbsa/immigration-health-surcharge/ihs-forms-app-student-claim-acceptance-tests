package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DependantDateOfBirthPage extends Page {

    private By dayLocator = By.id("dependant-date-of-birth-day");
    private By monthLocator = By.id("dependant-date-of-birth-month");
    private By yearLocator = By.id("dependant-date-of-birth-year");
    private By continueButtonLocator = By.id("continue-button");
    private By dateOfBirthErrorMessageLocator = By.partialLinkText("Enter the dependant's");
    private By minimumYearsErrorMessageLocator = By.partialLinkText("Enter a");

    public DependantDateOfBirthPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterDateOfBirthAndSubmit(String day, String month, String year) {
        sendTextValues(dayLocator, day);
        sendTextValues(monthLocator, month);
        sendTextValues(yearLocator, year);
        continueButton();
    }

    public String getDOBErrorMessage() {
        return getElementText(dateOfBirthErrorMessageLocator);
    }

    public String getMinimumYearsErrorMessage() {
        return getElementText(minimumYearsErrorMessageLocator);
    }

    public String getEnteredDay() {
        waitForPageLoad();
        return getElementValue(dayLocator);
    }

    public String getEnteredMonth() {
        return getElementValue(monthLocator);
    }

    public String getEnteredYear() {
        return getElementValue(yearLocator);
    }

    public void enterDateOfBirth(String day, String month, String year) {
        sendTextValues(dayLocator, day);
        sendTextValues(monthLocator, month);
        sendTextValues(yearLocator, year);
    }
}