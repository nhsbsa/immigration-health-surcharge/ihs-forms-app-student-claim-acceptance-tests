package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DependantNotEligiblePage extends Page {

    private By contactUsLocator = By.id("contact-us-page-link");
    private By moreInfoLocator = By.id("start-page-link");
    private By continueButtonLocator = By.id("continue-button");

    public DependantNotEligiblePage(WebDriver driver) {
        super(driver);
    }

    public void navigateToContactUs () {
        clickEvent(contactUsLocator);
    }

    public void navigateToMoreInfo () {
        clickEvent(moreInfoLocator);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }
}
