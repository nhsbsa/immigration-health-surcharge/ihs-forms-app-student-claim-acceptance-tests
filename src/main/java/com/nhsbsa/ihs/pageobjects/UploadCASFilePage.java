package com.nhsbsa.ihs.pageobjects;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;

public class UploadCASFilePage extends Page {

    private By uploadCASButtonLocator = By.id("upload-evidence");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select a file");
    private By uploadedEHICLocator = By.xpath("//*[@id=\"uploaded-files-evidence\"]/dl/div[1]/dd");
    private By uploadedCASLocator = By.xpath("//*[@id=\"uploaded-files-evidence\"]/dl/div[2]/dd");
    private By deleteEHICLocator = By.id("delete-ehic-file");
    private By deleteCASLocator = By.id("delete-cas-file");
    private By deleteFileMessageLocator = By.xpath("//*[@id=\"gov-grid-row-content\"]/div/form/div/div[2]/p");

    public UploadCASFilePage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void chooseCASAndContinue(String fileName) {
        sendTextValues(uploadCASButtonLocator, fileName);
        clickEvent(continueButtonLocator);
    }

    public String getUploadErrorMessage() {
        return getElementText(errorMessageLocator);
    }

    public String getUploadedEHIC() {
        return getElementText(uploadedEHICLocator);
    }

    public String getUploadedCAS() {
        return getElementText(uploadedCASLocator);
    }

    public void continueOnViewCAS() {
        clickEvent(continueButtonLocator);
    }

    public void chooseFileAndContinueBS(String fileName) throws IOException {
        String sPlatform = System.getProperty("platform_name");
        if(sPlatform.contains("Win") || sPlatform.contains("mac")) {
            ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
            sendTextValues(uploadCASButtonLocator, fileName);
        }
        else {
            ((AndroidDriver) driver).pushFile("/data/local/tmp/Payslip-June.png", new File(fileName));
            sendTextValues(uploadCASButtonLocator, "/data/local/tmp/Payslip-June.png");
        }
        continueButton();
    }

    public void deleteEHICLink() {
        clickEvent(deleteEHICLocator);
    }

    public void deleteCASLink() {
        clickEvent(deleteCASLocator);
    }

    public String getDeleteFileMessage() {
        return getElementText(deleteFileMessageLocator);

    }
}
