package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddDependantPage extends Page{

    private By yesRadioButtonLocator = By.id("add-dependants-with-european-health-insurance-card-ehic-yes");
    private By noRadioButtonLocator = By.id("add-dependants-with-european-health-insurance-card-ehic-no");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select 'Yes' if");

    public AddDependantPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void selectYesRadioButtonAndClickContinue() {
        clickEvent(yesRadioButtonLocator);
        continueButton();
    }

    public void selectNoRadioButtonAndClickContinue() {
        clickEvent(noRadioButtonLocator);
        continueButton();
    }

    public String getErrorMessageSelectNoneOption() {
        return getElementText(errorMessageLocator);
    }
}
