package com.nhsbsa.ihs.driver;

import com.nhsbsa.ihs.shared.SharedData;
import com.nhsbsa.ihs.utilities.ConfigReader;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Config {

    private Config() {
        throw new IllegalStateException("Config Utility Class");
    }

    private static WebDriver driver;
    static Logger logger = Logger.getLogger(Config.class.getName());
    private static final String HTTPS = "https://";
    private static final String WD_HUB = "/wd/hub";

    public static WebDriver setDriver() {
        SharedData.browser = System.getProperty("browser");
        SharedData.environment = System.getProperty("env");

        switch (SharedData.browser) {
            case "chrome":
                driver = chromeDriver();
                break;
            case "firefox":
                driver = firefoxDriver();
                break;
            case "headless":
                driver = headlessDriver();
                break;
            case "edge":
                driver = edgeDriver();
                break;
            case "saucelabs":
                try {
                    driver = sauceLabsDriver();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                break;
            default:
                logger.log(Level.SEVERE,"Please provide browser details");
                break;
        }
        return driver;
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static void setWebDriver(WebDriver webDriver) {
        driver = webDriver;
    }

    private static WebDriver headlessDriver() {
        String proxyAddress = System.getProperty("proxy");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--disable-extensions");
        options.addArguments("--disable-dev-shm-usage");
        if(proxyAddress != null) {
            Proxy proxy = new Proxy();
            proxy.setAutodetect(false);
            proxy.setHttpProxy(proxyAddress);
            proxy.setSslProxy(proxyAddress);
            proxy.setNoProxy("localhost");
            logger.log(Level.INFO,"Using proxy for http and ssl: {0}", proxyAddress);
            options.setCapability("proxy", proxy);
        }
        WebDriver driver = new ChromeDriver(options);
        configureCommonSettings(driver);
        return driver;
    }

    private static WebDriver chromeDriver() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        Map < String, Object > prefs = new HashMap <> ();
        prefs.put("profile.managed_default_content_settings.javascript", 2);
        options.setExperimentalOption("prefs", prefs);
        WebDriver driver = new ChromeDriver(options);
        configureCommonSettings(driver);
        return driver;
    }

    private static WebDriver firefoxDriver() {
        WebDriver driver = new FirefoxDriver();
        configureCommonSettings(driver);
        return driver;
    }

    private static WebDriver edgeDriver() {
        WebDriver driver = new EdgeDriver();
        configureCommonSettings(driver);
        return driver;
    }

    private static WebDriver sauceLabsDriver() throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        String sPlatform = System.getProperty("platform_name");

        capabilities.setCapability("platformName",System.getProperty("platform_name"));
        capabilities.setCapability("browserVersion",System.getProperty("browser_version"));
        capabilities.setCapability("browserName",System.getProperty("browser_name"));
        Map<String,Object> sauceOptions=new HashMap<>();
        sauceOptions.put("name" , "IHS Student Claim Tests");
        sauceOptions.put("build" , "IHS Student Claim Test build");
        sauceOptions.put("tunnelName", ConfigReader.getTunnelName());
        sauceOptions.put("screenResolution", System.getProperty("screen_resolution"));
        capabilities.setCapability("sauce:options",sauceOptions);

        if (sPlatform.contains("Android") || sPlatform.contains("iOS")) {
            sauceOptions.put("appiumVersion", "2.0.0");
            capabilities.setCapability("appium:deviceName", System.getProperty("device_name"));
            capabilities.setCapability("appium:platformVersion", System.getProperty("platform_version"));
            if (sPlatform.equalsIgnoreCase("Android")) {
                capabilities.setCapability("appium:automationName", "UiAutomator2");
                driver = new AndroidDriver(new URL(HTTPS + ConfigReader.getUser() + ":" + ConfigReader.getAccessKey() + "@" + ConfigReader.getServer() + WD_HUB), capabilities);
            } else {
                capabilities.setCapability("appium:automationName", "XCUITest");
                driver = new IOSDriver(new URL(HTTPS + ConfigReader.getUser() + ":" + ConfigReader.getAccessKey() + "@" + ConfigReader.getServer() + WD_HUB), capabilities);
            }
            return driver;
        }
        driver = new RemoteWebDriver(new URL(HTTPS + ConfigReader.getUser() + ":" + ConfigReader.getAccessKey() + "@" + ConfigReader.getServer() + WD_HUB), capabilities);
        return driver;
    }

    private static void configureCommonSettings(WebDriver driver) {
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(50));
        driver.manage().window().maximize();
    }
}